<?php

require_once "admin_actions/add_cat.php";
require_once "admin_actions/add_grade.php";
require_once "admin_actions/add_level.php";
require_once "admin_actions/add_question.php";
require_once "admin_actions/add_subject.php";
require_once "admin_actions/add_tag.php";
require_once "admin_actions/edit_cat.php";
require_once "admin_actions/edit_grade.php";
require_once "admin_actions/edit_level.php";
require_once "admin_actions/edit_question.php";
require_once "admin_actions/edit_subject.php";
require_once "admin_actions/edit_tag.php";