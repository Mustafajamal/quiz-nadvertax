<?php

//function saveQuizBeforePDF() {
//    $args = array(
//        "post_title" => trim(strip_tags($_POST['title'])),
//        "meta_input" => array(
//            "question_ids" => implode(",",json_decode(stripslashes($_POST['ids']))),
//            "answers" =>  trim(strip_tags($_POST['answers'])),
//            "subtitle" => trim(strip_tags($_POST['subtitle'])),
//            "user_id" => get_current_user_id()
//
//        ),
//        "post_type" => "eexams_quizzes",
//        "post_status" => "publish",
//        "post_author" => get_current_user_id()
//    );
//    wp_insert_post($args);
//}
function EExams_createQuixUI($args) 
{ 
    if (!current_user_can('can_create_quizes') && !current_user_can('manage_options')) {
        wp_die("Sorry, you don't have permission to view this page.");
    }
    ob_start();
    if (isset($_POST['ids'])) {
        wp_enqueue_script("jquery");
        wp_enqueue_script('EExams_mathjax', "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=default");
        wp_enqueue_script('EExams_pdf', "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js");
        wp_enqueue_script('EExams_CustomFontpdf', plugins_url('js/Roboto-normal.js?verison=1', __FILE__));
        wp_enqueue_script('EExams_canvas', plugins_url('js/html2canvas.js', __FILE__));
        wp_enqueue_script('EExams_canvas2image', "https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js");
        wp_enqueue_script('EExams_generatePDF', plugins_url('js/generatePDF.js?verison=2.3', __FILE__), array("jquery"), false, true);
        wp_enqueue_script('pdfobject', "https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.min.js");
        echo '<script type="text/x-mathjax-config">MathJax.Hub.Config({ 
                    jax: ["input/TeX", "output/SVG"],
                    extensions: ["tex2jax.js", "MathMenu.js", "MathZoom.js"],
                    showMathMenu: false,
                    showProcessingMessages: false,
                    messageStyle: "none",
                    SVG: {
                    useGlobalCache: false
                    },
                    TeX: {
                    extensions: ["AMSmath.js", "AMSsymbols.js", "autoload-all.js"]
                    }
                });</script>
        ';
        $ids = json_decode(stripslashes(stripslashes($_POST['ids'])));

        $posted_questions = json_decode(stripslashes($_POST['questions']));

        $posts = array();
        foreach ($ids as $id) {

            $question_obj = new EExams_Question($id);

//            $question = $question_obj->body;
//            $question = apply_filters('the_content', $question);
//            $question = str_replace(']]>', ']]&gt;', $question);
            $answers = $question_obj->answers;
            $type = $question_obj->type;
            $correct_answers = $question_obj->correct_answer;
            $temp = [];
            foreach( $posted_questions as $posted_question ) {
                if ( $id == $posted_question->id ) {
                    $question = $posted_question->body;
                    $question = apply_filters('the_content', $question);
                    $question = str_replace(']]>', ']]&gt;', $question);
                    $temp = array(
                        "title" => $posted_question->title,
                        "question" => $question,
                        "answers" => stripslashes($answers),
                        "type" => $type,
                        "correct_answer" => $correct_answers,
                        "points" => $posted_question->points
                    );
                    break;
                }
            }
            array_push($posts, $temp);
        }



        //saveQuizBeforePDF($posts);


        $pdf_settings = [
            "EExams_pdf_title_enabled" => get_option("EExams_pdf_title_enabled"),
            "EExams_pdf_title_font_size" => get_option("EExams_pdf_title_font_size"),
            "EExams_pdf_title_color" => get_option("EExams_pdf_title_color"),

            "EExams_pdf_subtitle_enabled" => get_option("EExams_pdf_subtitle_enabled"),
            "EExams_pdf_subtitle_font_size" => get_option("EExams_pdf_subtitle_font_size"),
            "EExams_pdf_subtitle_color" => get_option("EExams_pdf_subtitle_color"),

            "EExams_pdf_question_title_enabled" => get_option("EExams_pdf_question_title_enabled"),
            "EExams_pdf_question_title_font_size" => get_option("EExams_pdf_question_title_font_size"),
            "EExams_pdf_question_title_color" => get_option("EExams_pdf_question_title_color"),
            "EExams_pdf_questions_spacer" => get_option("EExams_pdf_questions_spacer"),

            //"EExams_pdf_points_enabled" => get_option("EExams_pdf_points_enabled"),
            "EExams_pdf_points_enabled" => get_option("EExams_pdf_points_enabled"),
            "EExams_pdf_points_font_size" => get_option("EExams_pdf_points_font_size"),
            "EExams_pdf_points_color" => get_option("EExams_pdf_points_color"),
            "EExams_pdf_points_text" => get_option("EExams_pdf_points_text"),

            //"EExams_pdf_footnote_enabled" => get_option("EExams_pdf_footnote_enabled"),
            "EExams_pdf_footnote_enabled" => ($_POST['footer-toggle'] == 'true') ? 'yes' : 'no',
            "EExams_pdf_footnote_font_size" => get_option("EExams_pdf_footnote_font_size"),
            "EExams_pdf_footnote_color" => get_option("EExams_pdf_footnote_color"),
            "EExams_pdf_footnote_text" => (!empty($_POST['footer'])) ? $_POST['footer'] : get_option("EExams_pdf_footnote_text"),
            "EExams_pdf_footnote_margin" => get_option("EExams_pdf_footnote_margin"),

            //"EExams_pdf_page_number_enabled" => get_option("EExams_pdf_page_number_enabled"),
            "EExams_pdf_page_number_enabled" => ($_POST['page-number-toggle'] == 'true') ? 'yes' : 'no',
            "EExams_pdf_page_number_font_size" => get_option("EExams_pdf_page_number_font_size"),
            "EExams_pdf_page_number_color" => get_option("EExams_pdf_page_number_color"),

            //"EExams_pdf_box_grade_enabled" => get_option("EExams_pdf_box_grade_enabled"),
            "EExams_pdf_box_grade_enabled" => ($_POST['box-toggle'] == 'true') ? 'yes' : 'no',

            "EExams_pdf_content_font_size" => get_option("EExams_pdf_content_font_size"),

            //"EExams_pdf_name_enabled" => get_option("EExams_pdf_name_enabled"),
            "EExams_pdf_name_enabled" => ($_POST['student-name-toggle'] == 'true') ? 'yes' : 'no',
            "EExams_pdf_name_font_size" => get_option("EExams_pdf_name_font_size"),
            "EExams_pdf_name_color" => get_option("EExams_pdf_name_color"),
            //"EExams_pdf_name_text" => get_option("EExams_pdf_name_text"),
            "EExams_pdf_name_text" => (!empty($_POST['student-name'])) ? $_POST['student-name'] : get_option("EExams_pdf_footnote_text"),
        ];

        wp_localize_script('EExams_generatePDF', 'EExams', array(
            "questions" => json_encode($posts),
            "title" => $_POST['title'],
            "subtitle" => $_POST['subtitle'],
            "answwers" => $_POST['answers'],
            "pdfq" => $_POST['pdfq'],
            "iconURL" => plugin_dir_url(__FILE__) . "img/",
            "filterPage" => get_page_link(intval(get_option('EExams_CreateAQuizPageID'))),
            "settings" => $pdf_settings
        ));
        echo '
            <div class="body-wrapper">
                <div id="scrapHtml" style="font-size:20px !important;overflow:visible !important;width:720px;background:#fff;"></div>
                <div id="scrapHtml2" style="font-size:20px !important;overflow:visible !important;width:720px;background:#fff;"></div>
                <div id="wrapper"></div>
                <div id="pdf-viewer" style="
                width: 100%;
                height: 100%;
                position: absolute;
                left: 0;
                right: 0;
                bottom: 0;
                top: 0px;
                "></div>
            </div>';
    } else {
        $edit = array();
        $answers = null;
        if (isset($_GET['edit']) && intval($_GET['edit']) !== 0) {
            $id = intval($_GET['edit']);
            $quizz = get_post($id);
            $student_name = get_post_meta($id,'student_name',true);
            if ($quizz) {
                $ids = explode(',', get_post_meta($id, "question_ids", true));
                $answers = get_post_meta($id, "answers", true);
                $edit['title'] = $quizz->post_title;
                $edit['subtitle'] = get_post_meta($id, "subtitle", true);
                $question_content = json_decode(get_post_meta($id,'question_contents',true),true);
                $selected_questions = array();
                $i=0;
                foreach ($ids as $question) {
                    $question = new EExams_Question($question);

                    $rating_submissions = intval($question->rating_submissions);
                    if ($rating_submissions == 0) {
                        $rating_submissions = 1;
                    }
                    $temp = array(
                        "id" => $question->id,
                        "title" => $question_content[$i]['title'],
                        "body" => $question_content[$i]['body'],
                        "subject" => $question->get_subject()->name,
                        "grade" => $question->get_grade()->name,
                        "level" => $question->get_level()->name,
                        "points" => $question->points,
                        "hours" => $question->hours,
                        "mins" => $question->mins,
                        "answers" => $question->answers,
                        "type" => $question->type,
                        "correct_answer" => $question->correct_answer,
                        "year" =>  $question->year,
                        "rating" => floatval($question->actual_rating),
                        "total_time_in_seconds" => $question->total_time_in_seconds,
                        "has_answers" => $question->has_answers,
                        "correspondance" => $question->correspondance
                    );
                    array_push($selected_questions, $temp);
                    $i++;
                }
                // $edit['selected_questions'] = $question_content;
                $edit['selected_questions'] = $selected_questions;
                $edit['id'] = $id;
                $edit['student_name'] = $student_name;
                $edit['footer'] = get_post_meta( $id, 'footer', true );
                $edit['box_toggle'] = get_post_meta( $id, 'box_toggle', true );
                $edit['footer_toggle'] = get_post_meta( $id, 'footer_toggle', true );
                $edit['page_number_toggle'] = get_post_meta( $id, 'page_number_toggle', true );
                $edit['student_name_toggle'] = get_post_meta( $id, 'student_name_toggle', true );
            }
        }

        wp_enqueue_script("jquery");
        wp_enqueue_script("jquery-ui-core");
        wp_enqueue_script("jquery-ui-sortable");
        wp_enqueue_script("jquery-ui-fix", plugins_url('js/jquery.ui.touch-punch.min.js', __FILE__));
        wp_enqueue_script('EExams_popper', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/popper.min.js");
        wp_enqueue_script(
            'EExams_bootstrap',
            "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"
        );
        wp_enqueue_script('EExams_swl', "https://cdn.jsdelivr.net/npm/sweetalert2@9");
        wp_enqueue_script('EExams_duration', plugins_url('js/duration-picker.min.js', __FILE__));
        wp_enqueue_script('EExams_readmore', plugins_url('js/readmore.min.js', __FILE__));
        wp_enqueue_script('EExams_mathjax', "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=default");
        wp_enqueue_script('EExams_stary', plugins_url('js/jquery.barrating.min.js', __FILE__));
//        wp_enqueue_script('EExams_simplemodal', "https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js");
        wp_enqueue_script('EExams_pagination', plugins_url('js/jquery.twbsPagination.min.js?verison=1.5', __FILE__));
        wp_enqueue_script('EExams_yearselect', plugins_url('js/year-select.js', __FILE__));
        //wp_enqueue_script('EExams_admincreateAquiz', plugins_url('js/createAquiz2.js?verison=1.9', __FILE__));
        wp_enqueue_script('EExams_admincreateAquiz', plugins_url('epitomi-exams/js/createAquiz2.js',time(), __FILE__));
        wp_enqueue_script('Toogle_Bootstrap_script', "https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js");
        // The Query
        wp_localize_script('EExams_admincreateAquiz', 'EExams', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'edit' => $edit,
            'iconsurl' => plugins_url('img/', __FILE__),
            "pdfq" => get_option("EExams_PDF_q"),
            "number_of_questions_per_page" =>  intval(get_option("EExams_num_per_page")),
        ));
?>
        <script type="text/x-mathjax-config">
            MathJax.Hub.Config({
            extensions: ["tex2jax.js"],
            jax: ["input/TeX", "output/HTML-CSS"],
            tex2jax: {
            inlineMath: [ [ "\\[" , "\\]" ] ],
            processEscapes: true
            },
            "HTML-CSS": { fonts: ["TeX"] }
        });
        </script>
        <div class="wrap bootstrap-wrapper">
            <div class="container-fluid mt-5">
                <div id="row-main" class="row">
                    <div id="sidebar" class="col-md-3 filters-bar">
                        <div class="filter-box">
                            <div class="filter-header row">
							<div class="col-6">
                                <h5 class="filter-title">
                                    Grades
                                </h5>
							</div>
							<div class="col-6">
								<span class="clear clear-filters text-danger">Clear all filters <i class="fas fa-sync-alt"></i></span>
							</div>
                            </div>
                            <div class="filter-body row grade-body">
                                <?php
                                $grades = new EExams_Grade();
                                $num = $grades->get_count_grades();
                                $grades = $grades->get_grades(6);
                                if (!empty($grades)) {
                                    foreach ($grades as $grade) {
                                        echo '<a class="filter grade col-4" data-filter="' . $grade['id'] . '">' . $grade['name'] . '</a>';
                                    }
                                } else {
                                    echo "there is no Grades found";
                                }
                                ?>
                                <input type="hidden" class="filter-input" name="grade" id="gradeInput">
                            </div>
                            <?php
                            if ($num > 6) {
                            ?>
                                <div class="filter-footer grade-footer">
                                    <a class="load-more grade-load-more">Load more <i class="fas fa-angle-down"></i></a>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <h5 class="filter-title">
                                    Subjects
                                </h5>
                            </div>
                            <div class="filter-body row subject-body">
                                <?php
                                $subjects = new EExams_Subject();
                                $num = $subjects->get_count_subjects();
                                $subjects = $subjects->get_subjects(6);
                                if (!empty($subjects)) {
                                    foreach ($subjects as $subject) {
                                        echo '<a class="filter subject col-4" data-filter="' . $subject['id'] . '">' . $subject['name'] . '</a>';
                                    }
                                } else {
                                    echo "there is no Subjects found";
                                }
                                ?>
                                <input type="hidden" class="filter-input" name="subject" id="subjectInput">
                            </div>
                            <?php
                            if ($num > 6) {
                            ?>
                                <div class="filter-footer subject-footer">
                                    <a class="load-more subject-load-more">Load more <i class="fas fa-angle-down"></i></a>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <form class="search-form">
                                    <div class="row no-gutters">
                                        <div class="col-auto">
                                            <h5 class="filter-title">
                                                Tags &nbsp;
                                            </h5>
                                        </div>
                                        <div class="col">
                                            <input type="text" class="search-input tags-search" placeholder="Search">
                                            <span class="search_icon"><i class="fas fa-search"></i></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="filter-body row tag-body">
                                <?php
                                $tags = new EExams_Tag();
                                $num = $tags->get_count_tags();
                                $tags = $tags->get_tags(6);
                                if (!empty($tags)) {
                                    foreach ($tags as $tag) {
                                        echo '<a class="filter tag col-4" data-filter="' . $tag['id'] . '">' . $tag['name'] . '</a>';
                                    }
                                } else {
                                    echo "there is no Tags found";
                                }
                                ?>
                                <input type="hidden" class="filter-input" name="tag" id="tagInput">
                            </div>
                            <?php
                            if ($num > 6) {
                            ?>
                                <div class="filter-footer tag-footer">
                                    <a class="load-more tag-load-more">Load more <i class="fas fa-angle-down"></i></a>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <h5 class="filter-title">
                                    Difficulty
                                </h5>
                            </div>
                            <div class="filter-body row level-body">
                                <?php
                                $levels = new EExams_Level();
                                $num = $levels->get_count_levels();
                                $levels = $levels->get_levels(3);
                                if (!empty($levels)) {
                                    foreach ($levels as $level) {
                                        echo '<a class="filter level col-4" data-filter="' . $level['id'] . '">' . $level['name'] . '</a>';
                                    }
                                } else {
                                    echo "there is no Difficulty Levels found";
                                }
                                ?>
                                <input type="hidden" class="filter-input" name="level" id="levelInput">
                            </div>
                            <?php
                            if ($num > 6) {
                            ?>
                                <div class="filter-footer level-footer">
                                    <a class="load-more level-load-more">Load more <i class="fas fa-angle-down"></i></a>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <h5 class="filter-title">
                                    Rating
                                </h5>
                            </div>
                            <div class="filter-body row">
                                <a class="filter rate col-4 rate1" data-filter="4.5">
                                    4.5 and up <br>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                </a>
                                <a class="filter rate col-4 rate2" data-filter="4">
                                    4 and up <br>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="far fa-star"></i>
                                </a>
                                <a class="filter rate col-4 rate3" data-filter="3.5">
                                    3.5 and up <br>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                    <i class="far fa-star"></i>
                                </a>
                                <a class="filter rate col-4 rate4" data-filter="3">
                                    3 and up <br>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </a>
                                <a class="filter rate col-4 rate5" data-filter="2.5">
                                    2.5 and up <br>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </a>
                                <input type="hidden" class="filter-input" name="rate" id="rateInput">
                            </div>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <h5 class="filter-title">
                                    Duration
                                </h5>
                            </div>
                            <div class="filter-body row">
                                <a class="filter duration col-6 dur1" data-filter='-300'>5 min and less</a>
                                <a class="filter duration col-6 dur2" data-filter='-600'>10 min and less</a>
                                <a class="filter duration col-6 dur3" data-filter='-900'>15 min and less</a>
                                <a class="filter duration col-6 dur4" data-filter='-1800'>30 min and less</a>
                                <a class="filter duration col-6 dur5" data-filter='-2700'>45 min and less</a>
                                <a class="filter duration col-6 dur6" data-filter='-3600'>60 min and less</a>
                                <a class="filter duration col-6 dur7" data-filter='+3600'>60 min and more</a>
                                <input type="hidden" class="filter-input" name="duration" id="durInput">
                            </div>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <h5 class="filter-title">
                                    Points
                                </h5>
                            </div>
                            <div class="filter-body row">
                                <a class="filter points col-4" data-filter='-5'>5 and less</a>
                                <a class="filter points col-4" data-filter='5-10'>5 - 10</a>
                                <a class="filter points col-4" data-filter='10-15'>10 - 15</a>
                                <a class="filter points col-4" data-filter='15-20'>15 - 20</a>
                                <a class="filter points col-4" data-filter='+20'>20 and up</a>

                                <input type="hidden" class="filter-input" name="points" id="pointsInput">
                            </div>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <h5 class="filter-title">
                                    Published Date
                                </h5>
                            </div>
                            <?php

                            $question = new EExams_Question();
                            $years = $question->get_all_years();

                            ?>
                            <div class="filter-body row">
                                <div class="col">
                                    From: &nbsp;&nbsp;<select name="from-year" class="yearselect from-year-select filter-input" id="from-year">
                                        <option value=""></option>
                                        <?php
                                        foreach ($years as $index) {
                                        ?>
                                            <option value="<?php echo $index['year'] ?>"><?php echo $index['year'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select> &nbsp;&nbsp;To: &nbsp;&nbsp;<select class="yearselect to-year-select filter-input" name="to-year" id="to-year">
                                        <option value=""></option>
                                        <?php
                                        foreach ($years as $index) {
                                        ?>
                                            <option value="<?php echo $index['year'] ?>"><?php echo $index['year'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="filter-box mt-3">
                            <div class="filter-header">
                                <h5 class="filter-title">
                                    Questions with Answers
                                </h5>
                                <div class="can-toggle demo-rebrand-2">
                                    <input id="answers" type="checkbox" class="filter-input" name="has-answers">
                                    <label for="answers">
                                        <div class="can-toggle__switch" data-checked="Active" data-unchecked="Not Active"></div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!--<span class="clear clear-filters-2 text-danger">Clear all filters <i class="fas fa-sync-alt"></i></span>-->
                    </div>
                    <div id="quiz" class="col-md-9">
                        <div class="btn-group" role="group" aria-label="Controls">
					<button type="button" class="btn btn-default toggle-sidebar"><i class="fas fa-angle-right"></i></button>
				</div>
                        <div class="row">
                            <div class="col-6">
                                <div class="left-col-header">
                                    <div class="row">
                                        <div class="col-auto">
                                            <div class="number-found"><span id="number_of_questions">0</span> results found</div>
                                        </div>
                                        <div class="col">
                                            <form id="left-column-serach" class="search-form">
                                                <div class="row no-gutters">
                                                    <div class="col-auto px-2">
                                                        <label for="sort-by">Sort by</label>
                                                        <select name="sort-by" class="filter-input" id="sort-by">
                                                            <option value="">Recently Added</option>

                                                            <optgroup label="Descending">
                                                                <option value="desc-actual_rating">Rating</option>
                                                                <option value="desc-level_id">Difficulty</option>
                                                                <option value="desc-total_time_in_seconds">Duration</option>
                                                            </optgroup>
                                                            <optgroup label="Ascending">
                                                                <option value="asc-actual_rating">Rating</option>
                                                                <option value="asc-level_id">Difficulty</option>
                                                                <option value="asc-total_time_in_seconds">Duration</option>
                                                            </optgroup>
             
                                                        </select>
                                                    </div>
                                                    <div class="col px-2">
                                                        <input type="text" class="filter-input search-input" name="search" placeholder="search in results">
                                                        <span class="search_icon"><i class="fas fa-search"></i></span>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="right-col-header">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="your-selection-container" style="display:none">
											<div class="col-5">
                                                <h5>Selection:</h5>
                                                    <span  style="cursor: pointer;" class="clear_results_right text-danger">Clear Results <i class="fas fa-sync-alt"></i></span>
											</div>

											<div class="col-6">
												<div class="col-auto">
													<button data-toggle="modal" onclick="showEditQuizModal('export')" type="button" disabled class="top-button btn btn-warning float-right ml-2">Export</button>
													<button data-toggle="modal" onclick="showEditQuizModal('save')" type="button" disabled class="top-button btn btn-success float-right">Save</button>
													<?php
													if (isset($_GET['edit']) && intval($_GET['edit']) !== 0) {
													?>
														<button type="button" class="SaveAs btn btn-success float-right">Save As PDF</button>
													<?php
													}
													?>
												</div>
											</div>
											
                                                <?php 
                                                // echo '<pre>';
                                                // print_r($question_content);
                                                // echo '</pre>';
                                                ?>
												<div class="col-12">
													<div class="your-selection"><span id="selection-num"></span> Selected</div>
													<div class="your-selection"><span id="selection-time"></span> min</div>
													<div class="your-selection"><span id="selection-points"></span> points</div>
												</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-12 questionsSelectedFrom">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row questionsSelectedTo">
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-6">
                                <div class="left-col-footer">
                                    <div class="row">
                                        <div class="col-12">
                                            <nav aria-label="Page navigation example" id="pagination">
                                                <ul class="pagination justify-content-end" id="paginationUl">

                                                </ul>
                                            </nav>

                                        </div>
                                    <input type="number" class="form-control" placeholder="Goto Page" id="goto_page">
                                    </div>

                                </div>
                            </div>
                            <div class="col-6">
                                <div class="right-col-footer">
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="button" style="display:none" class="finish end-button btn btn-warning float-right ml-2">Export</button>
                                            <button type="button" style="display:none" class="Save end-button btn btn-success float-right">Save</button>
                                            <?php
                                            if (isset($_GET['edit']) && intval($_GET['edit']) !== 0) {
                                            ?>
                                                <button type="button" class="SaveAs btn btn-success float-right">Save As PDF</button>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="edit-modal" class="modal fade">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 style="font-size: 1.25rem;" class="modal-title">Edit Quiz PDF</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div id="edit-pdf-div">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Title" id="title">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Sub Title" id="subtitle">
                                                </div>
                                            </div>
                                            <div class="col-md-4 ml-auto">
                                                <div class="card" style="width: auto; height: 100px;">
                                                    <input type="checkbox" id="box-toggle" data-size="mini" data-toggle="toggle" data-on="show" data-off="hide">
                                                </div>
                                                <br>
                                                <input type="checkbox" id="student-name-toggle" data-size="mini" data-toggle="toggle" data-on="show" data-off="hide">
                                                <input type="text" class="form-control" placeholder="Student Name" id="student-name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="checkbox" id="page-number-toggle" data-size="mini" data-toggle="toggle" data-on="show" data-off="hide">
                                    <p>Page Number</p>
                                </div>
                                <div class="col-md-4">
                                    <input type="checkbox" id="footer-toggle" data-size="mini" data-toggle="toggle" data-on="show" data-off="hide">
                                    <input type="text" class="form-control" placeholder="PDF Footer" id="footer">
                                </div>
                                <div id="button-div" class="col-md-4 text-right">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        
        <script>
		jQuery(document).ready(function () {
			jQuery(".toggle-sidebar").click(function () {
				jQuery("#sidebar").toggleClass("collapsed");
				jQuery("#quiz").toggleClass("col-md-12 col-md-9");
				jQuery(".toggle-sidebar i").toggleClass("fa-angle-left fa-angle-right");
				
				return false;
			});
		});
	</script>
<?php
    }

    return ob_get_clean();
}

function get_meta_values($meta_key,  $post_type = 'post')
{

    $posts = get_posts(
        array(
            'post_type' => $post_type,
            'meta_key' => $meta_key,
            'posts_per_page' => -1,
        )
    );

    $meta_values = array();
    foreach ($posts as $post) {
        $meta_values[] = get_post_meta($post->ID, $meta_key, true);
    }

    return $meta_values;
}

