<?php
function EEaxms_filterQuestions()
{
    global $post;
    try {
        // The Query
        $args = array(
            'post_type' => 'eexams_questions',
            'meta_query' => array(
                'relation' => 'AND',
            ),
            'post_status' => 'publish',
            'posts_per_page' => intval(get_option("EExams_num_per_page")),    
            'post__not_in' => array_map('intval', $_POST['selectedQuestions']),
            'paged' => intval($_POST['page']),
        );
        $data = $_POST['filter'];
        foreach ($data as $parm) {
            switch ($parm['name']) {
                case 'search':
                $args = array_merge($args, array(
                    's' => $parm['value'],
                ));
                break;
                case 'subject':
                    array_push($args['meta_query'], array(
                        'key' => '_subject',
                        'value' => json_decode(stripslashes($parm['value'])),
                    ));
                    break;
                case 'grade':
                    array_push($args['meta_query'], array(
                        'key' => '_grade_id',
                        'value' => json_decode(stripslashes($parm['value'])),
                    ));
                    break;
                case 'tag':
                    $args = array_merge($args, array(
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'questions_tags',
                                'field' => 'term_id',
                                'terms' => array_map('intval', json_decode(stripslashes($parm['value']))),
                            ),
                        ),
                    ));
                    break;
                case 'level':
                    array_push($args['meta_query'], array(
                        'key' => '_level_id',
                        'value' => json_decode(stripslashes($parm['value'])),
                    ));
                    break;
                case 'year':
                    if(intval($parm['value']) != 0)
                    {
                        array_push($args['meta_query'], array(
                            'key' => '_year',
                            'value' => intval($parm['value']),
                        ));
                    }
                    break;
                case 'points':
                    $or_args = array(
                        'relation' => 'OR',
                    );
                    foreach (json_decode(stripslashes($parm['value'])) as $data) {
                        if ($data[0] === '>') {
                            $str = ltrim($data, '>');
                            $val = intval($str);
                            array_push($or_args, array(
                                'key' => '_points',
                                'value' => $val,
                                'type' => 'numeric',
                                'compare' => '>',
                            ));
                        } else if ($data[0] === '<') {
                            $str = ltrim($data, '<');
                            $val = intval($str);
                            array_push($or_args, array(
                                'key' => '_points',
                                'value' => $val,
                                'type' => 'numeric',
                                'compare' => '<',
                            ));
                        } else {
                            $str = array_map('intval', explode('-', $data));
                            array_push($or_args, array(
                                'key' => '_points',
                                'value' => $str,
                                'type' => 'numeric',
                                'compare' => 'BETWEEN',
                            ));
                        }
                    }
                    array_push($args['meta_query'], $or_args);
                    break;
                case 'duration':
                    $duration = true;
                    $durationParm = json_decode(stripslashes($parm['value']));
                    $vaild = true;
                    $or_args = array(
                        'relation' => 'OR',
                    );
                    foreach ($durationParm as $data) {
                        $data = json_decode(stripslashes($data));
                        if (intval($data->minto) == -1 && intval($data->minfrom) != -1) {
                            array_push($or_args, array(
                                'key' => '_mins',
                                'value' => intval($data->minfrom),
                                'type' => 'numeric',
                                'compare' => '>',
                            ));
                            array_push($or_args, array(
                                'key' => '_hours',
                                'value' => 0,
                                'type' => 'numeric',
                                'compare' => '!=',
                            ));
                        } else if (intval($data->minfrom) == -1  && intval($data->minto) != -1) {
                            array_push($or_args, array(
                                'key' => '_mins',
                                'value' => intval($data->minto),
                                'type' => 'numeric',
                                'compare' => '<',
                            ));
                        } else if (intval($data->minfrom) != 0 && intval($data->minto) != 0 && intval($data->minfrom) != -1 && intval($data->minto) != -1) {
                            array_push($or_args, array(
                                'key' => '_mins',
                                'value' => array( intval($data->minfrom) ,  intval($data->minto) ),
                                'type' => 'numeric',
                                'compare' => 'BETWEEN',
                            ));
                        }
                        if (intval($data->hourto) == -1 && intval($data->hourfrom) != -1) {
                            array_push($or_args, array(
                                'key' => '_hours',
                                'value' => intval($data->hourfrom),
                                'type' => 'numeric',
                                'compare' => '>',
                            ));
                        } else if (intval($data->hourfrom) == -1  && intval($data->hourto) != -1) {
                            array_push($or_args, array(
                                'key' => '_hours',
                                'value' => intval($data->hourto),
                                'type' => 'numeric',
                                'compare' => '<',
                            ));
                        } else if (intval($data->hourfrom) != 0 && intval($data->hourto) != 0 && intval($data->hourfrom) != -1 && intval($data->hourto) != -1) {
                            array_push($or_args, array(
                                'key' => '_hours',
                                'value' => array( intval($data->hourfrom) ,  intval($data->hourto) ),
                                'type' => 'numeric',
                                'compare' => 'BETWEEN',
                            ));
                        }
                    }
                    array_push($args['meta_query'], $or_args);
                    break;
                default:
                    # code...
                    break;
            }
        }
        $the_query = new WP_Query($args);
        $posts = array();
        //print_r($args);
        // The Loop
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $rating_submissions = intval(get_post_meta( $post->ID, "_rating_submissions", true));
                if($rating_submissions == 0)
                {
                    $rating_submissions = 1;
                }
                $temp = array(
                    "post" => $post,
                    "subject" => get_the_title(intval(get_post_meta( $post->ID, "_subject", true ))),
                    "grade" => get_the_title(intval(get_post_meta( $post->ID, "_grade_id", true ))),
                    "level" => get_the_title(intval(get_post_meta( $post->ID, "_level_id", true ))),
                    "points" => get_post_meta( $post->ID, "_points", true),
                    "duration" => get_post_meta( $post->ID, "_duration", true),
                    "hours" => get_post_meta( $post->ID, "_hours", true),
                    "mins" => get_post_meta( $post->ID, "_mins", true),
                    "answers" => get_post_meta( $post->ID, "_answers",true),
                    "type" => get_post_meta( $post->ID, "_type", true),
                    "correct_answer" => get_post_meta( $post->ID, "_correct_answer", true),
                    "year" =>  get_post_meta( $post->ID, "_year", true),
                    "thumbnail" =>  get_the_post_thumbnail_url( $post->ID),
                    "rating" => floatval(intval(get_post_meta( $post->ID, "_rating", true))/intval($rating_submissions))
                );
                array_push($posts, $temp);
            }
            if(empty($posts))
            {
                echo '{"error":"not Found"}';
            }
            else
            {
                echo '{"data":'.json_encode($posts).',"current_page": "'.intval($_POST['page']).'","numberOfPages":"'.$the_query->max_num_pages.'"}';
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        } else {
            echo '{"error":"not Found"}';
        }
    } catch (Expection $e) {
        echo '{"error":"not vaild"}';
    }
    wp_die();
}
function EEaxms_editQuizz()
{
    global $current_user;
    if(isset($_POST['id']) && isset($_POST['title']))
    {
        $id = intval($_POST['id']);
        $post = get_post($id);
        if($post && $post->post_author == $current_user->ID)
        {
            $args = array(
                "ID" => $id,
                "post_title" => trim(strip_tags($_POST['title'])),
            );
            if(wp_update_post($args))
            {
                echo '{"success":"true"}';
                exit;
            }
        }
    }
}
function EEaxms_editQuizzSub()
{
    global $current_user;
    if(isset($_POST['id']) && isset($_POST['title']))
    {
        $id = intval($_POST['id']);
        $post = get_post($id);
        if($post && $post->post_author == $current_user->ID)
        {
            // $args = array(
            //     "ID" => $id,
            //     "post_title" => trim(strip_tags($_POST['title'])),
            // );
            update_post_meta( $id, 'subtitle', $_POST['title'] );
            if(wp_update_post())
            {
                echo '{"success":"true"}';
                exit;
            }
        }
    }
}

function EEaxms_get_question_details()
{
    $post = get_post($_POST['id']);
    $rating_submissions = intval(get_post_meta( $post->ID, "_rating_submissions", true));
    if($rating_submissions == 0)
    {
        $rating_submissions = 1;
    }
    $temp = array(
        "post" => $post,
        "subject" => get_the_title(intval(get_post_meta( $post->ID, "_subject", true ))),
        "grade" => get_the_title(intval(get_post_meta( $post->ID, "_grade_id", true ))),
        "level" => get_the_title(intval(get_post_meta( $post->ID, "_level_id", true ))),
        "points" => get_post_meta( $post->ID, "_points", true),
        "duration" => get_post_meta( $post->ID, "_duration", true),
        "hours" => get_post_meta( $post->ID, "_hours", true),
        "mins" => get_post_meta( $post->ID, "_mins", true),
        "answers" => get_post_meta( $post->ID, "_answers",true),
        "type" => get_post_meta( $post->ID, "_type", true),
        "correct_answer" => get_post_meta( $post->ID, "_correct_answer", true),
        "year" =>  get_post_meta( $post->ID, "_year", true),
        "thumbnail" =>  get_the_post_thumbnail_url( $post->ID),
        "rating" => floatval(floatval(get_post_meta( $post->ID, "_rating", true))/floatval($rating_submissions))
    );
    echo json_encode($temp);
    exit;
}

function EEaxms_report()
{
    global $current_user;
    if(isset($_POST['id']))
    {
        $id = intval($_POST['id']);

        $postarr = array(
            "post_type" => "eexams_reports",
            "post_title" => "Report",
            "post_content" => trim(strip_tags($_POST['msg'])),
            "post_parent" => $id,
            "meta_input" => array(
                "user_id" => get_current_user_id(),
                "question_id" => $id,
            ),
            "post_status" => "publish"

        );
        wp_insert_post($postarr);
        echo json_encode(array("true"));
        exit;
    }
}

function EEaxms_deleteQuizz()
{
    global $current_user;
    if(isset($_POST['id']))
    {
        $id = intval($_POST['id']);
        $post = get_post($id);
        if($post && $post->post_author == $current_user->ID)
        {
            if(wp_delete_post($id))
            {
                echo '{"success":"true"}';
                exit;
            }
        }
    }
}

function EEaxms_getQuizz()
{
    global $current_user;
    if(isset($_POST['id']))
    {
        $id = intval($_POST['id']);
        $post = get_post($id);
        if($post && $post->post_author == $current_user->ID)
        {
            $json = addslashes(json_encode(explode(",",get_post_meta( $post->ID, "question_ids", true ))));
            $answers = get_post_meta( $post->ID, "answers", true );
            $questions = get_post_meta( $post->ID, "question_contents", true );
            $box_toggle = get_post_meta( $post->ID, "box_toggle", true );
            $student_name_toggle = get_post_meta( $post->ID, "student_name_toggle", true );
            $student_name = get_post_meta( $post->ID, "student_name", true );
            $footer = get_post_meta( $post->ID, "footer", true );
            $footer_toggle = get_post_meta( $post->ID, "footer_toggle", true );
            $page_number_toggle = get_post_meta( $post->ID, "page_number_toggle", true );
            $subtitle = get_post_meta( $post->ID, "subtitle", true );

            $response = array (
                'success' => "true",
                'title' => $post->post_title,
                'json' => $json,
                'answers' => $answers,
                'questions' => $questions,
                'box_toggle' => $box_toggle,
                'student_name_toggle' => $student_name_toggle,
                'student_name' => $student_name,
                'footer' => $footer,
                'footer_toggle' => $footer_toggle,
                'page_number_toggle' => $page_number_toggle,
                'subtitle' => $subtitle,
            );

            echo json_encode($response);
            exit;
            echo '{"success":"true","ids":"'.$json.'","title":"'.$post->post_title.'","subtitle":"'.get_post_meta($post->ID,"subtitle",true).'","answers":"'.$answers.'", "questions":"' . $questions . '", "box_toggle": "' . $box_toggle . '", "student_name_toggle": "' . $student_name_toggle . '", "student_name": "' . $student_name . '", "footer": "' . $footer . '", "footer_toggle": "' . $footer_toggle . '", "page_number_toggle": "' . $page_number_toggle . '", "subtitle": "' . $subtitle . '"}';
            exit;
        }
    }
}

function EEaxms_UpdateQuestionRating()
{
    global $current_user;
    if(isset($_POST['id']) && isset($_POST['level']))
    {
        $id = intval($_POST['id']);
        $level = intval($_POST['level']);
        $post = get_post($id);
        if($post)
        {
            $currentRating = intval(get_post_meta($id, "_rating", true));
            update_post_meta($id, "_rating", $currentRating+$level);
            $numberOfsubmissions =  intval(get_post_meta($id, "_rating_submissions", true));
            update_post_meta($id, "_rating_submissions", ++$numberOfsubmissions);
            echo 1;
            exit;
        }
    }
} 

function EEaxms_SaveQuiz()
{
    if(!current_user_can( 'can_create_quizes' ) && !current_user_can( 'manage_options' ) ) { 
        echo json_encode(["error" => "yes"]);
        exit;
    }
    if(isset($_POST['save']) && $_POST['save'] == "true" && isset($_POST['title']) && isset($_POST['ids']))
    {
        $args = array(
            "post_title" => trim(strip_tags($_POST['title'])),
            "meta_input" => array(
                "question_ids" => implode(",",json_decode(stripslashes($_POST['ids']))),
                "question_contents" => str_replace('\n', '',strip_tags($_POST['questions'])),
                'box_toggle' => $_POST['boxToggle'],
                'student_name_toggle' => $_POST['studentNameToggle'],
                'student_name' => $_POST['studentName'],
                'footer' => $_POST['footer'],
                'footer_toggle' => $_POST['footerToggle'],
                'page_number_toggle' => $_POST['pageNumberToggle'],
                "answers" =>  trim(strip_tags($_POST['answers'])),
                "subtitle" => trim(strip_tags($_POST['subTitle'])),
                "user_id" => get_current_user_id()
            ),
            "post_type" => "eexams_quizzes",
            "post_status" => "publish",
            "post_author" => get_current_user_id()
        );
        if(isset($_GET['SaveEdit']) && $_GET['SaveEdit'] == "true" )
        {
            $args['ID'] = intval($_GET['id']);
            $post_author_id = get_post_field( 'post_author', intval($_GET['id']) );
            if(intval($post_author_id) !== intval(get_current_user_id()))
            {
                //handle redirect to show quizzes page
                echo json_encode(["error" => "yes"]);
                exit;
            }
        }
        if($_POST['quizz_id'] <> 0){
            $url = home_url().'/?edit='.$_POST['quizz_id'];
            $args['ID'] = $_POST['quizz_id'];
            wp_update_post( $args );
            echo json_encode(["success" => true, "data" => $_POST, "edit" => $_POST['edit'], "quizz_id" => $_POST['quizz_id'],"redirect" => $url]);
            exit;
        } else {
            if($quizz_id = wp_insert_post($args))
            {
                $url = home_url().'/?edit='.$quizz_id;
                //handle redirect to show quizzes page 
                echo json_encode(["success" => true,"redirect" => $url]);
                exit;
            }
        }

    }
}