Array.prototype.inArray = function (comparer) {
	for (var i = 0; i < this.length; i++) {
		if (comparer(this[i])) return i;
	}
	return false;
};

// adds an element to the array if it does not already exist using a comparer
// function
Array.prototype.pushOrUpdate = function (element, comparer) {
	var i = this.inArray(comparer);
	if (i === false) {
		this.push(element);
	} else {
		this[i] = element;
	}
};

var filterList = [];
var dataSet = [];
var numberOfpages = 0;
var current_page = 0;
var past_page = 1;
var selectedQuestions = [];
var totalNumberOfPages = 0;
var title = "";
var subtitle = "";
var edit = false;
var quizz_id = 0;

// Configure/customize these variables.
var showChar = 100; // How many characters are shown by default
var ellipsestext = "...";
var moretext = "Show more >";
var lesstext = "Show less";

var $pagination = jQuery('#paginationUl');
$pagination.twbsPagination({
	totalPages:1,
	startPage:1,
});


function initEdit() {
	if (EExams.edit.hasOwnProperty("selected_questions")) {
		//TODO: redo this after other changes
		EExams.edit.selected_questions.forEach(question => {
			selectedQuestions.push(question.post.ID.toString());
			var html =
				'<div style="cursor:default" class="row question" id="question-' +
				question.post.ID +
				'"><div class="col-3 no-gutters zero-padding"><img src="' + question.thumbnail + '" class="align-middle img-fluid" alt=""><p class="view" id="view-' +
				question.post.ID +
				'" style="display:none;font-size: 12px;">Full Text</p><p class="report" style="display:none;font-size: 12px;"><i class="far fa-flag"></i><br>report</p></div><div class="col-9 no-gutters"><h4>' +
				question.post.post_title +
				"</h4><p>" +
				question.grade +
				" • " +
				question.subject +
				" • " +
				question.hours +
				" h, " +
				question.mins +
				" m • " +
				question.level +
				" • " +
				question.year +
				" </p><div class='QuestionContent'>" +
				question.post.post_content +
				"</div><span style='cursor:pointer' id='questionS-" + (selectedQuestions.length - 1) + "' class='remove select'><i class='far fa-trash-alt'></i></span><span class='move'><i class='fas fa-arrows-alt'></i></span></div></div>";
			jQuery(".questionsSelectedTo").append(html);
			jQuery('.QuestionContent').readmore({
				collapsedHeight: 190,
				moreLink: '<a href="#" class="readmore">Read more</a>',
				lessLink: '<a href="#" class="readmore">Show less</a>'
			});
		});
		title = EExams.edit.title;
		subtitle = EExams.edit.subtitle;
		edit = true;
		quizz_id = EExams.edit.id;
	}
}

(function ($) {
	//display the questions
	dataSet = JSON.parse(EExams.posts);
	numberOfpages = parseInt(EExams.numberOfPages);
	current_page = parseInt(EExams.current_page);
	totalNumberOfPages = numberOfpages;

	function init() {
		$pagination.twbsPagination('destroy');
	
		$pagination.twbsPagination(jQuery.extend({}, {
			startPage: 1,
			totalPages: numberOfpages,
			onPageClick: function (evt, page) {
				$(this).addClass("active");
				current_page = parseInt(page);
				paginate();
			}
		}));
	}

	init();
	display();
	initEdit();
	$(".pagination").on("click", ".page-item-1", function (event) {
		event.preventDefault();
		if ($(this).attr("id") == "back") {
			if (current_page == 1) {
				return;
			}
			$(".overlay").fadeIn();
			var active = $(".active").removeClass('active');
			if (active.prev() && active.prev().length) {
				active.prev().addClass('active');
			}
			current_page--;
			paginate();
		} else if ($(this).attr("id") == "next") {
			if (current_page == numberOfpages) {
				return;
			}
			$(".overlay").fadeIn();
			var active = $(".active").removeClass('active');
			if (active.next() && active.next().length) {
				active.next().addClass('active');
			}
			current_page++;
			paginate();
		} else {
			$(".overlay").fadeIn();
			$(".page-item").removeClass("active");
			$(this).addClass("active");
			current_page = parseInt($(this).attr("id").substring(5));
			paginate();
		}
	});

	$('#sticky').on("change", function () {
		// this will contain a reference to the checkbox   
		if (this.checked) {
			$("#filters-area").addClass("sticky");
		} else {
			$("#filters-area").removeClass("sticky");
		}
	});

	//Clear search Input
	$(".clearSearch").on("click", function () {
		$("#search").val("");
		$("#search").trigger("change");
	});


	$(".questionsSelectedFrom").on("click", ".report", async function (e) {
		e.stopPropagation();
		console.log("report");
		var id = $(this)
			.attr("id")
			.substring(7);
		const { value: issue } = await Swal.fire({
			title: 'Please Expain The issue',
			input: 'text',
		})

		if (issue) {
			$.ajax({
				type: "post",
				url: EExams.ajaxurl,
				data: {
					action: "report",
					id: id,
					msg: issue,
				},
				dataType: "json",
				success: function (response) {
					console.log(response);
					Swal.fire(
						'Thanks for your feedback',
						'',
						'success'
					)
				}
			});
		}
		return false;
	});

	$(".questionsSelectedFrom").on("click", ".view", async function (e) {
		e.stopPropagation();
		var id = $(this)
			.attr("id")
			.substring(5);
		console.log(id);
		$.ajax({
			type: "post",
			url: EExams.ajaxurl,
			data: {
				action: "get_question_details",
				id: id,
			},
			dataType: "json",
			success: function (response) {
				console.log(response);
				var question = response;
				var html =
					'<div class="bootstrap-wrapper"><div class="container-fluid"><div class="row question"><div class="col-12 no-gutters"><h4>' +
					question.post.post_title +
					"</h4><p class='question-sub'>" +
					question.grade +
					" • " +
					question.subject +
					" • " +
					question.hours +
					" h, " +
					question.mins +
					" m • " +
					question.level +
					" • " +
					question.year +
					" </p><select id='starview" + question.post.ID + "'><option value=''></option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select><div class='question-content-wrapper'><p class='question-content'>" +
					question.post.post_content +
					"</p></div></div></div></div>";
				Swal.fire({
					customClass: "question-view",
					html: html,
					showCloseButton: true,
					showCancelButton: false,
					focusConfirm: false,
					showConfirmButton: false,
					onRender: function () {
						$('#starview' + question.post.ID).barrating({
							theme: 'fontawesome-stars-o',
							initialRating: question.rating,
							allowEmpty: true,
							onSelect: function (value, text, event) {
								if (typeof (event) !== 'undefined') {
									var id = $(this.$elem).attr("id").substring(8);
									$.ajax({
										type: "post",
										url: EExams.ajaxurl,
										data: {
											action: "rate",
											id: id,
											level: value,
										},
										dataType: "json",
										success: function (response) {
											console.log(response);
										}
									});
									console.log(event.target);
								} else {
									// rating was selected programmatically
									// by calling `set` method
								}
							}
						});
					},
					width: 800,
				})
			}
		});

		return false;
	});

	$(".questionsSelectedFrom").on("click", ".question", function () {
		$(this).appendTo(".questionsSelectedTo");
		$(this).find(".arrow").remove();
		$(this).find(".report").hide();
		$(this).find(".view").hide();
		$(this).find(".col-3").remove();
		$(this).find(".col-9").addClass("col-12")
		$(this).find(".col-9").removeClass("col-9")
		$(this).css("cursor", "default");
		var id = $(this)
			.attr("id")
			.substring(9);
		selectedQuestions.push(id);
		$(this)
			.find(".move")
			.show()
		$(this)
			.find(".remove")
			.show()
			.css("cursor", "pointer")
			.attr("id", "questionS-" + (selectedQuestions.length - 1));
		$(".overlay").fadeIn();
		if (parseInt(current_page) == parseInt(numberOfpages)) {
			if ((selectedQuestions.length >= parseInt(EExams.number_of_questions_per_page) && numberOfpages == totalNumberOfPages) || (dataSet.length % parseInt(EExams.number_of_questions_per_page) != 0) || (selectedQuestions.length % parseInt(EExams.number_of_questions_per_page) == 0 && numberOfpages <= totalNumberOfPages)) {
				Update(parseInt(current_page) - 1, true);
			}
			else {
				Update(parseInt(current_page), true);
			}
		}
		else {
			Update(parseInt(current_page), true);
		}
	});

	$(".questionsSelectedTo").sortable();

	$(".questionsSelectedTo").on("click", ".remove", function () {
		var id = $(this).closest(".row.question")
			.attr("id")
			.substring(9);
		var index = selectedQuestions.indexOf(id);
		selectedQuestions.splice(index, 1);
		$(this)
			.closest(".question")
			.remove();
		Update(current_page, true);
	});

	//subject
	$(".items").on("click", ".subject", function () {
		if (!$(this).hasClass("active")) {
			try {
				var json = JSON.parse($("#subjectInput").val());
			} catch (e) {
				var json = [];
			}
			json.push($(this).attr("id"));
			$("#subjectInput").val(JSON.stringify(json));
			$(this).addClass("active");

		} else {
			var json = JSON.parse($("#subjectInput").val());
			var index = json.indexOf($(this).attr("id"));
			if (index !== -1) json.splice(index, 1);
			$("#subjectInput").val(JSON.stringify(json));
			$(this).removeClass("active");

		}
		$("#subjectInput").trigger("change");
	});

	//grade
	$(".items").on("click", ".grade", function () {
		if (!$(this).hasClass("active")) {
			try {
				var json = JSON.parse($("#gradeInput").val());
			} catch (e) {
				var json = [];
			}
			json.push($(this).attr("id"));
			$("#gradeInput").val(JSON.stringify(json));
			$(this).addClass("active");
		} else {
			var json = JSON.parse($("#gradeInput").val());
			var index = json.indexOf($(this).attr("id"));
			if (index !== -1) json.splice(index, 1);
			$("#gradeInput").val(JSON.stringify(json));
			$(this).removeClass("active");

		}
		$("#gradeInput").trigger("change");
	});

	//tag
	$(".items").on("click", ".tag", function () {
		if (!$(this).hasClass("active")) {
			try {
				var json = JSON.parse($("#tagInput").val());
			} catch (e) {
				var json = [];
			}
			json.push($(this).attr("id"));
			$("#tagInput").val(JSON.stringify(json));
			$(this).addClass("active");
		} else {
			var json = JSON.parse($("#tagInput").val());
			var index = json.indexOf($(this).attr("id"));
			if (index !== -1) json.splice(index, 1);
			$("#tagInput").val(JSON.stringify(json));
			$(this).removeClass("active");

		}
		$("#tagInput").trigger("change");
	});

	//level
	$(".items").on("click", ".level", function () {
		if (!$(this).hasClass("active")) {
			try {
				var json = JSON.parse($("#levelInput").val());
			} catch (e) {
				var json = [];
			}
			json.push($(this).attr("id"));
			$("#levelInput").val(JSON.stringify(json));
			$(this).addClass("active");
		} else {
			var json = JSON.parse($("#levelInput").val());
			var index = json.indexOf($(this).attr("id"));
			if (index !== -1) json.splice(index, 1);
			$("#levelInput").val(JSON.stringify(json));
			$(this).removeClass("active");

		}
		$("#levelInput").trigger("change");
	});

	//points
	$(".items").on("click", ".point", function () {
		if (!$(this).hasClass("active")) {
			try {
				var json = JSON.parse($("#pointInput").val());
			} catch (e) {
				var json = [];
			}
			json.push($(this).data("value"));
			$("#pointInput").val(JSON.stringify(json));
			$(this).addClass("active");
		} else {
			var json = JSON.parse($("#pointInput").val());
			var index = json.indexOf($(this).data("value"));
			if (index !== -1) json.splice(index, 1);
			$("#pointInput").val(JSON.stringify(json));
			$(this).removeClass("active");

		}
		$("#pointInput").trigger("change");
	});

	//duration
	$(".items").on("click", ".duration", function () {
		if (!$(this).hasClass("active")) {
			try {
				var json = JSON.parse($("#durationInput").val());
			} catch (e) {
				var json = [];
			}
			json.push(JSON.stringify($(this).data("value")));
			$("#durationInput").val(JSON.stringify(json));
			$(this).addClass("active");
		} else {
			var json = JSON.parse($("#durationInput").val());
			var index = json.indexOf(JSON.stringify($(this).data("value")));
			if (index !== -1) json.splice(index, 1);
			$("#durationInput").val(JSON.stringify(json));
			$(this).removeClass("active");
		}
		$("#durationInput").trigger("change");
	});

	//clear
	$("#clear").on("click", function (event) {
		event.preventDefault()
		$(".active").removeClass("active");
		filterList = [];
		$(".filter-input").val("");
		Update(current_page, true);
	});

	var less = false;

	//showless
	$("#less").on("click", function (event) {
		event.preventDefault()
		if (less) {
			$(".less").fadeIn();
			$(this).html("Show Less");
			less = false;
		} else {
			$(".less").fadeOut();
			$(this).html("Show More");
			less = true;
		}
	});

	//finsih
	$("#finish").on("click", function () {
		if (edit) {
			var ids = [];
			selectedQuestions.forEach(function (element) {
				ids.push(element);
			});
			var answers = $("input[name='answers']:checked").val();
			var url = window.location.href;
			if (url.indexOf("?") > -1) {
				url += "&answers=" + answers;
			} else {
				url += "?answers=" + answers;
			}
			url += "&pdfq=" + EExams.pdfq + "&title=" + title + "&subtitle=" + subtitle + "&ids=" + JSON.stringify(ids);
			$("<div class='modal'><iframe src='"+url+"'></iframe></div>").appendTo('body').modal();
		}
		else
		{
			Swal.fire({
				showCloseButton: true,
				title: 'Export Question',
				html:
					'<input id="title" placeholder="Title" class="swal2-input">' +
					'<input id="subtitle" placeholder="Subtitle" class="swal2-input">',
				preConfirm: function () {
					return new Promise(function (resolve) {
						resolve([
							$('#title').val(),
							$('#subtitle').val()
						])
					})
				},
				onOpen: function () {
					$('#title').focus()
				}
			}).then(function (result) {
				var ids = [];
				selectedQuestions.forEach(function (element) {
					ids.push(element);
				});
				var answers = $("input[name='answers']:checked").val();
				var url = window.location.href;
				if (url.indexOf("?") > -1) {
					url += "&answers=" + answers;
				} else {
					url += "?answers=" + answers;
				}
				url += "&pdfq=" + EExams.pdfq + "&title=" + result.value[0] + "&subtitle=" + result.value[1] + "&ids=" + JSON.stringify(ids);
				$("<div class='modal'><iframe src='"+url+"'></iframe></div>").appendTo('body').modal();
			});
		}

	});
	$("#SaveAs").on("click", function () {
		Swal.fire({
			showCloseButton: true,
			title: 'Save Question',
			html:
				'<input id="title" placeholder="Title" class="swal2-input">' +
				'<input id="subtitle" placeholder="Subtitle" class="swal2-input">',
			preConfirm: function () {
				return new Promise(function (resolve) {
					resolve([
						$('#title').val(),
						$('#subtitle').val()
					])
				})
			},
			onOpen: function () {
				$('#title').focus()
			}
		}).then(function (result) {
			var ids = [];
			selectedQuestions.forEach(function (element) {
				ids.push(element);
			});
			var answers = $("input[name='answers']:checked").val();
			var url = window.location.href;
			if (url.indexOf("?") > -1) {
				url += "&answers=" + answers;
			} else {
				url += "?answers=" + answers;
			}
			if (result.value[0] === "") {
				return;
			}
			url += "&save=true&title=" + result.value[0] + "&subtitle=" + result.value[1] + "&ids=" + JSON.stringify(ids);
			fetch(url + "&action=saveQuiz").then(response => response.json()).then(data => {
				console.log(data);
				if (data.hasOwnProperty("success") && data.success === "yes") {
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						title: 'Quiz Saved!',
						showConfirmButton: false,
						timer: 500
					})
				}
				else {
					Swal.fire(
						'Error',
						'There was an error while saving the quiz',
						'success'
					);
				}
			});
		}).catch(swal.noop)
	});
	//save
	$("#Save").on("click", function () {
		if (edit) {
			var ids = [];
			selectedQuestions.forEach(function (element) {
				ids.push(element);
			});
			var answers = $("input[name='answers']:checked").val();
			var url = EExams.ajaxurl;
			if (url.indexOf("?") > -1) {
				url += "&answers=" + answers;
			} else {
				url += "?answers=" + answers;
			}
			url += "&save=true&SaveEdit=true&id=" + parseInt(quizz_id) + "&title=" + title + "&subtitle=" + subtitle + "&ids=" + JSON.stringify(ids);
			fetch(url + "&action=saveQuiz").then(response => response.json()).then(data => {
				console.log(data);
				if (data.hasOwnProperty("success") && data.success === "yes") {
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						title: 'Quiz Saved!',
						showConfirmButton: false,
						timer: 800
					})
				}
				else {
					Swal.fire(
						'Error',
						'There was an error while saving the quiz',
						'error'
					);
				}
			});
		}
		else
		{
			Swal.fire({
				showCloseButton: true,
				title: 'Save Question',
				html:
					'<input id="title" placeholder="Title" class="swal2-input" value="'+title+'">' +
					'<input id="subtitle" placeholder="Subtitle" class="swal2-input" value="'+subtitle+'">',
				preConfirm: function () {
					return new Promise(function (resolve) {
						resolve([
							$('#title').val(),
							$('#subtitle').val()
						])
					})
				},
				onOpen: function () {
					$('#title').focus()
				}
			}).then(function (result) {
				var ids = [];
				selectedQuestions.forEach(function (element) {
					ids.push(element);
				});
				var answers = $("input[name='answers']:checked").val();
				var url = EExams.ajaxurl;
				if (url.indexOf("?") > -1) {
					url += "&answers=" + answers;
				} else {
					url += "?answers=" + answers;
				}
				if (result.value[0] === "") {
					return;
				}
				url += "&save=true&title=" + result.value[0] + "&subtitle=" + result.value[1] + "&ids=" + JSON.stringify(ids);
				fetch(url + "&action=saveQuiz").then(response => response.json()).then(data => {
					console.log(data);
					if (data.hasOwnProperty("success") && data.success === "yes") {
						Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'Quiz Saved!',
							showConfirmButton: false,
							timer: 800
						})
					}
					else {
						Swal.fire(
							'Error',
							'There was an error while saving the quiz',
							'error'
						);
					}
				});
			}).catch(swal.noop)
		}
	});

	//Config
	var ajaxCallUrl = EExams.ajaxurl;
	var action = "filterQuestions";
	//Config

	//filter Input
	$(".filter-input").on("change", function () {
		

		//Restrict only one rating selection
		var check_length = $(this).val();
		var rating_count = $(check_length).text().split(',');
		alert( rating_count[2] );
		if (rating_count.length >= 2) {
			console.log("rating_count is 2 or more");
		}


		var name = $(this).attr("name");
		var val = $(this).val();
		var element = {
			name: name,
			value: val
		};
		filterList.pushOrUpdate(element, function (e) {
			return e.name === element.name;
		});
		Update(1, true);
	});

	function Update(page, filter) {
		$.ajax({
			type: "post",
			url: ajaxCallUrl,
			data: {
				filter: filterList,
				action: action,
				page: page,
				selectedQuestions: selectedQuestions,
			},
			dataType: "json",
			success: function (response) {
				if (response.hasOwnProperty("error")) {
					$(".questionsSelectedFrom").empty();
					$("#pagination").hide();
					$(".questionsSelectedFrom").append("<div class='no-results'>There is no results matching your search</div>")
					$(".overlay").fadeOut();
					return;
				}
				dataSet = response.data;
				current_page = response.current_page;
				console.log(current_page);
				if (filter) {
					numberOfpages = response.numberOfPages;
					init();
					$("#pagination").show();
					display();
				} else {
					display();
				}
			}
		});
	}

	function display() {
		$(".questionsSelectedFrom").empty();
		for (var i = 0; i < dataSet.length; i++) {
			var question = dataSet[i];
			if (question === "") continue;
			if (!question.thumbnail) {
				question.thumbnail = "";
			}
			var html =
				'<div class="row question" id="question-' +
				question.post.ID +
				'"><div class="col-3 no-gutters zero-padding"><img src="' + question.thumbnail + '" class="align-middle img-fluid" alt=""><p class="report" id="report-' +
				question.post.ID +
				'" style="font-size: 12px;"><i class="far fa-flag"></i><br>report</p><p class="view" id="view-' +
				question.post.ID +
				'" style="font-size: 12px;">Full Text</p></div><div class="col-9 no-gutters"><h4>' +
				question.post.post_title +
				"</h4><p>" +
				question.grade +
				" • " +
				question.subject +
				" • " +
				question.hours +
				" h, " +
				question.mins +
				" m • " +
				question.level +
				" • " +
				question.year +
				" </p><select  id='star" + question.post.ID + "'><option value=''></option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select><div class='QuestionContent'>" +
				question.post.post_content +
				"</div><span style='display:none' class='remove select'><i class='far fa-trash-alt'></i></span><span style='display:none' class='move'><i class='fas fa-arrows-alt'></i></span></div><span class='arrow'><i class='fas fa-arrow-right'></i></span></div>";
			$(".questionsSelectedFrom").append(html);
			$('.QuestionContent').readmore({
				collapsedHeight: 190,
				moreLink: '<a href="#" class="readmore">Read more</a>',
				lessLink: '<a href="#" class="readmore">Show less</a>'
			});
			$('#star' + question.post.ID).barrating({
				theme: 'fontawesome-stars-o',
				initialRating: question.rating,
				allowEmpty: true,
				onSelect: function (value, text, event) {
					if (typeof (event) !== 'undefined') {
						var id = $(this.$elem).attr("id").substring(4);
						$.ajax({
							type: "post",
							url: EExams.ajaxurl,
							data: {
								action: "rate",
								id: id,
								level: value,
							},
							dataType: "json",
							success: function (response) {
								console.log(response);
							}
						});
						console.log(event.target);
					} else {
						// rating was selected programmatically
						// by calling `set` method
					}
				}
			});

		}
		MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
		MathJax.Hub.Queue(inlineMath);
	}
	var inlineMath = function () {
		var existingStyles = $(".MathJax_Display").attr("style");
		$(".MathJax_Display").attr("style", existingStyles + "display:inline!important");
		$(".overlay").fadeOut();
	}

	function paginate() {
		if (current_page != past_page) {
			past_page = current_page;
			Update(current_page, false);
		}
		display();
	}

})(jQuery); 