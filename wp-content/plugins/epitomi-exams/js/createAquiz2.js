Array.prototype.inArray = function (comparer) {
    for (var i = 0; i < this.length; i++) {
        if (comparer(this[i])) return i;
    }
    return false;
};

// adds an element to the array if it does not already exist using a comparer
// function
Array.prototype.pushOrUpdate = function (element, comparer) {
    var i = this.inArray(comparer);
    if (i === false) {
        this.push(element);
    } else {
        this[i] = element;
    }
};

var filterList = [];
var dataSet = [];
var dataSet_selected = [];
var numberOfpages = 0;
var current_page = 0;
var past_page = 1;
var selectedQuestions = [];
var totalNumberOfPages = 0;
var title = "";
var subtitle = "";
var student_name = "";
var footer = "";
var edit = false;
var quizz_id = 0;

// toggles
var box_toggle = false;
var student_name_toggle = false;
var page_number_toggle = false;
var footer_toggle = false;

//Config
var ajaxCallUrl = EExams.ajaxurl;
var action = "filterQuestions_updated";
//Config

var $pagination = jQuery('#paginationUl');
$pagination.twbsPagination({
    totalPages: 1,
    startPage: 1,
});

(function ($) {
    initEdit();
    var p = 1;
    if (window.location.hash) {
        p = parseInt(window.location.hash.substring(6, 7));
        console.log(p);
    }
    Update(p, true, true);

    function init() {
        $pagination.twbsPagination('destroy');

        $pagination.twbsPagination(jQuery.extend({}, {
            startPage: parseInt(current_page),
            totalPages: parseInt(numberOfpages),
            onPageClick: function (evt, page) {
                $(this).addClass("active");
                window.location.hash = 'page-' + page;
                current_page = parseInt(page);
                paginate();
            }
        }));
    }

    function initEdit() {
        if (EExams.edit.hasOwnProperty("selected_questions")) {
            //TODO: redo this after other changes
            console.log(EExams.edit);
            EExams.edit.selected_questions.forEach(question => {
                dataSet_selected.push(question);
            selectedQuestions.push(question.id.toString());
            var html =
                `<div class="row question" id="question-${question.id}">
						<div class="col-12 no-gutters">
							<h3>${question.title}  <span class="question-id">#${question.id}</span></h3>
							<p>${question.grade} • ${question.subject} • ${question.hours} h, ${question.mins} m • ${question.level} • ${question.year}</p>
							<select class='star${question.id}'>
								<option value=''></option>
								<option value='1'>1</option>
								<option value='2'>2</option>
								<option value='3'>3</option>
								<option value='4'>4</option>
								<option value='5'>5</option>
							</select>
							<div class='QuestionContent'>
								${question.body} 
							</div>
						</div>
	
						<span class="move" style="font-size: 12px; display:block">Reorder <i class="fas fa-arrows-alt"></i></span>
						<span class="remove" style="font-size: 12px; display:block">Remove <i class="fas fa-trash"></i></span>
	
					</div>`;
            $(".questionsSelectedTo").append(html);
            $('.QuestionContent').readmore({
                collapsedHeight: 150,
                moreLink: '<a href="#" class="readmore">Expand <i class="fas fa-angle-down"></i></a>',
                lessLink: '<a href="#" class="readmore">Show less <i class="fas fa-angle-up"></i></a>'
            });
            $('.star' + question.id).barrating({
                theme: 'fontawesome-stars-o',
                initialRating: question.rating,
                allowEmpty: true,
                onSelect: function (value, text, event) {
                    if (typeof (event) !== 'undefined') {
                        var id = $(this.$elem).attr("class").substring(4);
                        $.ajax({
                            type: "post",
                            url: EExams.ajaxurl,
                            data: {
                                action: "rate_updated",
                                id: id,
                                level: value,
                            },
                            dataType: "json",
                            success: function (response) {
                                console.log(response);
                            }
                        });
                        console.log(event.target);
                    } else {
                        // rating was selected programmatically
                        // by calling `set` method
                    }
                }
            });
        });
            title = EExams.edit.title;
            subtitle = EExams.edit.subtitle;
            edit = true;
            quizz_id = EExams.edit.id;
            if ( edit == true ) {
                box_toggle = EExams.edit.box_toggle;
                student_name_toggle = EExams.edit.student_name_toggle;
                page_number_toggle = EExams.edit.page_number_toggle;
                footer_toggle = EExams.edit.footer_toggle;
                
                student_name = EExams.edit.student_name;
                footer = EExams.edit.footer;
            }

            if (selectedQuestions.length) {
                $(".top-button").removeAttr('disabled');
                $(".end-button").show();
                $(".your-selection-container").show();
            }
            else {
                $(".top-button").attr('disabled', 'disabled');
                $(".end-button").hide();
                $(".your-selection-container").hide();
            }
            calculate_selection();
        }
    }

    $(".pagination").on("click", ".page-item-1", function (event) {
        event.preventDefault();
        if ($(this).attr("id") == "back") {
            if (current_page == 1) {
                return;
            }
            $(".overlay").fadeIn();
            var active = $(".active").removeClass('active');
            if (active.prev() && active.prev().length) {
                active.prev().addClass('active');
            }
            current_page--;
            paginate();
        } else if ($(this).attr("id") == "next") {
            if (current_page == numberOfpages) {
                return;
            }
            $(".overlay").fadeIn();
            var active = $(".active").removeClass('active');
            if (active.next() && active.next().length) {
                active.next().addClass('active');
            }
            current_page++;
            paginate();
        } else {
            $(".overlay").fadeIn();
            $(".page-item").removeClass("active");
            $(this).addClass("active");
            current_page = parseInt($(this).attr("id").substring(5));
            paginate();
        }
    });

    $('#sticky').on("change", function () {
        // this will contain a reference to the checkbox
        if (this.checked) {
            $("#filters-area").addClass("sticky");
        } else {
            $("#filters-area").removeClass("sticky");
        }
    });

    //Clear search Input
    $(".clearSearch").on("click", function () {
        $("#search").val("");
        $("#search").trigger("change");
    });

    $(".questionsSelectedFrom").on("click", ".report", async function (e) {
        e.stopPropagation();
        console.log("report");
        var id = $(this)
            .attr("id")
            .substring(7);
        const { value: issue } = await Swal.fire({
            title: 'Please Expain The issue',
            input: 'text',
        })

        if (issue) {
            $.ajax({
                type: "post",
                url: EExams.ajaxurl,
                data: {
                    action: "EEaxms_report",
                    id: id,
                    msg: issue,
                },
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    Swal.fire({
                        icon: 'success',
                        title: 'Thanks for your feedback',
                        showConfirmButton: false,
                        timer: 700,
                    })
                }
            });
        }
        return false;
    });

    $(".questionsSelectedFrom").on("click", ".view", async function (e) {
        e.stopPropagation();
        var id = $(this)
            .attr("id")
            .substring(5);
        console.log(id);
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                action: "get_question_details_updated",
                id: id,
            },
            dataType: "json",
            success: function (response) {
                console.log(response);
                var question = response;
                var html =
                    '<div class="bootstrap-wrapper"><div class="container-fluid"><div class="row question"><div class="col-12 no-gutters"><h4>' +
                    question.title +
                    "</h4><p class='question-sub'>" +
                    question.grade +
                    " • " +
                    question.subject +
                    " • " +
                    question.hours +
                    " h, " +
                    question.mins +
                    " m • " +
                    question.level +
                    " • " +
                    question.year +
                    " </p><select id='starview" + question.id + "'><option value=''></option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option></select><div class='question-content-wrapper'><p class='question-content'>" +
                    question.body +
                    "</p></div></div></div></div>";
                Swal.fire({
                    customClass: "question-view",
                    html: html,
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    showConfirmButton: false,
                    onRender: function () {
                        $('#starview' + question.id).barrating({
                            theme: 'fontawesome-stars-o',
                            initialRating: question.rating,
                            allowEmpty: true,
                            onSelect: function (value, text, event) {
                                if (typeof (event) !== 'undefined') {
                                    var id = $(this.$elem).attr("id").substring(8);
                                    $.ajax({
                                        type: "post",
                                        url: EExams.ajaxurl,
                                        data: {
                                            action: "rate",
                                            id: id,
                                            level: value,
                                        },
                                        dataType: "json",
                                        success: function (response) {
                                            console.log(response);
                                        }
                                    });
                                    console.log(event.target);
                                } else {
                                    // rating was selected programmatically
                                    // by calling `set` method
                                }
                            }
                        });
                    },
                    width: 800,
                })
            }
        });

        return false;
    });

    $(".questionsSelectedFrom").on("click", ".question", function () {
        $(this).appendTo(".questionsSelectedTo");
        $(this).find(".arrow").remove();
        $(this).find(".report").remove();
        $(this).find(".view").remove();
        $(this).css("cursor", "default");
        var id = $(this)
            .attr("id")
            .substring(9);
        selectedQuestions.push(id);
        $(this)
            .find(".move")
            .show()
        $(this)
            .find(".remove")
            .show()
            .css("cursor", "pointer")
            .attr("id", "questionS-" + (selectedQuestions.length - 1));
        $(".overlay").fadeIn();
        if (parseInt(current_page) == parseInt(numberOfpages)) {
            if ((selectedQuestions.length >= parseInt(EExams.number_of_questions_per_page) && numberOfpages == totalNumberOfPages)
                || (selectedQuestions.length % parseInt(EExams.number_of_questions_per_page) == 0 && numberOfpages <= totalNumberOfPages)
                || (dataSet.length - 1 === 0 && current_page > 1 && numberOfpages > 1)) {
                Update(parseInt(current_page) - 1, true);
            }
            else {
                Update(parseInt(current_page), true);
            }
        }
        else {
            Update(parseInt(current_page), true);
        }

        if (selectedQuestions.length) {
            $(".top-button").removeAttr('disabled');
            $(".end-button").show();
            $(".your-selection-container").show();
        }
        else {
            $(".top-button").attr('disabled', 'disabled');
            $(".end-button").hide();
            $(".your-selection-container").hide();
        }

        calculate_selection_add(id);
    });

    $(".questionsSelectedTo").sortable({
        update: function(){
            var temp = selectedQuestions;
            selectedQuestions = [];
            $(".questionsSelectedTo .question").each(function (indexInArray, valueOfElement) {
                selectedQuestions.push($(this).attr("id").substring(9));
            });
        }
    });



    $(".questionsSelectedTo").on("click", ".remove", function () {
        var id = $(this).closest(".row.question")
            .attr("id")
            .substring(9);
        var index = selectedQuestions.indexOf(id);
        selectedQuestions.splice(index, 1);
        $(this)
            .closest(".question")
            .remove();
        Update(current_page, true);
        if (selectedQuestions.length) {
            $(".top-button").removeAttr('disabled');
            $(".end-button").show();
            $(".your-selection-container").show();
        }
        else {
            $(".top-button").attr('disabled', 'disabled');
            $(".end-button").hide();
            $(".your-selection-container").hide();
        }
        calculate_selection_remove(id);
    });

    //NEW FILTERS
    $(document).on("click", ".grade", function () {
        if (!$(this).hasClass("active")) {
            try {
                var json = JSON.parse($("#gradeInput").val());
            } catch (e) {
                var json = [];
            }
            console.log(json);
            json.push($(this).data("filter"));
            $("#gradeInput").val(JSON.stringify(json));
            $(this).addClass("active");

        } else {
            var json = JSON.parse($("#gradeInput").val());
            var index = json.indexOf($(this).data("filter"));
            if (index !== -1) json.splice(index, 1);
            $("#gradeInput").val(JSON.stringify(json));
            $(this).removeClass("active");

        }
        $("#gradeInput").trigger("change");
    });

    $(document).on("click", ".grade-load-more", function () {
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                action: "EExams_load_more_grades"
            },
            success: function (response) {
                $(".grade").remove();
                $(".grade-body").prepend(response);
                $(".grade-footer").remove();
            }
        });
    });


    $(document).on("click", ".subject", function () {
        if (!$(this).hasClass("active")) {
            try {
                var json = JSON.parse($("#subjectInput").val());
            } catch (e) {
                var json = [];
            }
            json.push($(this).data("filter"));
            $("#subjectInput").val(JSON.stringify(json));
            $(this).addClass("active");

        } else {
            var json = JSON.parse($("#subjectInput").val());
            var index = json.indexOf($(this).data("filter"));
            if (index !== -1) json.splice(index, 1);
            $("#subjectInput").val(JSON.stringify(json));
            $(this).removeClass("active");

        }
        $("#subjectInput").trigger("change");
    });

    $(document).on("click", ".subject-load-more", function () {
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                action: "EExams_load_more_subjects"
            },
            success: function (response) {
                $(".subject").remove();
                $(".subject-body").prepend(response);
                $(".subject-footer").remove();
            }
        });
    });

    $(document).on("click", ".tag", function () {
        if (!$(this).hasClass("active")) {
            try {
                var json = JSON.parse($("#tagInput").val());
            } catch (e) {
                var json = [];
            }
            json.push($(this).data("filter"));
            $("#tagInput").val(JSON.stringify(json));
            $(this).addClass("active");

        } else {
            var json = JSON.parse($("#tagInput").val());
            var index = json.indexOf($(this).data("filter"));
            if (index !== -1) json.splice(index, 1);
            $("#tagInput").val(JSON.stringify(json));
            $(this).removeClass("active");

        }
        $("#tagInput").trigger("change");
    });

    $(document).on("click", ".tag-load-more", function () {
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                action: "EExams_load_more_tags"
            },
            success: function (response) {
                $(".tag").remove();
                $(".tag-body").prepend(response);
                $(".tag-footer").remove();
            }
        });
    });

    $(document).on("click", ".level", function () {
        if (!$(this).hasClass("active")) {
            try {
                var json = JSON.parse($("#levelInput").val());
            } catch (e) {
                var json = [];
            }
            json.push($(this).data("filter"));
            $("#levelInput").val(JSON.stringify(json));
            $(this).addClass("active");

        } else {
            var json = JSON.parse($("#levelInput").val());
            var index = json.indexOf($(this).data("filter"));
            if (index !== -1) json.splice(index, 1);
            $("#levelInput").val(JSON.stringify(json));
            $(this).removeClass("active");

        }
        $("#levelInput").trigger("change");
    });

    $(document).on("click", ".level-load-more", function () {
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                action: "EExams_load_more_levels"
            },
            success: function (response) {
                $(".level").remove();
                $(".level-body").prepend(response);
                $(".level-footer").remove();
            }
        });
    });

    $(document).on("click", ".rate", function () {

        if (!$(this).hasClass("active")) {
            try {
                var json = JSON.parse($("#rateInput").val());
            } catch (e) {
                var json = [];
            }
            json.push($(this).data("filter"));
            $("#rateInput").val(JSON.stringify(json));
            $(this).addClass("active");

        } else {
            var json = JSON.parse($("#rateInput").val());
            var index = json.indexOf($(this).data("filter"));
            if (index !== -1) json.splice(index, 1);
            $("#rateInput").val(JSON.stringify(json));
            $(this).removeClass("active");

        }
        $("#rateInput").trigger("change");
        
    });


    //Rate Input Change restricted to single select
    $(document).on("click", ".rate", function () {
        //Restrict only one rating selection
        var rating_array = $("#rateInput").val();
        final_array = rating_array.split(",");
        console.log(final_array);
        console.log(final_array.length);
        if (final_array.length >= 2) {
            console.log("rating_count is 2 or more");
            $(".rate1,.rate2,.rate3,.rate4,.rate5").removeClass("active");       
            $(this).addClass("active");
            
            $("#rateInput").val(final_array[0]);
            var test_check_val = $("#rateInput").val();
            console.log("class added to itself removed others",test_check_val);

            try {
                var json = JSON.parse($("#rateInput").val());
            } catch (e) {
                var json = [];
            }
            json.push($(this).data("filter"));
            $("#rateInput").val(JSON.stringify(json));
            $(this).addClass("active");
            $("#rateInput").trigger("change");
        }        
        
    });

    $(document).on("click", ".duration", function () {
        if (!$(this).hasClass("active")) {
            try {
                var json = JSON.parse($("#durInput").val());
            } catch (e) {
                var json = [];
            }
            json.push($(this).data("filter"));
            $("#durInput").val(JSON.stringify(json));
            $(this).addClass("active");

        } else {
            var json = JSON.parse($("#durInput").val());
            var index = json.indexOf($(this).data("filter"));
            if (index !== -1) json.splice(index, 1);
            $("#durInput").val(JSON.stringify(json));
            $(this).removeClass("active");

        }
        $("#durInput").trigger("change");
    });
  
    //Duration Input Change restricted to single select
    $(document).on("click", ".duration", function () {
        //Restrict only one rating selection
        var duration_array = $("#durInput").val();
        final_dur_array = duration_array.split(",");
        console.log(final_dur_array);
        console.log(final_dur_array.length);
        if (final_dur_array.length >= 2) {
            console.log("rating_count is 2 or more");
            $(".dur1,.dur2,.dur3,.dur4,.dur5,.dur6,.dur7").removeClass("active");       
            $(this).addClass("active");
            
            $("#durInput").val(final_dur_array[0]);
            // var dur_check_val = $("#durInput").val();
            // console.log("class added to itself removed others",dur_check_val);

            // try {
            //     var json = JSON.parse($("#durInput").val());
            // } catch (e) {
            //     var json = [];
            // }
            // json.push($(this).data("filter"));
            // $("#durInput").val(JSON.stringify(json));
            $(this).addClass("active");
            $("#durInput").trigger("change");
        }        
        
    });

    $(document).on("click", ".points", function () {
        if (!$(this).hasClass("active")) {
            try {
                var json = JSON.parse($("#pointsInput").val());
            } catch (e) {
                var json = [];
            }
            json.push($(this).data("filter"));
            $("#pointsInput").val(JSON.stringify(json));
            $(this).addClass("active");

        } else {
            var json = JSON.parse($("#pointsInput").val());
            var index = json.indexOf($(this).data("filter"));
            if (index !== -1) json.splice(index, 1);
            $("#pointsInput").val(JSON.stringify(json));
            $(this).removeClass("active");

        }
        $("#pointsInput").trigger("change");
    });

    $(".from-year-select").on("change", function () {
        var value = this.value;
        console.log(value);
        var disable = true;
        $(".to-year-select option").each(function (i, obj) {
            var cvalue = parseInt($(this).val());
            if (!isNaN(cvalue) && cvalue <= value) {
                $(this).attr("disabled", "disabled")
            }
            else {
                if (!isNaN(cvalue)) {
                    $(this).removeAttr("disabled", "disabled")
                    disable = false;
                }
            }
            console.log(cvalue);
        });
        if (disable) {
            $(".to-year-select").attr("disabled", "disabled")
        }
        else {
            $(".to-year-select").removeAttr("disabled", "disabled")
        }
    })

    //clear
    $(".clear").on("click", function (event) {
        event.preventDefault()
        $(".active").removeClass("active");
        filterList = [];
        $(".filter-input").val("");
        Update(current_page, true);
    });

    $(".search-input").on("input", function () {
        if ($(this).val()) {
            $(this).next(".search_icon").html('<i class="fas fa-times"></i>');

        }
        else {
            $(this).next(".search_icon").html('<i class="fas fa-search"></i>');
        }
    });

    $(".tags-search").on("change", function () {
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                action: "EExams_search_tags",
                search: $(this).val()
            },
            success: function (response) {
                $(".tag").remove();
                $(".tag-body").prepend(response);
                $(".tag-footer").remove();
            }
        });
    });

    $('.search-input.filter-input').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            var name = $(this).attr("name");
            var val = $(this).val();
            var element = {
                name: name,
                value: val
            };
            filterList.pushOrUpdate(element, function (e) {
                return e.name === element.name;
            });
            Update(1, true);
        }
    });

    $('#goto_page').change(function() {
            var go_to_pagenumber = this.value; 
            console.log(go_to_pagenumber);         
            Update(go_to_pagenumber, true);
    });

    $('.tags-search').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();

            var search = this.value;
            $.ajax({
                type: "post",
                url: EExams.ajaxurl,
                data: {
                    action: "EExams_search_tags",
                    search: search
                },
                success: function (response) {
                    $(".tag").remove();
                    $(".tag-body").prepend(response);
                    $(".tag-footer").remove();
                }
            });
        }
    });


    $(".search_icon").on("click", function () {
        $(this).prev(".search-input").val("").trigger("change");
        $(this).html('<i class="fas fa-search"></i>');
    })

    var less = false;

    /***
     *
     * Code By Turaab Ali
     *
     * ****/

    window.showEditQuizModal = function (button_type) {
        var button_html = '<br><button onclick="exportPDF()" type="button" class="btn btn-primary">Export</button>';

        if ( button_type === 'save' ) {
            button_html = '<br><button onclick="savePDF()" type="button" class="btn btn-primary">Save</button>';
        }

        $('#button-div').children().remove();
        $('#button-div').append(button_html);
        $('#subtitle').val(subtitle);
        $('#title').val(title);
        if ( student_name_toggle == "true" ) {
            $('#student-name-toggle').closest('.toggle').removeClass('off');
            $('#student-name-toggle').closest('.toggle').removeClass('btn-default');
            $('#student-name-toggle').closest('.toggle').addClass('btn-primary');
            $('#student-name-toggle').prop('checked',true);
            $('#student-name').val(student_name);
        }

        if ( footer_toggle == "true" ) {
            $('#footer-toggle').closest('.toggle').removeClass('off');
            $('#footer-toggle').closest('.toggle').removeClass('btn-default');
            $('#footer-toggle').closest('.toggle').addClass('btn-primary');
            $('#footer-toggle').prop('checked',true);
            $('#footer').val(footer);
        }

        if ( page_number_toggle == "true" ) {
            $('#page-number-toggle').closest('.toggle').removeClass('off');
            $('#page-number-toggle').closest('.toggle').removeClass('btn-default');
            $('#page-number-toggle').closest('.toggle').addClass('btn-primary');
            $('#page-number-toggle').prop('checked',true);
        }

        if ( box_toggle == "true" ) {
            $('#box-toggle').closest('.toggle').removeClass('off');
            $('#box-toggle').closest('.toggle').removeClass('btn-default');
            $('#box-toggle').closest('.toggle').addClass('btn-primary');
            $('#box-toggle').prop('checked',true);
        }


        // $('#student-name').val(student_name);
        var ids = get_selected_questions();
        jQuery.ajax({
            type: 'POST',
            url: EExams.ajaxurl,
            data: {
                action: 'EExams_get_selected_questions',
                ids: ids,
                edit: edit,
                quizz_id: quizz_id
            },
            success:function(result) {
                console.log(result);
                //return;
                $('#edit-modal').modal();

                var data = JSON.parse(result);

                var count = 1;
                data.forEach(function(item) {
                    $('#edit-pdf-div #br-' + item.id).remove();
                    $('#edit-pdf-div #question-' + item.id).remove();
                    var html = '';
                    html += '<br id="br-' + item.id + '">';
                    html += '<div id="question-' + item.id + '" class="row">';
                    html += '<div class="col-md-12">';
                    html += '<h5 class="card-title">Question '+ count +'</h5>';
                    html += '<div class="card">';
                    html += '<div class="card-body">';
                    html += '<div class="row">';
                    html += '<div class="col-md-8"><h5 id="title-' + item.id + '" style="border: solid 1px black; padding: 5px;" contenteditable="true">'+ item.title +'</h5></div>';
                    html += '<div class="col-md-4 text-right">';
                    html += '<p id="points-' + item.id + '" style="border: solid 1px black; padding: 5px;" contenteditable="true">' + item.points + '</p>';
                    html += '</div>';
                    html += '</div>';
                    html += '<p id="body-' + item.id + '" style="border: solid 1px black; padding: 5px;" contenteditable="true">' + item.body + '</p>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    count++;
                    $('#edit-pdf-div').append(html);
                });



            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                alert(errorThrown);
            }
        });
    }

    window.exportPDF = function() {
        var title = $('#title').val();
        var subTitle = $('#subtitle').val();
        var boxToggle = $('#box-toggle').prop('checked');
        var studentNameToggle = $('#student-name-toggle').prop('checked');
        var studentName = $('#student-name').val();
        var footer = $('#footer').val();
        var footerToggle = $('#footer-toggle').prop('checked');
        var pageNumberToggle = $('#page-number-toggle').prop('checked');

        var ids = get_selected_questions();
        var questions = [];
        for ( var i = 0; i < ids.length; i++ ) {
            questions[i] = {
                "id": ids[i],
                "title": $('#title-' + ids[i]).html(),
                "body": $('#body-' + ids[i]).html(),
                "points": $('#points-' + ids[i]).html()
            };
        }
        var answers = $("input[name='answers']:checked").val();
        var url = window.location.href.replace(location.hash,"");

        $('#pdf-form').remove();
        var pdfForm = '<form id="pdf-form" method="post" action="'+ url +'" target="_blank">' +
            '<input type="hidden" name="title" value="' + title + '">' +
            '<input type="hidden" name="subtitle" value="' + subTitle + '">' +
            '<input type="hidden" name="box-toggle" value="' + boxToggle + '">' +
            '<input type="hidden" name="student-name-toggle" value="' + studentNameToggle + '">' +
            '<input type="hidden" name="student-name" value="' + studentName + '">' +
            '<input type="hidden" name="footer" value="' + footer + '">' +
            '<input type="hidden" name="footer-toggle" value="' + footerToggle + '">' +
            '<input type="hidden" name="page-number-toggle" value="' + pageNumberToggle + '">' +
            '<input type="hidden" name="pdfq" value="' + EExams.pdfq + '">' +
            '<input type="hidden" name="ids" value=\'' + JSON.stringify(ids) + '\'>' +
            '<input type="hidden" name="questions" value=\'' + JSON.stringify(questions) + '\'>' +
            '<input type="hidden" name="answers" value="' + answers + '">' +
            '</form>';


        $(pdfForm).appendTo('body').submit();
    }
    window.generatePDF = function() {
        $('.jquery-modal').modal().hide();
        $('.jquery-modal').removeClass('blocker');
        Swal.fire({
            showCloseButton: true,
            title: 'Export Quiz',
            html:
            '<input id="title" placeholder="Title" class="swal2-input">' +
            '<input id="subtitle" placeholder="Subtitle" class="swal2-input">',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#title').val(),
                        $('#subtitle').val()
                    ])
                })
            },
            onOpen: function () {
                $('#title').focus()
            }
        }).then(function (result) {
            var ids = get_selected_questions();
            var questions = [];
            for (var i = 0; i < ids.length; i++) {
                questions[i] = {
                    "id": ids[i],
                    "title": $('#title-' + ids[i]).text(),
                    "body": $('#body-' + ids[i]).text(),
                    "points": $('#points-' + ids[i]).text()
                };
                $('#title-' + ids[i]).remove();
                $('#body-' + ids[i]).remove();
                $('#points-' + ids[i]).remove();
            }


            var answers = $("input[name='answers']:checked").val();
            var url = location.href.replace(location.hash,"");
            if (url.indexOf("?") > -1) {
                url += "&answers=" + answers;
            } else {
                url += "?answers=" + answers;
            }
            url += "&pdfq=" + EExams.pdfq + "&title=" + result.value[0] + "&subtitle=" + result.value[1] + "&ids=" + JSON.stringify(ids) + "&questions=" + JSON.stringify(questions);
            $("<div class='modal'><iframe src='" + url + "'></iframe></div>").appendTo('body').modal();
        });
    }
    //finsih
    $(".finish").on("click", function () {
        if (edit) {
            var ids = get_selected_questions();
            var answers = $("input[name='answers']:checked").val();
            var url = window.location.href.replace(location.hash,"");
            if (url.indexOf("?") > -1) {
                url += "&answers=" + answers;
            } else {
                url += "?answers=" + answers;
            }
            url += "&pdfq=" + EExams.pdfq + "&title=" + title + "&subtitle=" + subtitle + "&ids=" + JSON.stringify(ids);
            $("<div class='modal'><iframe src='" + url + "'></iframe></div>").appendTo('body').modal();
        }
        else {
            var ids = get_selected_questions();
            jQuery.ajax({
                type: 'POST',
                url: EExams.ajaxurl,
                data: {
                    action: 'EExams_get_selected_questions',
                    ids: ids
                },
                success:function(result) {
                    console.log(result);
                    //return;

                    var data = JSON.parse(result);
                    var html = '';
                    data.forEach(function(item) {
                        html += '';
                        html += '<div class="row">';
                        html += '<div class="col-md-6"><h4 id="title-' + item.id + '" contenteditable="true">' + item.title + '</h4></div>';
                        html +=	'<div class="col-md-6 text-right"><p id="points-' + item.id + '" contenteditable="true">' + item.points + '</p></div>';
                        html += '</div>';

                        html += '<div class="row">';
                        html +=	'<div class="col-md-12 text-right"><p id="body-' + item.id + '" contenteditable="true">' + item.body + '</p></div>';
                        html += '</div>';
                    });
                    html += '<button type="button" onclick="generatePDF()" class="top-button btn btn-success float-right">Generate PDF</button>';
                    $("<div class='modal'>" + html + "</div>").appendTo('.container-fluid').modal();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    alert(errorThrown);
                }
            });
        }

    });




    $(".SaveAs").on("click", function () {
        Swal.fire({
            showCloseButton: true,
            title: 'Save Quiz',
            html:
            '<input id="title" placeholder="Title" class="swal2-input">' +
            '<input id="subtitle" placeholder="Subtitle" class="swal2-input">',
            preConfirm: function () {
                return new Promise(function (resolve) {
                    resolve([
                        $('#title').val(),
                        $('#subtitle').val()
                    ])
                })
            },
            onOpen: function () {
                $('#title').focus()
            }
        }).then(function (result) {
            var ids = get_selected_questions();
            var answers = $("input[name='answers']:checked").val();
            var url = EExams.ajaxurl;
            if (url.indexOf("?") > -1) {
                url += "&answers=" + answers;
            } else {
                url += "?answers=" + answers;
            }
            if (result.value[0] === "") {
                return;
            }
            url += "&save=true&title=" + result.value[0] + "&subtitle=" + result.value[1] + "&ids=" + JSON.stringify(ids);
            fetch(url + "&action=saveQuiz").then(response => response.json()).then(data => {
                console.log(data);
            if (data.hasOwnProperty("success") && data.success === "yes") {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Quiz Saved!',
                    showConfirmButton: false,
                    timer: 500
                })
            }
            else {
                Swal.fire(
                    'Error',
                    'There was an error while saving the quiz',
                    'success'
                );
            }
        });
        }).catch(swal.noop)
    });


    window.savePDF = function() {

        var title = $('#title').val();
        var subTitle = $('#subtitle').val();
        var boxToggle = $('#box-toggle').prop('checked');
        var studentNameToggle = $('#student-name-toggle').prop('checked');
        var studentName = $('#student-name').val();
        var footer = $('#footer').val();
        var footerToggle = $('#footer-toggle').prop('checked');
        var pageNumberToggle = $('#page-number-toggle').prop('checked');

        var ids = get_selected_questions();
        var questions = [];
        for ( var i = 0; i < ids.length; i++ ) {
            questions[i] = {
                "id": ids[i],
                "title": $('#title-' + ids[i]).html(),
                "body": $('#body-' + ids[i]).html(),
                "points": $('#points-' + ids[i]).html()
            };
        }
        var answers = $("input[name='answers']:checked").val();


        // console.log(questions);
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                action: "saveQuiz",
                title: title,
                subTitle: subTitle,
                boxToggle: boxToggle,
                studentNameToggle: studentNameToggle,
                studentName: studentName,
                footer: footer,
                footerToggle: footerToggle,
                pageNumberToggle: pageNumberToggle,
                questions: JSON.stringify(questions),
                answers: answers,
                save: true,
                edit: edit,
                quizz_id: quizz_id,
                ids: JSON.stringify(ids)
            },
            success: function (response) {
                var data = JSON.parse(response);
                if (data.success) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Quiz Saved!',
                        showConfirmButton: false,
                        timer: 800
                    })
                    // location.reload();
                    // questions['test'] = 'test';
                    // window.location.href(data.redirect);
                    window.location.href = data.redirect;
                    console.log(questions);
                    console.log(response);
                }
                else {
                    Swal.fire(
                        'Error',
                        'There was an error while saving the quiz',
                        'error'
                    );
                }
            }
        });
    }
    //save

    $(".Save").on("click", function () {
        if (edit) {
            var ids = get_selected_questions();
            var answers = $("input[name='answers']:checked").val();
            var url = EExams.ajaxurl;
            if (url.indexOf("?") > -1) {
                url += "&answers=" + answers;
            } else {
                url += "?answers=" + answers;
            }
            url += "&save=true&SaveEdit=true&id=" + parseInt(quizz_id) + "&title=" + title + "&subtitle=" + subtitle + "&ids=" + JSON.stringify(ids);
            fetch(url + "&action=saveQuiz").then(response => response.json()).then(data => {
                console.log(data);
            if (data.hasOwnProperty("success") && data.success === "yes") {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Quiz Saved!',
                    showConfirmButton: false,
                    timer: 800
                })
            }
            else {
                Swal.fire(
                    'Error',
                    'There was an error while saving the quiz',
                    'error'
                );
            }
        });
        }
        else {
            Swal.fire({
                showCloseButton: true,
                title: 'Save Quiz',
                html:
                '<input id="title-swal" placeholder="Title" class="swal2-input" value="' + title + '">' +
                '<input id="subtitle-swal" placeholder="Subtitle" class="swal2-input" value="' + subtitle + '">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#title-swal').val(),
                            $('#subtitle-swal').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#title-swal').focus()
                }
            }).then(function (result) {
                var ids = get_selected_questions();
                var answers = $("input[name='answers']:checked").val();
                var url = EExams.ajaxurl;

                if (url.indexOf("?") > -1) {
                    url += "&answers=" + answers;
                } else {
                    url += "?answers=" + answers;
                }
                if (result.value[0] === "") {
                    return;
                }
                url += "&save=true&title=" + result.value[0] + "&subtitle=" + result.value[1] + "&ids=" + JSON.stringify(ids);
                fetch(url + "&action=saveQuiz").then(response => response.json()).then(data => {
                    console.log(data);
                if (data.hasOwnProperty("success") && data.success === "yes") {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Quiz Saved!',
                        showConfirmButton: false,
                        timer: 800
                    })
                }
                else {
                    Swal.fire(
                        'Error',
                        'There was an error while saving the quiz',
                        'error'
                    );
                }
            });
            }).catch(swal.noop)
        }
    });

    //filter Input
    $(".filter-input").on("change", function () {
        
        var name = $(this).attr("name");
        var val = $(this).val();
        if(name === "has-answers")
        {
            if($(this).prop("checked") == true){
                val = "on";
            }
            else if($(this).prop("checked") == false){
                val = "off";
            }
        }
        var element = {
            name: name,
            value: val
        };
        filterList.pushOrUpdate(element, function (e) {
            return e.name === element.name;
        });
        Update(1, true);
    });

    function Update(page, filter, initvar = false) {
        $.ajax({
            type: "post",
            url: ajaxCallUrl,
            data: {
                filter: filterList,
                action: action,
                page: page,
                selectedQuestions: selectedQuestions,
            },
            dataType: "json",
            success: function (response) {
                if (response.hasOwnProperty("error")) {
                    $(".questionsSelectedFrom").empty();
                    $("#pagination").hide();
                    $(".questionsSelectedFrom").append("<div class='no-results'>There is no results matching your search</div>")
                    $(".overlay").fadeOut();
                    return;
                }
                //console.log(response);
                dataSet = response.data;
                current_page = response.current_page;
                //console.log(current_page);
                if (filter) {
                    numberOfpages = response.numberOfPages;
                    $("#number_of_questions").html(response.numberOfQuestions);
                    init();
                    $("#pagination").show();
                    display();
                } else {
                    display();
                }
                if (initvar) {
                    totalNumberOfPages = numberOfpages;
                }
            }
        });
    }

    function display() {
        $(".questionsSelectedFrom").empty();
        for (var i = 0; i < dataSet.length; i++) {
            var question = dataSet[i];
            if (question === "") continue;
            if (!question.thumbnail) {
                question.thumbnail = "";
            }
            var html =
                `<div class="row question" id="question-${question.id}">
					<div class="col-12 no-gutters">
						<h3>${question.title}  <span class="question-id">#${question.id}</span></h3>
						<p>${question.grade} • ${question.subject} • ${question.hours} h, ${question.mins} m • ${question.level} • ${question.year}</p>
						<select class='star${question.id}'>
							<option value=''></option>
							<option value='1'>1</option>
							<option value='2'>2</option>
							<option value='3'>3</option>
							<option value='4'>4</option>
							<option value='5'>5</option>
						</select>
						<div class='QuestionContent'>
							${question.body} 

							<br><br>

				`;
            if((question.type === "radio" || question.type === "multi") && question.has_answers)
            {
                html += `
						<div class="answers">
							<ol>`;
                JSON.parse(stripslashes(question.answers)).forEach(answer => {
                    html += `<li>${answer}</li>`;
            });
                html += `<ol>
						<br>
						<i>Answer</i><br>
						${question.correct_answer}
						</div>	
					`;
            }
            html +=`
				</div>

					</div>

					<span class="arrow"><i class="fas fa-arrow-right"></i></span>
					<span class="report" id="report-${question.id}" style="font-size: 12px;">
						<i class="far fa-flag"></i><br>report
					</span>
					<span class="view" id="view-${question.id}" style="font-size: 12px;">Full View <i class="fas fa-expand"></i></span>
					<span class="move" style="font-size: 12px;">Reorder <i class="fas fa-arrows-alt"></i></span>
					<span class="remove" style="font-size: 12px;">Remove <i class="fas fa-trash"></i></span>

				</div>`;
            $(".questionsSelectedFrom").append(html);
            $('.QuestionContent').readmore({
                collapsedHeight: 150,
                moreLink: '<a href="#" class="readmore">Expand <i class="fas fa-angle-down"></a>',
                lessLink: '<a href="#" class="readmore">Show less <i class="fas fa-angle-up"></a>'
            });
            $('.star' + question.id).barrating({
                theme: 'fontawesome-stars-o',
                initialRating: question.rating,
                allowEmpty: true,
                onSelect: function (value, text, event) {
                    if (typeof (event) !== 'undefined') {
                        var id = $(this.$elem).attr("class").substring(4);
                        $.ajax({
                            type: "post",
                            url: EExams.ajaxurl,
                            data: {
                                action: "rate_updated",
                                id: id,
                                level: value,
                            },
                            dataType: "json",
                            success: function (response) {
                                console.log(response);
                            }
                        });
                        console.log(event.target);
                    } else {
                        // rating was selected programmatically
                        // by calling `set` method
                    }
                }
            });

        }
        MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        MathJax.Hub.Queue(inlineMath);
    }

    var inlineMath = function () {
        var existingStyles = $(".MathJax_Display").attr("style");
        $(".MathJax_Display").attr("style", existingStyles + "display:inline!important");
        $(".overlay").fadeOut();
    }

    function paginate() {
        if (current_page != past_page) {
            past_page = current_page;
            Update(current_page, false);
        }
        display();
    }

    function calculate_selection_add(id) {
        dataSet.forEach(question => {
            if (question.id === id) {
            dataSet_selected.push(question);
        }
    });
        calculate_selection();
    }

    function calculate_selection_remove(id) {
        var i = -1;
        dataSet_selected.forEach(function (question, index) {
            if (question.id === id) {
                i = index;
            }
        });
        if (i > -1) {
            dataSet_selected.splice(i, 1);
        }
        calculate_selection();
    }

    function calculate_selection() {
        var total_time = 0;
        var total_points = 0;
        dataSet_selected.forEach(question => {
            total_time += parseInt(question.total_time_in_seconds);
        total_points += parseInt(question.points);
    });

        $("#selection-num").html(dataSet_selected.length);
        $("#selection-time").html(Math.ceil(total_time / 60));
        $("#selection-points").html(total_points);
    }
    
    $(".clear_results_right").on("click", function () {
        console.log("Cleared..");
        $(".questionsSelectedTo").html("");
        $(".your-selection-container").hide();
     });

    function get_selected_questions() {
        var ids = [];
        selectedQuestions.forEach(function (element) {
            ids.push(element);
        });
        return ids;
    }

})(jQuery);

function stripslashes (str) {
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +      fixed by: Mick@el
    // +   improved by: marrtins
    // +   bugfixed by: Onno Marsman
    // +   improved by: rezna
    // +   input by: Rick Waldron
    // +   reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +   input by: Brant Messenger (http://www.brantmessenger.com/)
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: stripslashes('Kevin\'s code');
    // *     returns 1: "Kevin's code"
    // *     example 2: stripslashes('Kevin\\\'s code');
    // *     returns 2: "Kevin\'s code"
    return (str + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
            case '\\':
                return '\\';
            case '0':
                return '\u0000';
            case '':
                return '';
            default:
                return n1;
        }
    });
}