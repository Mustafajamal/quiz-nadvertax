(function ($) {
    $(".edit").on("click", function (event) {
        event.preventDefault()
        var id = $(this).attr("id");
        var titleID = $(this).data("title");
        swal("Enter A title:", {
            content: "input"
        }).then(value => {
            if(!!value.trim())
            {
                $.ajax({
                    type: "post",
                    url: EExams.ajaxurl,
                    data: {
                        id: id,
                        title: value,
                        action: "editQuizz"
                    },
                    dataType: "json",
                    success: function (response) { 
                        if (response.success == "true") {
                            $(titleID).html(value);
                        }
                    }
                });
            }
        });
    });

    //Edit Subtitle
    $(".edit_sub").on("click", function (event) {
        event.preventDefault()
        var id = $(this).attr("id");
        var titleID = $(this).data("title");
        swal("Enter Subtitle:", {
            content: "input"
        }).then(value => {
            if(!!value.trim())
            {
                $.ajax({
                    type: "post",
                    url: EExams.ajaxurl,
                    data: {
                        id: id,
                        title: value,
                        action: "editQuizzSub"
                    },
                    dataType: "json",
                    success: function (response) { 
                        if (response.success == "true") {
                            $(titleID).html(value);
                        }
                    }
                });
            }
        });
    });
    $(".delete").on("click", function (event) {
        event.preventDefault()
        var id = $(this).attr("id");
        var row = $(this).data("row");
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                id: id,
                action: "deleteQuizz"
            },
            dataType: "json",
            success: function (response) {
                if (response.success == "true") {
                    $(row).remove();
                }
            }
        });
    });
    $(".export").on("click", function (event) {
        event.preventDefault()
        var id = $(this).attr("id");
        $.ajax({
            type: "post",
            url: EExams.ajaxurl,
            data: {
                id: id,
                action: "getQuizz"
            },
            success: function (data) {
                var response = JSON.parse(data);
                if (response.success) {
                    var url = EExams.exportUrl;
                    var ids = response.json;

                    //var url = window.location.href.replace(location.hash,"");

                    $('#pdf-form').remove();
                    var pdfForm = '<form id="pdf-form" method="post" action="'+ url +'" target="_blank">' +
                        '<input type="hidden" name="title" value="' + response.title + '">' +
                        '<input type="hidden" name="subtitle" value="' + response.subtitle + '">' +
                        '<input type="hidden" name="box-toggle" value="' + response.box_toggle + '">' +
                        '<input type="hidden" name="student-name-toggle" value="' + response.student_name_toggle + '">' +
                        '<input type="hidden" name="student-name" value="' + response.student_name + '">' +
                        '<input type="hidden" name="footer" value="' + response.footer + '">' +
                        '<input type="hidden" name="footer-toggle" value="' + response.footer_toggle + '">' +
                        '<input type="hidden" name="page-number-toggle" value="' + response.page_number_toggle + '">' +
                        '<input type="hidden" name="pdfq" value="' + EExams.pdfq + '">' +
                        '<input type="hidden" name="ids" value=\'' + ids + '\'>' +
                        '<input type="hidden" name="questions" value=\'' + response.questions + '\'>' +
                        '<input type="hidden" name="answers" value="' + response.answers + '">' +
                        '</form>';


                    $(pdfForm).appendTo('body').submit();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
})(jQuery);

window.exportPDF = function() {
    var title = $('#title').val();
    var subTitle = $('#subtitle').val();
    var boxToggle = $('#box-toggle').prop('checked');
    var studentNameToggle = $('#student-name-toggle').prop('checked');
    var studentName = $('#student-name').val();
    var footer = $('#footer').val();
    var footerToggle = $('#footer-toggle').prop('checked');
    var pageNumberToggle = $('#page-number-toggle').prop('checked');

    var ids = get_selected_questions();
    var questions = [];
    for ( var i = 0; i < ids.length; i++ ) {
        questions[i] = {
            "id": ids[i],
            "title": $('#title-' + ids[i]).text(),
            "body": $('#body-' + ids[i]).text(),
            "points": $('#points-' + ids[i]).text()
        };
    }
    var answers = $("input[name='answers']:checked").val();
    var url = window.location.href.replace(location.hash,"");

    $('#pdf-form').remove();
    var pdfForm = '<form id="pdf-form" method="post" action="'+ url +'" target="_blank">' +
        '<input type="hidden" name="title" value="' + title + '">' +
        '<input type="hidden" name="subtitle" value="' + subTitle + '">' +
        '<input type="hidden" name="box-toggle" value="' + boxToggle + '">' +
        '<input type="hidden" name="student-name-toggle" value="' + studentNameToggle + '">' +
        '<input type="hidden" name="student-name" value="' + studentName + '">' +
        '<input type="hidden" name="footer" value="' + footer + '">' +
        '<input type="hidden" name="footer-toggle" value="' + footerToggle + '">' +
        '<input type="hidden" name="page-number-toggle" value="' + pageNumberToggle + '">' +
        '<input type="hidden" name="pdfq" value="' + EExams.pdfq + '">' +
        '<input type="hidden" name="ids" value=\'' + JSON.stringify(ids) + '\'>' +
        '<input type="hidden" name="questions" value=\'' + JSON.stringify(questions) + '\'>' +
        '<input type="hidden" name="answers" value="' + answers + '">' +
        '</form>';


    $(pdfForm).appendTo('body').submit();
}